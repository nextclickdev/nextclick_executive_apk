package com.kwiky.executivemodule.models;

public class Exicutivetypes {

    String id,executive_type;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getExecutive_type() {
        return executive_type;
    }

    public void setExecutive_type(String executive_type) {
        this.executive_type = executive_type;
    }
}
