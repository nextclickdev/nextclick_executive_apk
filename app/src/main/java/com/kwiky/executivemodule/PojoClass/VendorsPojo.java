package com.kwiky.executivemodule.PojoClass;


import java.util.ArrayList;

public class VendorsPojo {

    String id,vendor_user_id,customer_name,unique_id,location_id,executive_id,constituency_id,category_id,
            desc,no_of_banners,address,landmark,pincode,sounds_like,created_at,updated_at,vendorstatus,
            availability,executive_user_id,shop_address;
    String name;
    String email;
    String image;
    String is_having_lead;
    Double lattitude;
    Double longitude;
    double distance;
    boolean status;

    public String getShop_address() {
        return shop_address;
    }

    public void setShop_address(String shop_address) {
        this.shop_address = shop_address;
    }

    public String getVendorstatus() {
        return vendorstatus;
    }

    public void setVendorstatus(String vendorstatus) {
        this.vendorstatus = vendorstatus;
    }

    public String getVendor_user_id() {
        return vendor_user_id;
    }

    public void setVendor_user_id(String vendor_user_id) {
        this.vendor_user_id = vendor_user_id;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getUnique_id() {
        return unique_id;
    }

    public void setUnique_id(String unique_id) {
        this.unique_id = unique_id;
    }

    public String getLocation_id() {
        return location_id;
    }

    public void setLocation_id(String location_id) {
        this.location_id = location_id;
    }

    public String getExecutive_id() {
        return executive_id;
    }

    public void setExecutive_id(String executive_id) {
        this.executive_id = executive_id;
    }

    public String getConstituency_id() {
        return constituency_id;
    }

    public void setConstituency_id(String constituency_id) {
        this.constituency_id = constituency_id;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getNo_of_banners() {
        return no_of_banners;
    }

    public void setNo_of_banners(String no_of_banners) {
        this.no_of_banners = no_of_banners;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getSounds_like() {
        return sounds_like;
    }

    public void setSounds_like(String sounds_like) {
        this.sounds_like = sounds_like;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getAvailability() {
        return availability;
    }

    public void setAvailability(String availability) {
        this.availability = availability;
    }

    public String getExecutive_user_id() {
        return executive_user_id;
    }

    public void setExecutive_user_id(String executive_user_id) {
        this.executive_user_id = executive_user_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Double getLattitude() {
        return lattitude;
    }

    public void setLattitude(Double lattitude) {
        this.lattitude = lattitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }




    public String getIs_having_lead() {
        return is_having_lead;
    }

    public void setIs_having_lead(String is_having_lead) {
        this.is_having_lead = is_having_lead;
    }
}
