package com.kwiky.executivemodule.Activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.kwiky.executivemodule.Helpers.CustomDialog;
import com.kwiky.executivemodule.Helpers.Logout_Dialog;
import com.kwiky.executivemodule.Helpers.NetworkUtil;
import com.kwiky.executivemodule.Helpers.uiHelpers.UImsgs;
import com.kwiky.executivemodule.PojoClass.Transactions;
import com.kwiky.executivemodule.PojoClass.VendorsPojo;
import com.kwiky.executivemodule.R;
import com.kwiky.executivemodule.authentication.CreateVendor;
import com.kwiky.executivemodule.utilities.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import static com.kwiky.executivemodule.Config.Config.APP_ID;
import static com.kwiky.executivemodule.Config.Config.APP_ID_VALUE;
import static com.kwiky.executivemodule.Config.Config.GET_TERMSCONDITIONS;
import static com.kwiky.executivemodule.Config.Config.PROFILE;
import static com.kwiky.executivemodule.Config.Config.USER_TOKEN;
import static com.kwiky.executivemodule.Config.Config.VENDOR_LIST;
import static com.kwiky.executivemodule.Config.Config.WALLETHISTORY;
import static com.kwiky.executivemodule.Config.Config.X_AUTH_TOKEN;

public class DashboardActivity extends Activity {

    private FloatingActionButton add_vendor;
    private CardView card_addvendor;
    private CardView card_totalvendors,card_profile,card_wallet,card_pending,card_approved;
    private ImageView logout,refresh_details;
    private TextView approved_vendors,tv_pending,tv_total_vendors,tv_walletBalance;
    PreferenceManager preferenceManager;
    private CustomDialog customDialog;
    private Context mContext;
    private ArrayList<VendorsPojo> totalvendorlist;
    private ArrayList<VendorsPojo> pendingvendorlist;
    private ArrayList<VendorsPojo> approvedvendorlist;
    private int pendingcount=0,approvedcount=0;
    String year,month,day,start_date_str,end_date_str,status="2",joing_date="";
    private boolean newDesign = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(newDesign)
        setContentView(R.layout.activity_dashboard_new);
        else
            setContentView(R.layout.activity_dashboard);

        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.Iconblue));
        }

      //  getSupportActionBar().hide();

        add_vendor=findViewById(R.id.add_vendor);
        card_profile=findViewById(R.id.card_profile);
        card_totalvendors=findViewById(R.id.card_totalvendors);
        card_wallet=findViewById(R.id.card_wallet);
        card_pending=findViewById(R.id.card_pending);
        card_approved=findViewById(R.id.card_approved);
        tv_walletBalance=findViewById(R.id.tv_walletBalance);
        logout=findViewById(R.id.logout);
        refresh_details=findViewById(R.id.refresh_details);
        tv_pending=findViewById(R.id.tv_pending);
        tv_total_vendors=findViewById(R.id.tv_total_vendors);
        approved_vendors=findViewById(R.id.approved_vendors);
        card_addvendor=findViewById(R.id.card_addvendor);
        mContext =DashboardActivity.this;
        preferenceManager=new PreferenceManager(DashboardActivity.this);
        customDialog=new CustomDialog(DashboardActivity.this);

        totalvendorlist=new ArrayList<>();
        pendingvendorlist=new ArrayList<>();
        approvedvendorlist=new ArrayList<>();

        if (NetworkUtil.isNetworkConnected(DashboardActivity.this)) {
            fetchUserDetails();
        } else {
            Toast.makeText(DashboardActivity.this, ""+getResources().getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
        }

        refresh_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (NetworkUtil.isNetworkConnected(DashboardActivity.this)) {
                    getvendorlist();
                } else {
                    Toast.makeText(DashboardActivity.this, ""+getResources().getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
                }

                getDate();
                if (NetworkUtil.isNetworkConnected(DashboardActivity.this)) {
                    getWallet(start_date_str,end_date_str,"","");
                } else {
                    Toast.makeText(DashboardActivity.this, ""+getResources().getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
                }

                if (NetworkUtil.isNetworkConnected(DashboardActivity.this)) {
                    fetchUserDetails();
                } else {
                    Toast.makeText(DashboardActivity.this, ""+getResources().getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
                }

            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Logout_Dialog logout_dialog=new Logout_Dialog(DashboardActivity.this);
                logout_dialog.showDialog();
            }
        });
        add_vendor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (status.equalsIgnoreCase("1")){
                    startActivity(new Intent(DashboardActivity.this, CreateVendor.class));//RegisterVendorActivity
                }else {
                    Toast.makeText(mContext, "Please Contact Admin", Toast.LENGTH_SHORT).show();
                }
               // startActivity(new Intent(DashboardActivity.this,CommonFiledsActivity.class));
            }
        }); card_addvendor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (status.equalsIgnoreCase("1")){
                    startActivity(new Intent(DashboardActivity.this,CreateVendor.class));//RegisterVendorActivity
                }else {
                    Toast.makeText(mContext, "Please Contact Admin", Toast.LENGTH_SHORT).show();
                }
               // startActivity(new Intent(DashboardActivity.this,CommonFiledsActivity.class));
            }
        });
        card_totalvendors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (status.equalsIgnoreCase("1")){
                    Intent intent=new Intent(DashboardActivity.this,VendorsActivity.class);
                    intent.putExtra("title","Total vendors");
                    intent.putExtra("whichshow",1);
                    startActivity(intent);
                }else {
                    Toast.makeText(mContext, "Please Contact Admin", Toast.LENGTH_SHORT).show();
                }

            }
        }); card_pending.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (status.equalsIgnoreCase("1")){
                    Intent intent=new Intent(DashboardActivity.this,VendorsActivity.class);
                    intent.putExtra("title","Pending vendors");
                    intent.putExtra("whichshow",2);
                    startActivity(intent);
                }else {
                    Toast.makeText(mContext, "Please Contact Admin", Toast.LENGTH_SHORT).show();
                }

            }
        }); card_approved.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (status.equalsIgnoreCase("1")){
                    Intent intent=new Intent(DashboardActivity.this,VendorsActivity.class);
                    intent.putExtra("title","Approved vendors");
                    intent.putExtra("whichshow",3);
                    startActivity(intent);
                }else {
                    Toast.makeText(mContext, "Please Contact Admin", Toast.LENGTH_SHORT).show();
                }

            }
        });
        card_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(mContext,ProfileActivity.class);
                i.putExtra("joing_date",joing_date);
                startActivity(i);
            }
        });
        card_wallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (status.equalsIgnoreCase("1")){
                    startActivity(new Intent(mContext,WalletActivity.class));
                }else {
                    Toast.makeText(mContext, "Please Contact Admin", Toast.LENGTH_SHORT).show();
                }

            }
        });

       // getvendorlist();


    }

    private void fetchUserDetails() {
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, PROFILE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.d("profile r response", response);

                    JSONObject jsonObject = new JSONObject(response);
                    JSONObject jsonObjectData = jsonObject.getJSONObject("data");
                    System.out.println("aaaaaa  responce  " + jsonObject);

                     status=jsonObjectData.getString("status");
                    String joingdate=jsonObjectData.getString("created_at");
                    String[] datespilt=joingdate.split(" ");
                    String[] datesspilt=datespilt[0].split("-");


                    joing_date=datesspilt[2]+"/"+datesspilt[1]+"/"+datesspilt[0];

                    JSONObject jsonObject1=jsonObjectData.getJSONObject("executive_address");

                    String executive_type_id=jsonObject1.getString("executive_type_id");

                    if (executive_type_id.equalsIgnoreCase("2")){
                        card_wallet.setVisibility(View.GONE);
                    }

                }
                 catch (JSONException e) {
                    System.out.println("aaaaaaa catch  "+e.getMessage());
                    e.printStackTrace();
                }
                //Toast.makeText(mContext, "responce "+response, Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.d("fetch user error", error.toString());
            }
        }) {
           /* @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                return super.getBody();
            }*/

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put("X_AUTH_TOKEN", preferenceManager.getString(USER_TOKEN));
                System.out.println("aaaaaa token "+ preferenceManager.getString(USER_TOKEN));
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }
    private void getvendorlist() {
        pendingcount=0;approvedcount=0;
        customDialog.show();
        RequestQueue requestQueue = Volley.newRequestQueue(DashboardActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, VENDOR_LIST,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("vendor_list", response);
                        customDialog.dismiss();
                        if (response != null) {
                            try {

                                JSONObject jsonObject = new JSONObject(response);
                                System.out.println("aaaaaaaa vendorlist  "+jsonObject.toString());
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    JSONArray datarray = jsonObject.getJSONArray("data");
                                    for (int i=0;i<datarray.length();i++){
                                        JSONObject jsonObject1=datarray.getJSONObject(i);
                                        VendorsPojo vendorsPojo=new VendorsPojo();
                                       /* vendorsPojo.setId(jsonObject1.getString("id"));
                                        vendorsPojo.setVendor_user_id(jsonObject1.getString("vendor_user_id"));
                                        vendorsPojo.setCustomer_name(jsonObject1.getString("customer_name"));
                                        vendorsPojo.setName(jsonObject1.getString("name"));
                                        vendorsPojo.setEmail(jsonObject1.getString("email"));
                                        vendorsPojo.setUnique_id(jsonObject1.getString("unique_id"));
                                        vendorsPojo.setLocation_id(jsonObject1.getString("location_id"));
                                        vendorsPojo.setExecutive_id(jsonObject1.getString("executive_id"));
                                        vendorsPojo.setConstituency_id(jsonObject1.getString("constituency_id"));
                                        vendorsPojo.setCategory_id(jsonObject1.getString("category_id"));
                                        vendorsPojo.setDesc(jsonObject1.getString("desc"));
                                        vendorsPojo.setAddress(jsonObject1.getString("address"));
                                        vendorsPojo.setNo_of_banners(jsonObject1.getString("no_of_banners"));
                                        vendorsPojo.setLandmark(jsonObject1.getString("landmark"));
                                        vendorsPojo.setPincode(jsonObject1.getString("pincode"));
                                        vendorsPojo.setSounds_like(jsonObject1.getString("sounds_like"));
                                        vendorsPojo.setCreated_at(jsonObject1.getString("created_at"));
                                        vendorsPojo.setUpdated_at(jsonObject1.getString("updated_at"));
                                        vendorsPojo.setAvailability(jsonObject1.getString("availability"));
                                        vendorsPojo.setStatus(jsonObject1.getString("status"));
                                        vendorsPojo.setExecutive_user_id(jsonObject1.getString("executive_user_id"));
*/
                                        if (jsonObject1.getString("status").equalsIgnoreCase("2")){
                                          //  pendingvendorlist.add(vendorsPojo);
                                            pendingcount=pendingcount+1;
                                        }else {
                                            approvedcount=approvedcount+1;
                                           // approvedvendorlist.add(vendorsPojo);
                                        }
                                       // totalvendorlist.add(vendorsPojo);
                                    }

                                    approved_vendors.setText(""+approvedcount);
                                    tv_pending.setText(""+pendingcount);
                                    tv_total_vendors.setText("Total Vendors ( "+(pendingcount+approvedcount)+" )");

                                }
                            } catch (Exception e) {
                                System.out.println("aaaaaaaa catch "+e.getMessage());
                                customDialog.dismiss();
                                e.printStackTrace();
                            }
                        } else {
                            customDialog.dismiss();
                            UImsgs.showToast(mContext, getString(R.string.maintenance));
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        customDialog.dismiss();
                        UImsgs.showToast(mContext, getString(R.string.oops));
                        System.out.println("aaaaaaa  111 "+error.getMessage());
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                System.out.println("aaaaaaa token "+ preferenceManager.getString(USER_TOKEN));
                map.put(X_AUTH_TOKEN, preferenceManager.getString(USER_TOKEN));
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (NetworkUtil.isNetworkConnected(DashboardActivity.this)) {
            getvendorlist();
            fetchUserDetails();
        } else {
            Toast.makeText(DashboardActivity.this, ""+getResources().getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
        }

        getDate();
        if (NetworkUtil.isNetworkConnected(DashboardActivity.this)) {
            getWallet(start_date_str,end_date_str,"","");
        } else {
            Toast.makeText(DashboardActivity.this, ""+getResources().getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
        }



    }

    public void getWallet(String start_date_str, String end_date_str, String s, String s1){


        Map<String, String> uploadMap = new HashMap<>();

        uploadMap.put("start_date", start_date_str);
        uploadMap.put("end_date", end_date_str);
        uploadMap.put("type", s);
        uploadMap.put("status", s1);

        final JSONObject json = new JSONObject(uploadMap);

        System.out.println("aaaaaaaaa request"+json.toString());
        customDialog.show();

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, WALLETHISTORY,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        customDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("aaaaaaaa jsonobject  "+jsonObject.toString());
                            boolean status=jsonObject.getBoolean("status");
                            int http_code=jsonObject.getInt("http_code");

                            if (status){
                                String message=jsonObject.getString("message");
                                JSONObject dataobj=jsonObject.getJSONObject("data");

                                try{
                                    JSONObject paymentobj=dataobj.getJSONObject("user");
                                    tv_walletBalance.setText("₹ "+paymentobj.getString("wallet"));
                                }catch (JSONException e2){

                                }
                            }

                        } catch (JSONException e) {
                            Toast.makeText(mContext, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customDialog.dismiss();
                Toast.makeText(mContext, ""+error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        })
        {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("X_AUTH_TOKEN",preferenceManager.getString(USER_TOKEN));
                return map;
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return json.toString() == null ? null : json.toString().getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }
    public void getDate(){
        Calendar calander = Calendar.getInstance();
        int mday = calander.get(Calendar.DAY_OF_MONTH);
        int cMonth = calander.get(Calendar.MONTH) + 1;
        year =""+ calander.get(Calendar.YEAR);
        if ((cMonth) <= 9) {
            month = 0 + "" + (cMonth);
        } else {
            month = "" + (cMonth);
        }  if (mday <= 9) {
            day = 0 + "" + mday;
        } else {
            day = "" + mday;
        }

        start_date_str=year+"-"+month+"-"+day;
        end_date_str=year+"-"+month+"-"+day;


        SimpleDateFormat mdformat = new SimpleDateFormat("HH:mm:ss");
        String strDate = "Current Time : " + mdformat.format(calander.getTime());
          /* int cHour = calander.get(Calendar.Ho);
            int cMinute = calander.get(Calendar.MINUTE);
            int cSecond = calander.get(Calendar.SECOND);
    */
    }

    public void setLogout() {
        UImsgs.showToast(DashboardActivity.this,"Successfully logged out");
        preferenceManager.clear();
        Intent intent = new Intent(DashboardActivity.this,LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }
}