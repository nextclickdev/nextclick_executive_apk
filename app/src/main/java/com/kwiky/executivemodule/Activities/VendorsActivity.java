package com.kwiky.executivemodule.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.kwiky.executivemodule.Adapters.VendorsAdapter;
import com.kwiky.executivemodule.Helpers.CustomDialog;
import com.kwiky.executivemodule.Helpers.uiHelpers.UImsgs;
import com.kwiky.executivemodule.PojoClass.VendorsPojo;
import com.kwiky.executivemodule.R;
import com.kwiky.executivemodule.utilities.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static com.kwiky.executivemodule.Config.Config.USER_TOKEN;
import static com.kwiky.executivemodule.Config.Config.VENDOR_LIST;
import static com.kwiky.executivemodule.Config.Config.X_AUTH_TOKEN;

public class VendorsActivity extends Activity {
    private RecyclerView vendors_recycelr;
    private ArrayList<VendorsPojo> vendorsList = new ArrayList<>();
    PreferenceManager preferenceManager;
    Context context;
    private TextView tv_novendors;
    VendorsAdapter vendorsAdapter;
    private CustomDialog customDialog;
    private TextView tv_heading;
    private int whichshow;
    private ImageView img_back;
    private ArrayList<VendorsPojo> totalvendorlist;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendors);
        Locale.setDefault(Locale.ENGLISH);
      //  getSupportActionBar().hide();
        tv_novendors=findViewById(R.id.tv_novendors);
        context = VendorsActivity.this;
        customDialog=new CustomDialog(context);
        totalvendorlist=new ArrayList<>();
        preferenceManager=new PreferenceManager(context);

        whichshow=getIntent().getIntExtra("whichshow",0);
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.Iconblue));
        }

        vendors_recycelr=findViewById(R.id.vendors_recycelr);
        tv_heading=findViewById(R.id.tv_heading);
        img_back=findViewById(R.id.img_back);

        tv_heading.setText(getIntent().getStringExtra("title"));

        vendors_recycelr.setLayoutManager(new GridLayoutManager(context,1));

        vendorsAdapter=new VendorsAdapter(context,vendorsList);
        vendors_recycelr.setAdapter(vendorsAdapter);

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        getvendorlist();
    }
    private void getvendorlist() {
        customDialog.show();
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, VENDOR_LIST,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("vendor_list", response);
                        customDialog.dismiss();
                        if (response != null) {
                            try {

                                JSONObject jsonObject = new JSONObject(response);
                                System.out.println("aaaaaaaa vendorlist  "+jsonObject.toString());
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    JSONArray datarray = jsonObject.getJSONArray("data");
                                    for (int i=0;i<datarray.length();i++){
                                        JSONObject jsonObject1=datarray.getJSONObject(i);
                                        VendorsPojo vendorsPojo=new VendorsPojo();
                                        vendorsPojo.setId(jsonObject1.getString("id"));
                                        vendorsPojo.setVendor_user_id(jsonObject1.getString("vendor_user_id"));
                                        vendorsPojo.setCustomer_name(jsonObject1.getString("customer_name"));
                                        vendorsPojo.setName(jsonObject1.getString("name"));
                                        vendorsPojo.setEmail(jsonObject1.getString("email"));
                                        vendorsPojo.setUnique_id(jsonObject1.getString("unique_id"));
                                        vendorsPojo.setLocation_id(jsonObject1.getString("location_id"));
                                        vendorsPojo.setExecutive_id(jsonObject1.getString("executive_id"));
                                        vendorsPojo.setConstituency_id(jsonObject1.getString("constituency_id"));
                                        vendorsPojo.setCategory_id(jsonObject1.getString("category_id"));
                                        vendorsPojo.setDesc(jsonObject1.getString("desc"));
                                        vendorsPojo.setAddress(jsonObject1.getString("address"));
                                        vendorsPojo.setNo_of_banners(jsonObject1.getString("no_of_banners"));
                                        vendorsPojo.setLandmark(jsonObject1.getString("landmark"));
                                        vendorsPojo.setPincode(jsonObject1.getString("pincode"));
                                        vendorsPojo.setSounds_like(jsonObject1.getString("sounds_like"));
                                        vendorsPojo.setCreated_at(jsonObject1.getString("created_at"));
                                        vendorsPojo.setUpdated_at(jsonObject1.getString("updated_at"));
                                        vendorsPojo.setAvailability(jsonObject1.getString("availability"));
                                        vendorsPojo.setVendorstatus(jsonObject1.getString("status"));
                                        vendorsPojo.setImage(jsonObject1.getString("image"));
                                        vendorsPojo.setExecutive_user_id(jsonObject1.getString("executive_user_id"));

                                        try{
                                            vendorsPojo.setShop_address(jsonObject1.getJSONObject("location").getString("address"));
                                        }catch (Exception e){
                                            vendorsPojo.setShop_address(jsonObject1.getString("address"));
                                        }


                                        if (whichshow==1){
                                            //total
                                            totalvendorlist.add(vendorsPojo);
                                        }else if (whichshow==2){
                                            //pending -2
                                            if (jsonObject1.getString("status").equalsIgnoreCase("2")){
                                                totalvendorlist.add(vendorsPojo);
                                            }
                                        }else if (whichshow==3){
                                            //approved -1
                                            if (jsonObject1.getString("status").equalsIgnoreCase("1")){
                                                totalvendorlist.add(vendorsPojo);
                                            }
                                        }else {
                                            totalvendorlist.add(vendorsPojo);
                                        }
                                    }
                                   if (totalvendorlist.size()!=0){
                                       vendorsAdapter.setchenge(totalvendorlist);
                                       tv_novendors.setVisibility(View.GONE);
                                       vendors_recycelr.setVisibility(View.VISIBLE);
                                   }else {
                                       tv_novendors.setVisibility(View.VISIBLE);
                                       vendors_recycelr.setVisibility(View.GONE);
                                       setText(whichshow);
                                   }
                                }else {
                                    tv_novendors.setVisibility(View.VISIBLE);
                                    vendors_recycelr.setVisibility(View.GONE);
                                    setText(whichshow);
                                }
                            } catch (Exception e) {
                                tv_novendors.setVisibility(View.VISIBLE);
                                vendors_recycelr.setVisibility(View.GONE);
                                setText(whichshow);
                                customDialog.dismiss();
                                e.printStackTrace();


                            }
                        } else {
                            tv_novendors.setVisibility(View.VISIBLE);
                            vendors_recycelr.setVisibility(View.GONE);
                            setText(whichshow);
                            customDialog.dismiss();
                            UImsgs.showToast(context, getString(R.string.maintenance));
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        customDialog.dismiss();
                        tv_novendors.setVisibility(View.VISIBLE);
                        vendors_recycelr.setVisibility(View.GONE);
                        UImsgs.showToast(context, getString(R.string.oops));
                        System.out.println("aaaaaaa  111 "+error.getMessage());
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(X_AUTH_TOKEN, preferenceManager.getString(USER_TOKEN));
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void setText(int whichshow) {

        if (whichshow==1){
            //total
            tv_novendors.setText("No vendors found");
        }else if (whichshow==2){
            //pending -2
            tv_novendors.setText("No Pending vendors found");
        }else if (whichshow==3){
            //approved -1
            tv_novendors.setText("No Approved vendors found");
        }
    }
}