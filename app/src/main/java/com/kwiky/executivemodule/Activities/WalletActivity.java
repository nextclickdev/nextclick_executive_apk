package com.kwiky.executivemodule.Activities;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.kwiky.executivemodule.Adapters.TransactionsAdapter;
import com.kwiky.executivemodule.Helpers.CustomDialog;
import com.kwiky.executivemodule.Helpers.NetworkUtil;
import com.kwiky.executivemodule.PojoClass.Transactions;
import com.kwiky.executivemodule.R;
import com.kwiky.executivemodule.utilities.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.kwiky.executivemodule.Config.Config.USER_TOKEN;
import static com.kwiky.executivemodule.Config.Config.WALLETHISTORY;


public class WalletActivity extends AppCompatActivity {

    private RecyclerView txnListRecyclerView;
    private TextView tv_walletBalance,start_date,tv_get,end_date,no_wallet;
    private CustomDialog mCustomDialog;
    ImageView wallet_back;
    private Context mContext;
    private ArrayList<Transactions> transactionlist;
    private PreferenceManager preferenceManager;
    String year,month,day,start_date_str,end_date_str;
    private String fromday="",frommonth="",fromyear="",today="",tomonth="",toyear="";
    private List<Transactions> transactionsList = new ArrayList<>();
    TransactionsAdapter transactionsAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet);
       // getSupportActionBar().hide();

        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.Iconblue));
        }

        Locale.setDefault(Locale.ENGLISH);
        init();

    }
    public void init(){
        mContext= WalletActivity.this;
        txnListRecyclerView=findViewById(R.id.txnListRecyclerView);
        tv_walletBalance=findViewById(R.id.tv_walletBalance);
        wallet_back=findViewById(R.id.wallet_back);
        no_wallet=findViewById(R.id.no_wallet);
        start_date=findViewById(R.id.start_date);
        tv_get=findViewById(R.id.tv_get);
        end_date=findViewById(R.id.end_date);
        mCustomDialog=new CustomDialog(WalletActivity.this);
        txnListRecyclerView.setLayoutManager(new GridLayoutManager(WalletActivity.this,1));
        transactionlist=new ArrayList<>();
        preferenceManager=new PreferenceManager(mContext);
        getDate();
        if (NetworkUtil.isNetworkConnected(WalletActivity.this)) {
            getWallet(start_date_str,end_date_str,"","");
        } else {
            Toast.makeText(WalletActivity.this, ""+getResources().getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
        }

        transactionsAdapter = new TransactionsAdapter(mContext,transactionsList);

        txnListRecyclerView.setHasFixedSize(true);
        txnListRecyclerView.setAdapter(transactionsAdapter);

        wallet_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        start_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datepicker(0);
            }
        });

        end_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((fromyear.equals("")) || (frommonth.equals("")) || (fromday.equals(""))){
                    Toast.makeText(mContext, "Please select startdate", Toast.LENGTH_SHORT).show();
                }else {
                    datepicker(1);
                }
            }
        });

        tv_get.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  int currentitem=orders_view_pager.getCurrentItem();
                //  AddTabsToFragments(currentitem);
                if (NetworkUtil.isNetworkConnected(WalletActivity.this)) {
                    getWallet(start_date_str,end_date_str,"","");
                } else {
                    Toast.makeText(WalletActivity.this, ""+getResources().getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
                }

            }
        });
    }
    public void datepicker(final int i){
        final Calendar c;
        final int mYear;
        final int mMonth;
        final int mDay;
        if (i==0){
            c = Calendar.getInstance(Locale.ENGLISH);
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);
        }else {
            c = Calendar.getInstance(Locale.ENGLISH);
            c.set(Integer.parseInt(fromyear),(Integer.parseInt(frommonth)-1),Integer.parseInt(fromday));
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);
        }
        DatePickerDialog datePickerDialog = new DatePickerDialog(mContext,new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year,
                                  int monthOfYear, int dayOfMonth) {
                if (i==0){
                    if ((monthOfYear+1) <= 9) {
                        frommonth = 0 + "" + (monthOfYear+1);
                    } else {
                        frommonth = "" + (monthOfYear+1);
                    }  if (dayOfMonth <= 9) {
                        fromday = 0 + "" + dayOfMonth;
                    } else {
                        fromday = "" + dayOfMonth;
                    }
                    fromyear=""+year;
                    start_date_str=fromyear+"-"+frommonth+"-"+fromday;
                    start_date.setText(""+start_date_str);
                }else {
                    if ((monthOfYear+1) <= 9) {
                        tomonth = 0 + "" + (monthOfYear+1);
                    } else {
                        tomonth = "" + (monthOfYear+1);
                    }  if (dayOfMonth <= 9) {
                        today = 0 + "" + dayOfMonth;
                    } else {
                        today = "" + dayOfMonth;
                    }
                    toyear=""+year;
                    end_date_str=toyear+"-"+tomonth+"-"+today;
                    //   System.out.println("aaaaaaaa  enddate   "+enddate);

                   //  end_date.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                    end_date.setText(""+end_date_str);
                }
            }
        }, mYear, mMonth, mDay);
        if (i==0){
            Calendar c1=Calendar.getInstance(Locale.ENGLISH);
            c1.set((mYear),mMonth,mDay);
            datePickerDialog.getDatePicker().setMaxDate(c1.getTimeInMillis());
            //  end_date.setText("Choose Date");
        }else {
            Calendar c1=Calendar.getInstance(Locale.ENGLISH);
            Calendar c2=Calendar.getInstance(Locale.ENGLISH);
            int mYear1 = c2.get(Calendar.YEAR);
            int mMonth1 = c2.get(Calendar.MONTH);
            int mDay1 = c2.get(Calendar.DAY_OF_MONTH);

            c2.set((mYear1),mMonth1,mDay1);
            c1.set(Integer.parseInt(fromyear),(Integer.parseInt(frommonth)-1),Integer.parseInt(fromday));
            datePickerDialog.getDatePicker().setMinDate(c1.getTimeInMillis());
            datePickerDialog.getDatePicker().setMaxDate(c2.getTimeInMillis());
        }
        datePickerDialog.show();
    }
    public void getWallet(String start_date_str, String end_date_str, String s, String s1){
        transactionlist.clear();

        Map<String, String> uploadMap = new HashMap<>();

        uploadMap.put("start_date", start_date_str);
        uploadMap.put("end_date", end_date_str);
        uploadMap.put("type", s);
        uploadMap.put("status", s1);

        final JSONObject json = new JSONObject(uploadMap);

        System.out.println("aaaaaaaaa request"+json.toString());
        mCustomDialog.show();

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, WALLETHISTORY,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        mCustomDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("aaaaaaaa jsonobject  "+jsonObject.toString());
                            boolean status=jsonObject.getBoolean("status");
                            int http_code=jsonObject.getInt("http_code");

                            if (status){
                                String message=jsonObject.getString("message");
                                JSONObject dataobj=jsonObject.getJSONObject("data");


                                try{
                                    JSONArray transarray=dataobj.getJSONArray("wallet_transactions");
                                    for (int i=0;i<transarray.length();i++){
                                        JSONObject transobj=transarray.getJSONObject(i);
                                        Transactions transactionsPojo=new Transactions();
                                        transactionsPojo.setId(transobj.getString("id"));
                                        transactionsPojo.setTxn_id(transobj.getString("txn_id"));
                                        transactionsPojo.setAccount_user_id(transobj.getString("account_user_id"));
                                        transactionsPojo.setAmount(transobj.getString("amount"));
                                        transactionsPojo.setType(transobj.getString("type"));
                                        transactionsPojo.setBalance(transobj.getString("balance"));
                                        transactionsPojo.setEcom_order_id(transobj.getString("ecom_order_id"));
                                        transactionsPojo.setMessage(transobj.getString("message"));
                                        transactionsPojo.setCreated_at(transobj.getString("created_at"));
                                        transactionsPojo.setUpdated_at(transobj.getString("updated_at"));
                                        transactionsPojo.setStatus(transobj.getString("status"));
                                        transactionlist.add(transactionsPojo);

                                    }
                                    transactionsAdapter.setrefresh(transactionlist);

                                }catch (JSONException e1){
                                }

                                if(transactionlist==null || transactionlist.size()==0)
                                {
                                    no_wallet.setVisibility(View.VISIBLE);
                                    txnListRecyclerView.setVisibility(View.GONE);
                                }

                                try{
                                    JSONObject paymentobj=dataobj.getJSONObject("user");
                                    tv_walletBalance.setText("₹ "+paymentobj.getString("wallet"));
                                }catch (JSONException e2){

                                }
                            }

                        } catch (JSONException e) {
                            Toast.makeText(mContext, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mCustomDialog.dismiss();
                Toast.makeText(mContext, ""+error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());


                no_wallet.setVisibility(View.VISIBLE);
                txnListRecyclerView.setVisibility(View.GONE);
            }
        })
        {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                System.out.println("aaaaaaaaaa  error  " + preferenceManager.getString(USER_TOKEN));
                map.put("X_AUTH_TOKEN",preferenceManager.getString(USER_TOKEN));

                return map;
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return json.toString() == null ? null : json.toString().getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }
        public void getDate(){
            Calendar calander = Calendar.getInstance(Locale.ENGLISH);
            int mday = calander.get(Calendar.DAY_OF_MONTH);
            int cMonth = calander.get(Calendar.MONTH) + 1;
            year =""+ calander.get(Calendar.YEAR);
            if ((cMonth) <= 9) {
                month = 0 + "" + (cMonth);
            } else {
                month = "" + (cMonth);
            }  if (mday <= 9) {
                day = 0 + "" + mday;
            } else {
                day = "" + mday;
            }

            start_date_str=year+"-"+month+"-"+day;
            end_date_str=year+"-"+month+"-"+day;

            start_date.setText(""+start_date_str);
            end_date.setText(""+end_date_str);

            SimpleDateFormat mdformat = new SimpleDateFormat("HH:mm:ss");
            String strDate = "Current Time : " + mdformat.format(calander.getTime());
          /* int cHour = calander.get(Calendar.Ho);
            int cMinute = calander.get(Calendar.MINUTE);
            int cSecond = calander.get(Calendar.SECOND);
    */
        }
}