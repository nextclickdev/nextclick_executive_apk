package com.kwiky.executivemodule.Activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Html;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.material.textfield.TextInputEditText;
import com.google.zxing.WriterException;
import com.kwiky.executivemodule.Config.Config;
import com.kwiky.executivemodule.Helpers.CustomDialog;
import com.kwiky.executivemodule.Helpers.NetworkUtil;
import com.kwiky.executivemodule.Helpers.Utility;
import com.kwiky.executivemodule.R;
import com.kwiky.executivemodule.authentication.UIMsgs;
import com.kwiky.executivemodule.models.Exicutivetypes;
import com.kwiky.executivemodule.utilities.AppConstants;
import com.kwiky.executivemodule.utilities.PreferenceManager;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import androidmads.library.qrgenearator.QRGContents;
import androidmads.library.qrgenearator.QRGEncoder;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.Manifest.permission.CAMERA;
import static com.kwiky.executivemodule.Activities.RegisterVendorActivity.UPDATE_LOCATION;
import static com.kwiky.executivemodule.Config.Config.APP_ID;
import static com.kwiky.executivemodule.Config.Config.APP_ID_VALUE;
import static com.kwiky.executivemodule.Config.Config.MAINTENANCE;
import static com.kwiky.executivemodule.Config.Config.OOPS;
import static com.kwiky.executivemodule.Config.Config.PROFILE;
import static com.kwiky.executivemodule.Config.Config.PROFILEUPDATE;
import static com.kwiky.executivemodule.Config.Config.REGISTER;
import static com.kwiky.executivemodule.Config.Config.USER_PROFILE;
import static com.kwiky.executivemodule.Config.Config.USER_TOKEN;
import static com.kwiky.executivemodule.Config.Config.X_AUTH_TOKEN;


public class ProfileActivity extends Activity implements View.OnClickListener, LocationListener {

    private TextView tv_ack_joindate,tv_conistance,et_firstname,et_back_firstname,tv_edit,tv_userid,tv_heading,et_mobilenumber,et_emailid,executive_type;
    private ImageView img_download,back_imageView,img_refresh,img_adhar,img_bankpassbook,img_back_aadhar,img_back;
    ImageView profileImage_edit,img_qrcode;
    private TextInputEditText tv_address,et_lastnamename,et_address,et_adharnumber;
    private boolean editview=false;
    private LinearLayout layout_download,layout_back_download;
    private String userChoosenTask,base64image="";
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    Context mContext;
    private boolean isCameraPermissionGranted;
    PreferenceManager preferenceManager;
    Double lattitude, longitude;
    LocationManager locationManager;
    private int whichimage;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;
    private Bitmap bm_bankpasbook,bm_front_adhar,bm_back_adhar;
    private String adhar64="",bankpas64="",profile64="",adhar_front64="",adhar_back64="";
    private CustomDialog customDialog;
    String unique_id,firstname,lastname,status;
    private CircleImageView profileImage_main;
    Bitmap bitmap;
    QRGEncoder qrgEncoder;
    private ArrayList<Exicutivetypes> exicutive_types;
    String executiveTypeId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_new);
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.Iconblue));
        }
        init();
        getTypes();
    }
    public void init(){
        tv_edit=findViewById(R.id.tv_edit);
        back_imageView=findViewById(R.id.back_imageView);
        profileImage_edit=findViewById(R.id.profileImage_edit);
        tv_userid=findViewById(R.id.tv_userid);
        et_firstname=findViewById(R.id.et_firstname);
        et_back_firstname=findViewById(R.id.et_back_firstname);
        et_lastnamename=findViewById(R.id.et_lastnamename);
        et_mobilenumber=findViewById(R.id.et_mobilenumber);
        et_emailid=findViewById(R.id.et_emailid);
        executive_type=findViewById(R.id.executive_type);
        img_refresh=findViewById(R.id.img_refresh);
        tv_address=findViewById(R.id.tv_address);
        et_address=findViewById(R.id.et_address);
        et_adharnumber=findViewById(R.id.et_adharnumber);
        img_adhar=findViewById(R.id.img_adhar);
        img_bankpassbook=findViewById(R.id.img_bankpassbook);
        img_back_aadhar=findViewById(R.id.img_back_aadhar);
        tv_heading=findViewById(R.id.tv_heading);
        profileImage_main=findViewById(R.id.profileImage_main);
        img_qrcode=findViewById(R.id.img_qrcode);
        img_back=findViewById(R.id.img_back);
        img_download=findViewById(R.id.img_download);
        layout_download=findViewById(R.id.layout_download);
        layout_back_download=findViewById(R.id.layout_back_download);
        tv_conistance=findViewById(R.id.tv_conistance);
        tv_ack_joindate=findViewById(R.id.tv_ack_joindate);

      //  tv_edit.setVisibility(View.GONE);

        String joing_date=getIntent().getStringExtra("joing_date");


        tv_ack_joindate.setText(""+joing_date);



        customDialog=new CustomDialog(ProfileActivity.this);
       // enable(et_firstname,0);
      //  enable(et_lastnamename,0);
      //  enable(et_mobilenumber,0);
      //  enable(et_emailid,0);
        enable(tv_address,0);
        enable(et_address,0);
        enable(et_adharnumber,0);

        mContext = getApplicationContext();
        preferenceManager = new PreferenceManager(mContext);
        preferenceManager.putBoolean(getString(R.string.refresh_profile), false);
        if (NetworkUtil.isNetworkConnected(ProfileActivity.this)) {
            fetchUserDetails();
        } else {
            Toast.makeText(ProfileActivity.this, ""+getResources().getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
        }

      //  tv_edit.setOnClickListener(this);
        back_imageView.setOnClickListener(this);
       // profileImage_edit.setOnClickListener(this);
      //  img_refresh.setOnClickListener(this);
      //  img_adhar.setClickable(false);
       // img_back_aadhar.setClickable(false);
       // img_bankpassbook.setClickable(false);

       // img_adhar.setEnabled(false);
      //  img_back_aadhar.setEnabled(false);
       // img_bankpassbook.setEnabled(false);


     /*  img_adhar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage(101);
            }
        });
        img_back_aadhar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage(102);
            }
        }); img_bankpassbook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage(2);
            }
        });*/
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        img_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                File file = saveBitMap(ProfileActivity.this, layout_download);//which view you want to pass that view as parameter
                File file1 = saveBitMap(ProfileActivity.this, layout_back_download);//which view you want to pass that view as parameter
                if (file != null) {
                    Log.i("TAG", "Drawing saved to the gallery!");
                } else {
                    Log.i("TAG", "Oops! Image could not be saved.");
                }

                if (file1 != null) {
                    Log.i("TAG", "Drawing saved to the gallery!");
                } else {
                    Log.i("TAG", "Oops! Image could not be saved.");
                }



               /* layout_download.setDrawingCacheEnabled(true);

                layout_download.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                        View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));

                layout_download.layout(0, 0, layout_download.getMeasuredWidth(), layout_download.getMeasuredHeight());

                layout_download.buildDrawingCache(true);
                Bitmap b = Bitmap.createBitmap(layout_download.getDrawingCache());
                layout_download.setDrawingCacheEnabled(false); // clear drawing cache

                OutputStream fOut = null;
                Uri outputFileUri;
                try {
                    File root = new File(Environment.getExternalStorageDirectory()
                            + File.separator + "NextClick" + File.separator);
                    root.mkdirs();
                    File sdImageMainDirectory = new File(root, "myidcard.jpg");
                    outputFileUri = Uri.fromFile(sdImageMainDirectory);
                    fOut = new FileOutputStream(sdImageMainDirectory);
                } catch (Exception e) {
                    Toast.makeText(ProfileActivity.this, "Error occured. Please try again later.",
                            Toast.LENGTH_SHORT).show();
                }

                try {
                    b.compress(Bitmap.CompressFormat.PNG, 100, fOut);
                    fOut.flush();
                    fOut.close();
                } catch (Exception e) {
                }
*/
            }
        });
    }
    private File saveBitMap(Context context, View drawView){
        customDialog.show();
        customDialog.setMessage("Downloading...");
        File pictureFileDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"NextClick");
        if (!pictureFileDir.exists()) {
            boolean isDirectoryCreated = pictureFileDir.mkdirs();
            if(!isDirectoryCreated){
                System.out.println("aaaaaaaaa Can't create directory to save the image");
                customDialog.dismiss();
                return null;
            }

        }
        String filename = pictureFileDir.getPath() +File.separator+ System.currentTimeMillis()+".jpg";
        File pictureFile = new File(filename);
        Bitmap bitmap =getBitmapFromView(drawView);
        try {
            pictureFile.createNewFile();
            FileOutputStream oStream = new FileOutputStream(pictureFile);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, oStream);
            oStream.flush();
            oStream.close();
            Toast.makeText(context, "Download Sucessfully...", Toast.LENGTH_SHORT).show();
            customDialog.dismiss();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("aaaaaaaa There was an issue saving the image.");
            customDialog.dismiss();
        }
        scanGallery( context,pictureFile.getAbsolutePath());
        return pictureFile;
    }
    //create bitmap from view and returns it
    private Bitmap getBitmapFromView(View view) {
        //Define a bitmap with the same size as the view
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(),Bitmap.Config.ARGB_8888);
        //Bind a canvas to it
        Canvas canvas = new Canvas(returnedBitmap);
        //Get the view's background
        Drawable bgDrawable =view.getBackground();
        if (bgDrawable!=null) {
            //has background drawable, then draw it on the canvas
            bgDrawable.draw(canvas);
        }   else{
            //does not have background drawable, then draw white background on the canvas
            canvas.drawColor(Color.WHITE);
        }
        // draw the view on the canvas
        view.draw(canvas);
        //return the bitmap
        return returnedBitmap;
    }
    // used for scanning gallery
    private void scanGallery(Context cntx, String path) {
        try {
            MediaScannerConnection.scanFile(cntx, new String[] { path },null, new MediaScannerConnection.OnScanCompletedListener() {
                public void onScanCompleted(String path, Uri uri) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void selectImage(int whichimage) {
        this.whichimage=whichimage;
        final CharSequence[] items = {getString(R.string.take_photo), getString(R.string.choose_from_library),
                getString(R.string.cancel) };

        AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this);
        builder.setTitle(getString(R.string.add_photo));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result= Utility.checkPermission(ProfileActivity.this);

                if (items[item].equals(getString(R.string.take_photo))) {
                    userChoosenTask =getString(R.string.take_photo);
                    if(result) {
                        if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                            //requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
                            isCameraPermissionGranted = true;
                            ActivityCompat.requestPermissions(ProfileActivity.this, new String[]{Manifest.permission.CAMERA},
                                    MY_CAMERA_PERMISSION_CODE);
                        } else {
                            // cameraIntent();
                            isCameraPermissionGranted = false;
                            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(cameraIntent, REQUEST_CAMERA);
                        }
                    }
                    else
                        isCameraPermissionGranted=true;
                } else if (items[item].equals(getString(R.string.choose_from_library))) {
                    userChoosenTask =getString(R.string.choose_from_library);
                    if(result)
                        galleryIntent();

                } else if (items[item].equals(getString(R.string.cancel))){
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        switch (requestCode) {

            case MY_CAMERA_PERMISSION_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(cameraIntent, REQUEST_CAMERA);
                }
                break;

            default:
                int permissionLocation = ContextCompat.checkSelfPermission(ProfileActivity.this,
                        Manifest.permission.ACCESS_FINE_LOCATION);
                if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
                    getLocation();
                }
                if(isCameraPermissionGranted) {
                    int cameraLocation = ContextCompat.checkSelfPermission(ProfileActivity.this,
                            CAMERA);
                    if (cameraLocation == PackageManager.PERMISSION_GRANTED) {
                        isCameraPermissionGranted =false;
                        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, REQUEST_CAMERA);
                    }

                }
                break;
        }
    }
    @Override
    public void onClick(View v) {
        System.out.println("aaaaaaaa id "+v.getId());
        switch (v.getId()){
           /* case R.id.tv_edit:
                if (editview){
                    updateApi();
                }else {
                    editview=true;
                    //  tv_edit.setVisibility(View.INVISIBLE);
                    tv_edit.setText("Save");
                    tv_heading.setText("Edit Profile");
                    //enable(et_firstname,1);
                    enable(et_lastnamename,1);
                   // enable(et_mobilenumber,1);
                  //  enable(et_emailid,1);
                    enable(tv_address,1);
                    enable(et_address,1);
                    enable(et_adharnumber,1);

                    img_adhar.setClickable(true);
                    img_back_aadhar.setClickable(true);
                    img_bankpassbook.setClickable(true);

                    img_adhar.setEnabled(true);
                    img_back_aadhar.setEnabled(true);
                    img_bankpassbook.setEnabled(true);


                }
                System.out.println("aaaaaaaa  edit click");


                break;*/
            case R.id.back_imageView:
                if (editview==true){
                    editview=false;
                    tv_heading.setText("Profile View");
                   // tv_edit.setVisibility(View.VISIBLE);
                  //  enable(et_firstname,0);
                    enable(et_lastnamename,0);
                  //  enable(et_mobilenumber,0);
                  //  enable(et_emailid,0);
                    enable(tv_address,0);
                    enable(et_address,0);
                    enable(et_adharnumber,0);
                }else {
                    finish();
                }

                break;
            case R.id.profileImage_edit:
                selectImage();
                break;

               /* case R.id.img_refresh:
                    getLocation();
                break;*/
        }
    }
    public void enable(TextInputEditText editText, int position){
        /*if (position==0){
            editText.setClickable(false);
            editText.setEnabled(false);
        }else {
            editText.setClickable(true);
            editText.setEnabled(true);
        }*/

    }
    private void selectImage() {

        final CharSequence[] items = { "Take Photo", "Choose from Library",
                "Cancel" };

        AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result= Utility.checkPermission(ProfileActivity.this);

                if (items[item].equals("Take Photo")) {
                    userChoosenTask ="Take Photo";
                    if(result)
                        cameraIntent();

                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask ="Choose from Library";
                    if(result)
                        galleryIntent();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }
    private void galleryIntent()
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"),SELECT_FILE);
    }

    private void cameraIntent()
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }
   /* @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }*/
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            // onCaptureImageResult(data);
             if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }
    public String convert(Bitmap bitmap) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, outputStream);
        return Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);
    }
    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm=null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
                base64image=convert(bm);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 50, stream);
        byte[] imageInByte = stream.toByteArray();
        long lengthbmp = imageInByte.length/1024;

        setimage(whichimage,bm);
        System.out.println("aaaaaaaaaa  size "+lengthbmp);
      //  profileImage_edit.setImageBitmap(bm);

    }
    public void setimage(int whichimage,Bitmap bitmap){
        System.out.println("aaaaaaaaa bitmap check  "+bitmap.toString());
        switch (whichimage){
            case 2:
                img_bankpassbook.setImageBitmap(bitmap);
                bm_bankpasbook=bitmap;
                bankpas64=convert(bitmap);
                break;
            case 101:
               // img_adhar.setImageBitmap(bitmap);
                bm_front_adhar=bitmap;
                adhar_front64=convert(bitmap);
                break;
            case 102:
               // img_back_aadhar.setImageBitmap(bitmap);
                bm_back_adhar=bitmap;
                adhar_back64=convert(bitmap);
                break;
        }
    }
    private void onCaptureImageResult(Intent data) {

        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 50, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        fixOrientation(thumbnail);

       // profileImage_edit.setImageBitmap(thumbnail);


        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        byte[] imageInByte = stream.toByteArray();
        long lengthbmp = imageInByte.length/1024;

        System.out.println("aaaaaaaaaa  size "+lengthbmp);
      //  setimage(whichimage,thumbnail);

    }
    public void fixOrientation(Bitmap bitmap) {
        if (bitmap.getWidth() > bitmap.getHeight()) {
            Matrix matrix = new Matrix();
            matrix.postRotate(270);
            bitmap = Bitmap.createBitmap(bitmap , 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
         //   profileImage_edit.setImageBitmap(bitmap);
            setimage(whichimage,bitmap);
        }
    }

    public static Bitmap getRotateImage(String photoPath, Bitmap bitmap) throws IOException {
        ExifInterface ei = new ExifInterface(photoPath);
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED);

        Bitmap rotatedBitmap = null;
        switch (orientation) {

            case ExifInterface.ORIENTATION_ROTATE_90:
                rotatedBitmap = rotateImage(bitmap, 90);
                break;

            case ExifInterface.ORIENTATION_ROTATE_180:
                rotatedBitmap = rotateImage(bitmap, 180);
                break;

            case ExifInterface.ORIENTATION_ROTATE_270:
                rotatedBitmap = rotateImage(bitmap, 270);
                break;

            case ExifInterface.ORIENTATION_NORMAL:
            default:
                rotatedBitmap = bitmap;
        }

        return rotatedBitmap;

    }


    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

    private void fetchUserDetails() {
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, USER_PROFILE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.d("profile r response", response);

                    JSONObject jsonObject = new JSONObject(response);
                    System.out.println("aaaaaaaaaaa response "+jsonObject.toString());
                    boolean status = jsonObject.getBoolean("status");
                    Integer status_code = jsonObject.getInt("http_code");
                    if (status || status_code == 200) {
                        JSONObject jsonObjectData = jsonObject.getJSONObject("data");
                        System.out.println("aaaaaa  responce  " + jsonObject);



                       /* status = jsonObjectData.getString("status");
                        if (status.equalsIgnoreCase("1")) {
                            tv_edit.setVisibility(View.GONE);
                        } else {
                            tv_edit.setVisibility(View.VISIBLE);
                        }*/

                        firstname = jsonObjectData.getString("first_name");
                        lastname = jsonObjectData.getString("last_name");

                      /*  String joing_date=jsonObjectData.getString("created_at");
                        String[] datespilt=joing_date.split(" ");
                        String[] datesspilt=datespilt[0].split("-");


                        tv_ack_joindate.setText(""+datesspilt[2]+"/"+datesspilt[1]+"/"+datesspilt[0]);
*/
                        et_firstname.setText(firstname+" " + lastname);//first_name
                        et_back_firstname.setText(firstname+" " + lastname);//first_name
                     //   et_lastnamename.setText("" + jsonObjectData.getString("last_name"));
                        et_emailid.setText("" + jsonObjectData.getString("email"));
                        et_mobilenumber.setText("+91 " + jsonObjectData.getString("phone"));

                        //et_adharnumber.setText(""+jsonObjectData.getString("aadhar_number"));
                        tv_userid.setText("NXC "+jsonObjectData.getJSONObject("executive_address").getString("user_id"));
                        tv_conistance.setText(jsonObjectData.getJSONObject("executive_address").getString("location"));
                        // unique_id=jsonObjectData.getString("unique_id");
                       // JSONObject locationbject = jsonObjectData.getJSONObject("location");
                        //tv_address.setText("" + locationbject.getString("address"));

                        try {
                            if (jsonObjectData.has("executive_address")) {
                                JSONObject addressObj = jsonObjectData.getJSONObject("executive_address");
                                //tv_address.setText("" + addressObj.getString("location"));
                                if (addressObj.has("executive_type_id")) {
                                    executiveTypeId = addressObj.getString("executive_type_id");
                                    if(exicutive_types != null && exicutive_types.size() > 0)
                                    {
                                        for (int i = 0; i < exicutive_types.size(); i++) {
                                            if (exicutive_types.get(i).getId().equals(executiveTypeId)) {
                                                executive_type.setText(exicutive_types.get(i).getExecutive_type());
                                                break;
                                            }
                                        }
                                    }
                                    else
                                        executive_type.setText(executiveTypeId);
                                }
                            }
                        }catch (Exception ex){}

                        WindowManager manager = (WindowManager) getSystemService(WINDOW_SERVICE);

                        // initializing a variable for default display.
                        Display display = manager.getDefaultDisplay();

                        // creating a variable for point which
                        // is to be displayed in QR Code.
                        Point point = new Point();
                        display.getSize(point);

                        // getting width and
                        // height of a point
                        int width = point.x;
                        int height = point.y;

                        // generating dimension from width and height.
                        int dimen = width < height ? width : height;
                        dimen = dimen * 3 / 4;

                        // setting this dimensions inside our qr code
                        // encoder to generate our qr code.
                        qrgEncoder = new QRGEncoder(firstname+" "+lastname, null, QRGContents.Type.TEXT, dimen);
                        try {
                            // getting our qrcode in the form of bitmap.
                            bitmap = qrgEncoder.encodeAsBitmap();
                            // the bitmap is set inside our image
                            // view using .setimagebitmap method.
                            img_qrcode.setImageBitmap(bitmap);
                        } catch (WriterException e) {
                            // this method is called for
                            // exception handling.
                            Log.e("Tag", e.toString());
                        }

                        try {
                            if(jsonObjectData.getString("profile_image")!=null) {

                                Picasso.get().load(jsonObjectData.getString("profile_image"))
                                        .networkPolicy(NetworkPolicy.NO_CACHE)
                                        .memoryPolicy(MemoryPolicy.NO_CACHE)
                                        .into(profileImage_main);


                               /* Glide.with(ProfileActivity.this)
                                        .load(jsonObjectData.getString("profile_image"))
                                        .placeholder(R.drawable.no_image)
                                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                                        .skipMemoryCache(true)
                                        .into(profileImage_main);*/
                                System.out.println("aaaaaaaaaa profileimg  "+jsonObjectData.getString("profile_image"));
                            }
                            else
                            {
                                Glide.with(ProfileActivity.this)
                                        .load(R.drawable.no_image)
                                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                                        .skipMemoryCache(true)
                                        .into(profileImage_main);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                   /* try
                    {

                        Glide.with(ProfileActivity.this)
                                .load(jsonObjectData.getString("aadhar_front"))
                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                .skipMemoryCache(true)
                                .placeholder(R.drawable.no_image)
                                .into(img_adhar);


                        Glide.with(ProfileActivity.this)
                                .load(jsonObjectData.getString("aadhar_back"))
                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                .skipMemoryCache(true)
                                .placeholder(R.drawable.no_image)
                                .into(img_back_aadhar);


                        Glide.with(ProfileActivity.this)
                                .load(jsonObjectData.getString("passbook"))
                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                .skipMemoryCache(true)
                                .placeholder(R.drawable.no_image)
                                .into(img_bankpassbook);
                    }
                    catch (Exception ex){}*/
                    }

                } catch (JSONException e) {
                    System.out.println("aaaaaaa catch  "+e.getMessage());
                    e.printStackTrace();
                }
                //Toast.makeText(mContext, "responce "+response, Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.d("fetch user error", error.toString());
            }
        }) {
           /* @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                return super.getBody();
            }*/

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put("X_AUTH_TOKEN", preferenceManager.getString(USER_TOKEN));
                System.out.println("aaaaaa token "+ preferenceManager.getString(USER_TOKEN));
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }
    /*Locataion Start*/
    @SuppressLint("MissingPermission")
    void getLocation() {

        try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 5, (LocationListener) this);
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }
    public static String isNull(String str) {
        if(TextUtils.isEmpty(str)||str.equalsIgnoreCase("null"))
            return "";
        else
            return str;
    }

    @Override
    public void onLocationChanged(@NonNull Location location) {
        try {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            /*locationText.setText(locationText.getText() + "\n"+addresses.get(0).getAddressLine(0)+", "+
                    addresses.get(0).getAddressLine(1)+", "+addresses.get(0).getAddressLine(2));
            */
            String address = addresses.get(0).getAddressLine(0) + "";
            String[] arr = address.split(",");
            /*Toast.makeText(this, arr.length+"", Toast.LENGTH_SHORT).show();*/

            Double lat1 = location.getLatitude();
            Double lang1 = location.getLongitude();
            lattitude = lat1;
            longitude = lang1;
            System.out.println("aaaaaaaaa  address 1 "+address);
            tv_address.setText(address);


        } catch (Exception e) {

        }
    }
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    private void updateApi() {
        Map<String, String> dataMap = new HashMap<>();
        dataMap.put("first_name", firstname);
        dataMap.put("unique_id", unique_id);
       // dataMap.put("last_name", et_last_name.getText().toString());
        dataMap.put("mobile", et_mobilenumber.getText().toString());
        dataMap.put("email", et_emailid.getText().toString());
        dataMap.put("aadhar_number", et_adharnumber.getText().toString());
       /* dataMap.put("password", et_password.getText().toString());
        dataMap.put("aadhar_number", et_adhar_number.getText().toString());
        dataMap.put("latitude", ""+wayLatitude);
        dataMap.put("longitude", ""+wayLongitude);
        dataMap.put("geo_lcoation_address", ""+et_location.getText().toString());*/
        //  dataMap.put("aadhar_card_image", ""+adhar64);
        dataMap.put("bank_passbook_image", ""+bankpas64);
       // dataMap.put("profile", ""+profile64);
        dataMap.put("aadhar_card_image_front", ""+adhar_front64);
        dataMap.put("aadhar_card_image_back", ""+adhar_back64);

        JSONObject json = new JSONObject(dataMap);


        System.out.println("aaaaaaaaaa   json  "+json);

        customDialog.show();
        final String data = json.toString();
        RequestQueue requestQueue = Volley.newRequestQueue(ProfileActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, PROFILEUPDATE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    System.out.println("aaaaa  response "+response.toString());
                    try {
                        customDialog.dismiss();
                        JSONObject jsonObject = new JSONObject(response);
                        System.out.println("aaaaaaaaa jsonobject "+jsonObject.toString());
                        boolean status = jsonObject.getBoolean("status");
                        if (status) {
                          //  tv_edit.setText("Edit");
                            editview=false;
                          /*  img_adhar.setClickable(false);
                            img_back_aadhar.setClickable(false);
                            img_bankpassbook.setClickable(false);

                            img_adhar.setEnabled(false);
                            img_back_aadhar.setEnabled(false);
                            img_bankpassbook.setEnabled(false);*/
                            String message = jsonObject.getString("message");
                            Toast.makeText(ProfileActivity.this, ""+message, Toast.LENGTH_SHORT).show();
                           // finish();
                        } else {
                            Toast.makeText(ProfileActivity.this, Html.fromHtml(jsonObject.getString("message")), Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        System.out.println("aaaaaaa catch  "+e.getMessage());
                        customDialog.dismiss();
                        //  UiMsgs.showToast(mContext, "Something went wrong");
                        e.printStackTrace();
                    }
                } else {
                    System.out.println("aaaaaaa catch Server under maintenance ");
                    customDialog.dismiss();
                    // UiMsgs.showToast(mContext, "Server under maintenance");
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customDialog.dismiss();
                System.out.println("aaaaaaa error  "+error.getMessage());
                //   UiMsgs.showToast(mContext, "Something went wrong");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(X_AUTH_TOKEN, preferenceManager.getString(USER_TOKEN));
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void getTypes() {

        RequestQueue requestQueue = Volley.newRequestQueue(ProfileActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Config.ECITIVE_TYPES,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("tax_res", response);
                        if (response != null) {
                            try {

                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    JSONArray dataArray = jsonObject.getJSONArray("data");
                                    if (dataArray.length() > 0) {
                                        exicutive_types = new ArrayList<>();
                                        for (int i = 0; i < dataArray.length(); i++) {
                                            JSONObject categoryObject = dataArray.getJSONObject(i);
                                            Exicutivetypes exicutivetypes=new Exicutivetypes();
                                            exicutivetypes.setId(categoryObject.getString("id"));
                                            if(exicutivetypes.getId().equals(executiveTypeId))
                                            {
                                                executive_type.setText(categoryObject.getString("executive_type"));
                                            }
                                            exicutivetypes.setExecutive_type(categoryObject.getString("executive_type"));
                                            exicutive_types.add(exicutivetypes);
                                        }
                                    }
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            UIMsgs.showToast(ProfileActivity.this, MAINTENANCE);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        UIMsgs.showToast(ProfileActivity.this, OOPS);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }
}