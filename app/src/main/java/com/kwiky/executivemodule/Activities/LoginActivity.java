package com.kwiky.executivemodule.Activities;


import static android.Manifest.permission.READ_SMS;
import static android.Manifest.permission.RECEIVE_SMS;
import static android.Manifest.permission.SEND_SMS;
import static android.view.KeyEvent.KEYCODE_DEL;
import static android.view.View.GONE;

import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.kwiky.executivemodule.Config.Config;
import com.kwiky.executivemodule.Constants.IErrors;
import com.kwiky.executivemodule.Helpers.NetworkUtil;
import com.kwiky.executivemodule.Helpers.Utility;
import com.kwiky.executivemodule.Helpers.Validations;
import com.kwiky.executivemodule.Helpers.netwrokHelpers.ConnectionDetector;
import com.kwiky.executivemodule.Helpers.uiHelpers.IncomingSms;
import com.kwiky.executivemodule.Helpers.uiHelpers.UImsgs;
import com.kwiky.executivemodule.R;
import com.kwiky.executivemodule.authentication.ResetPasswordActivity;
import com.kwiky.executivemodule.authentication.UIMsgs;
import com.kwiky.executivemodule.authentication.VerifyAndCreateAccount;
import com.kwiky.executivemodule.authentication.WaitingActivity;
import com.kwiky.executivemodule.authentication.WelcomeActivity;
import com.kwiky.executivemodule.utilities.PreferenceManager;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.kwiky.executivemodule.Config.Config.APP_ID;
import static com.kwiky.executivemodule.Config.Config.APP_ID_VALUE;
import static com.kwiky.executivemodule.Config.Config.ForgotPassword;
import static com.kwiky.executivemodule.Config.Config.Login;
import static com.kwiky.executivemodule.Config.Config.USER_TOKEN;


public class LoginActivity extends Activity {


    EditText user_id, password;
    Button submit;
    TextView forgotPassword,tvRegister,tv_change_loginType;
    String user_id_str, password_str;
    Map<String, String> mapToParse = new HashMap<>();
    PreferenceManager preferenceManager;
    Context mContext;
    Bundle bundle;
    Double lat, lang;
    ConnectionDetector connectionDetector;
    Validations validations;
    Animation animationShake;
    ProgressDialog progressDialog;

    private ArrayList permissionsToRequest;
    private ArrayList permissions = new ArrayList();
    private final static int ALL_PERMISSIONS_RESULT = 101;

    private static final int CAMERA_REQUEST = 1888;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
      //  getSupportActionBar().hide();
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
        }
        initIdView();
        mContext = getApplicationContext();
        preferenceManager = new PreferenceManager(mContext);
        connectionDetector = new ConnectionDetector(mContext);
        validations = new Validations();
        animationShake = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.shake);

        permissions.add(READ_SMS);
        permissions.add(RECEIVE_SMS);
        permissions.add(SEND_SMS);

        permissionsToRequest = findUnAskedPermissions(permissions);
        if (permissionsToRequest.size() > 0) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions((String[]) permissionsToRequest.toArray(new String[permissionsToRequest.size()]),
                        ALL_PERMISSIONS_RESULT);
            }
        }

        tvRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, VerifyAndCreateAccount.class));//ExicutiveRegisterActivity
            }
        });

        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent registerIntent = new Intent(mContext, ResetPasswordActivity.class);
                startActivity(registerIntent);

/*

               //WindowManager windowManager2 = (WindowManager)getSystemService(WINDOW_SERVICE);
                LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View alertLayout = inflater.inflate(R.layout.forgotpassword, null);

                final EditText mailtext = alertLayout.findViewById(R.id.forgot_mail);
                final TextView ok = alertLayout.findViewById(R.id.ok);

                AlertDialog.Builder alert = new AlertDialog.Builder(LoginActivity.this);
                alert.setIcon(R.mipmap.ic_launcher);
                alert.setTitle("Forgot Password");
                // this is set the view from XML inside AlertDialog
                alert.setView(alertLayout);
                // disallow cancel of AlertDialog on click of back button and outside touch
                alert.setCancelable(true);
                final AlertDialog dialog = alert.create();
                dialog.show();
                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String forgotmail = mailtext.getText().toString().trim();
                        if(forgotmail.length()==0){
                            mailtext.setError("Should Not Be Empty");
                            mailtext.requestFocus();
                        }
                        else if(validations.isValidEmail(forgotmail)){
                            mailtext.setError("Inavalid");
                            mailtext.requestFocus();
                        }
                        else{
                            if (NetworkUtil.isNetworkConnected(LoginActivity.this)) {
                                forgotPassword(forgotmail,dialog);
                            } else {
                                Toast.makeText(LoginActivity.this, ""+getResources().getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
                            }


                        }

                    }
                });
 */
            }
        });


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (connectionDetector.isConnectingToInternet()) {
                    // startActivity(new Intent(LoginActivity.this,DashboardActivity.class));
                    fetcher();
                    if (!isOtpLogin)
                        login(password_str);
                    else if (validator(user_id_str, password_str))
                        sendOTP(user_id_str);

                } else {
                    Toast.makeText(mContext, "" + getResources().getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
        builder.setTitle(R.string.app_name);
        builder.setIcon(R.mipmap.ic_launcher);
        builder.setMessage("Do you want to exit?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void forgotPassword(final String forgotMail, final AlertDialog adialog){
        Map<String,String> datamap = new HashMap<>();
        datamap.put("identity",forgotMail);
        JSONObject json = new JSONObject(datamap);
        final String data = json.toString();
        adialog.dismiss();
        progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setIcon(R.drawable.nextclick_logo_black);
        progressDialog.setMessage("Please wait while sending.....");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setProgress(0);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ForgotPassword, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if(response!=null){
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        Boolean status = jsonObject.getBoolean("status");
                        String message = jsonObject.getString("message");
                        if(status){
                            progressDialog.dismiss();

                            AlertDialog.Builder alert = new AlertDialog.Builder(LoginActivity.this);
                            alert.setIcon(R.mipmap.ic_launcher);
                            alert.setTitle("NextClick - Reply");
                            alert.setMessage("Reset Link Has Been Sent To Your Mail");
                            alert.setCancelable(true);
                            AlertDialog dialog = alert.create();
                            dialog.show();

                        }
                        else{
                            progressDialog.dismiss();
                            AlertDialog.Builder alert = new AlertDialog.Builder(LoginActivity.this);
                            alert.setIcon(R.mipmap.ic_launcher);
                            alert.setTitle("NextClick - Reply");
                            alert.setMessage(message);
                            alert.setCancelable(true);
                            AlertDialog dialog = alert.create();
                            dialog.show();
                        }

                    }
                    catch (Exception e){

                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UImsgs.showToast(mContext,error.toString());
            }
        }){
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }


    public void fetcher() {

        user_id_str = user_id.getText().toString().trim();
        password_str = password.getText().toString().trim();


    }

    public boolean validator(String user_id_str, String password_str) {
        boolean validity = true;
        if (user_id_str.length() == 0) {
            user_id.setError(getResources().getString(R.string.empty));
            user_id.requestFocus();
            validity = false;
        }
        else if (user_id_str.length() < 10) {
            user_id.setError(getResources().getString(R.string.invalid));
            user_id.requestFocus();
            validity = false;
        }
        else if (user_id_str.startsWith("0") || (user_id_str.startsWith("1")) || user_id_str.startsWith("2")||
                user_id_str.startsWith("3")|| user_id_str.startsWith("4")||user_id_str.startsWith("5")){
            user_id.setError(getResources().getString(R.string.invalid));
            user_id.requestFocus();
            validity = false;
        }
        else if (!isOtpLogin && password_str.length() == 0) {
            password.setError(getResources().getString(R.string.empty));
            password.requestFocus();
            validity = false;
        }

        return validity;
    }


    public void loginPost(JSONObject json,final String input) {
        final String data = json.toString();
        progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setIcon(R.drawable.nextclick_logo_black);
        progressDialog.setMessage("Please wait while logging.....");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setProgress(0);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Login, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonData = new JSONObject(response);
                    System.out.println("aaaaaaa login  "+jsonData.toString());
                    String status = jsonData.getString("status");
                    String messagae = jsonData.getString("message");
                    if (status.equalsIgnoreCase("true")) {

                        progressDialog.dismiss();
                        JSONObject dataObject = jsonData.getJSONObject("data");
                        String token = dataObject.getString("token");
                        preferenceManager.putString(USER_TOKEN,token);

                        if(dataObject.has("approval_status")) {
                            String approval_status = dataObject.getString("approval_status");
                            preferenceManager.putString("approval_status", null);
                            preferenceManager.putString(USER_TOKEN, token);

                            if (approval_status.equals("1"))//1-active,2-in active, 3- pending
                            {
                                //preferenceManager.putString(USER_TOKEN, token);
                                preferenceManager.putString("approval_status", approval_status);

                                startActivity(new Intent(LoginActivity.this,DashboardActivity.class));
                                finish();
                            }
                            else if (approval_status.equals("3"))
                            {
                                Intent registerIntent = new Intent(mContext, WelcomeActivity.class);
                                startActivity(registerIntent);
                            }
                            else
                            {
                                Intent intent = new Intent(mContext, WaitingActivity.class);
                                intent.putExtra("approval_status",approval_status);
                                startActivity(intent);
                                //Toast.makeText(mContext, "Your status is currently in-active, please contact the nextclick admin team.", Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                            Toast.makeText(mContext, "approval_status is not coming from the backend api", Toast.LENGTH_SHORT).show();

                    } else {
                        progressDialog.dismiss();
                        try {
                            //  if (isOtpLogin)
                            //  showVerifyOTPDialog(input, user_id_str, false);
                            //else


                            if(messagae.equals("INCORRECT_PASSWORD"))
                            {
                                messagae="Incorrect password";
                            }
                            else if(messagae.equals("USER_NOT_EXISTS"))
                            {
                                messagae="You don't have the account please sign up";
                            }
                            else if(messagae.equals("INCORRECT_OTP"))
                            {
                                if(isOtpLogin) {
                                    showVerifyOTPDialog(input, user_id_str, false);
                                    return;
                                }
                                else
                                    messagae="Incorrect otp";
                            }
                            UImsgs.showToast(mContext, messagae);
                                /*if(jsonObject.getString("data").contains("Incorrect Login"))
                                {
                                    Toasty.error(mContext, "No details found with given mobile no", Toast.LENGTH_SHORT).show();
                                }
                                else
                                    Toasty.error(mContext, Html.fromHtml(jsonObject.getString("data")), Toast.LENGTH_SHORT).show();*/
                        }
                        catch (Exception ex) {
                            UImsgs.showToast(mContext, messagae);
                        }
                        //Toast.makeText(LoginActivity.this, "Invalid Credentials", Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    System.out.println("aaaaaa error "+e.getMessage());
                    UImsgs.showToast(mContext, String.valueOf(e));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("aaaaaa error "+error.getMessage());
                //Toast.makeText(LoginActivity.this, error+"", Toast.LENGTH_SHORT).show();

            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(APP_ID, APP_ID_VALUE);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        requestQueue.add(stringRequest);

    }

    public void initIdView() {

        user_id = (EditText) findViewById(R.id.user_id);
        password = (EditText) findViewById(R.id.password);
        submit = (Button) findViewById(R.id.sign_in_button);
        forgotPassword = (TextView) findViewById(R.id.forgot_password);
        tvRegister = (TextView) findViewById(R.id.tvRegister);
        tv_change_loginType = (TextView) findViewById(R.id.tv_change_loginType);
    }

    private boolean isValidateFields() {

        if (validations.isBlank(user_id)) {
            user_id.startAnimation(animationShake);
            user_id.setError(IErrors.EMPTY);
            user_id.requestFocus();
            return false;
        } /*else if (validations.isValidEmail(user_id.getText().toString())) {
            user_id.startAnimation(animationShake);
            user_id.setError("Please Enter Valid Email");
            user_id.requestFocus();
            return false;

        }*/ else if (validations.isBlank(password)) {
            password.startAnimation(animationShake);
            password.setError(IErrors.EMPTY);
            password.requestFocus();
            return false;
        }
        /*else if (password.getText().length()<2)
        {
            password.startAnimation(animationShake);
            password.setError("Password should minimum 4 digit");
            password.requestFocus();
            return false;
        }*/

        return true;
    }


    Boolean isOtpLogin =true;
    public void changeLoginType(View view)
    {
        if(tv_change_loginType.getText().toString().equals(getString(R.string.login_with_password)))
        {
            isOtpLogin =false;
            tv_change_loginType.setText(getString(R.string.login_with_otp));
            password.setVisibility(View.VISIBLE);
        }
        else
        {
            isOtpLogin =true;
            tv_change_loginType.setText(getString(R.string.login_with_password));
            password.setVisibility(GONE);
        }
    }



    private void sendOTP(final String mobile) {

        final ProgressDialog progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setIcon(R.drawable.nextclick_logo_black);
        progressDialog.setMessage("Please wait while sending the otp.....");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setProgress(0);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        Map<String, String> fcmMap = new HashMap<>();
        fcmMap.put("mobile", mobile);
        JSONObject json = new JSONObject(fcmMap);
        System.out.println("aaaaaaa request "+json.toString());
        final String data = json.toString();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Config.SEND_OTP, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (response != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        System.out.println("aaaaaaa response "+jsonObject.toString());
                        boolean status = jsonObject.getBoolean("status");
                        if (status) {
                            //showVerifyOTPDialog(jsonObject.getJSONObject("data").getString("otp"),mobile,true);
                            showVerifyOTPDialog(null,mobile,false);
                        } else {
                            UIMsgs.showToast(mContext, Html.fromHtml(jsonObject.getString("message")).toString());
                        }
                    } catch (Exception e) {
                        UIMsgs.showToast(mContext, "Something went wrong", Toast.LENGTH_SHORT);
                        e.printStackTrace();
                    } finally {
                        progressDialog.dismiss();
                    }
                } else {
                    progressDialog.dismiss();
                    UIMsgs.showToast(mContext, "Server under maintenance", Toast.LENGTH_SHORT);
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                progressDialog.dismiss();
                System.out.println("aaaaaaa error  "+error.getMessage());
                UIMsgs.showToast(mContext, "Something went wrong");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                //  map.put(AUTH_TOKEN, preferenceManager.getString(TOKEN_KEY));
                map.put(APP_ID, APP_ID_VALUE);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }
    GenericTextWatcher watcher1,watcher2,watcher3,watcher4,watcher5,watcher6;
    EditText editTextone,editTexttwo,editTextthree, editTextfour,editTextFive,editTextSix;
    Dialog otpDialog;
    private void showVerifyOTPDialog(String otp, final String mobilenumber, boolean isTest) {
        if(otpDialog ==null) {
            otpDialog = new Dialog(this);
            otpDialog.setContentView(R.layout.verify_otp_dialog);


            LinearLayout layout_root = (LinearLayout) otpDialog.findViewById(R.id.layout_root);
            DisplayMetrics displayMetrics = mContext.getResources().getDisplayMetrics();
            ViewGroup.LayoutParams layoutParams = layout_root.getLayoutParams();
            layoutParams.width = displayMetrics.widthPixels - 50;
            layout_root.setLayoutParams(layoutParams);

            Button applyButton = (Button) otpDialog.findViewById(R.id.dialogButtonOK);
            ImageView closeButton = (ImageView) otpDialog.findViewById(R.id.closeButton);
            editTextone = (EditText) otpDialog.findViewById(R.id.editTextone);
            editTexttwo = (EditText) otpDialog.findViewById(R.id.editTexttwo);
            editTextthree = (EditText) otpDialog.findViewById(R.id.editTextthree);
            editTextfour = (EditText) otpDialog.findViewById(R.id.editTextfour);
            editTextFive = (EditText) otpDialog.findViewById(R.id.editTextFive);
            editTextSix = (EditText) otpDialog.findViewById(R.id.editTextSix);
            TextView tv_error = (TextView) otpDialog.findViewById(R.id.tv_error);
            final TextView tv_timer_resend = (TextView) otpDialog.findViewById(R.id.tv_timer_resend);
            new CountDownTimer(30000, 1000) {

                public void onTick(long millisUntilFinished) {
                    tv_timer_resend.setText("Resends remaining: " + millisUntilFinished / 1000);
                }

                public void onFinish() {
                    tv_timer_resend.setText("Resend Otp");
                }

            }.start();
            tv_timer_resend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (tv_timer_resend.getText().toString().equals("Resend Otp"))
                        sendOTP(mobilenumber);
                }
            });
            if (otp != null && !otp.isEmpty() && otp.length() >= 6) {
                if (!isTest)
                    tv_error.setVisibility(View.VISIBLE);
                editTextone.setText(otp.substring(0, 1));
                editTexttwo.setText(otp.substring(1, 2));
                editTextthree.setText(otp.substring(2, 3));
                editTextfour.setText(otp.substring(3, 4));
                editTextFive.setText(otp.substring(4, 5));
                editTextSix.setText(otp.substring(5, 6));
            }


           /* watcher1 = new LoginActivity.GenericTextWatcher(editTextone);
            watcher2 = new LoginActivity.GenericTextWatcher(editTexttwo);
            watcher3 = new LoginActivity.GenericTextWatcher(editTextthree);
            watcher4 = new LoginActivity.GenericTextWatcher(editTextfour);
            watcher5 = new LoginActivity.GenericTextWatcher(editTextFive);
            watcher6 = new LoginActivity.GenericTextWatcher(editTextSix);
            editTextone.addTextChangedListener(watcher1);
            editTextone.setOnKeyListener(watcher1);
            editTexttwo.addTextChangedListener(watcher2);
            editTexttwo.setOnKeyListener(watcher2);
            editTextthree.addTextChangedListener(watcher3);
            editTextthree.setOnKeyListener(watcher3);
            editTextfour.addTextChangedListener(watcher4);
            editTextfour.setOnKeyListener(watcher4);
            editTextFive.addTextChangedListener(watcher5);
            editTextFive.setOnKeyListener(watcher5);
            editTextSix.addTextChangedListener(watcher6);
            editTextSix.setOnKeyListener(watcher6);*/

            Utility.setupOtpInputs(mContext,editTextone,editTexttwo,editTextthree,editTextfour,editTextFive,editTextSix);

            applyButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String otp = editTextone.getText().toString() + editTexttwo.getText().toString() +
                            editTextthree.getText().toString() + editTextfour.getText().toString() +
                            editTextFive.getText().toString() + editTextSix.getText().toString();

                    if (otp != null && !otp.isEmpty() && otp.length() >= 6) {
                        otpDialog.dismiss();
                        otpDialog = null;
                        fetcher();
                        login(otp);
                    }
                }
            });
            closeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    otpDialog.dismiss();
                    otpDialog = null;
                }
            });
            otpDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            otpDialog.setCanceledOnTouchOutside(false);
            otpDialog.show();
        }
    }

    private void login(String input) {
        fetcher();
        if (validator(user_id_str, password_str)) {
            Map<String, String> dataMap = new HashMap<>();
            dataMap.put("identity", user_id_str);
            if (isOtpLogin)
                dataMap.put("otp", input);
            else
                dataMap.put("password", input);
            dataMap.put("intent", Utility.ExecutiveIntent);//Possible Intents: ["user", "delivery_partner", "vendor", "executive"]

            JSONObject json = new JSONObject(dataMap);
            if (NetworkUtil.isNetworkConnected(mContext)) {
                loginPost(json,input);
            } else {
                Toast.makeText(mContext, "" + getResources().getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
            }
        }
    }



    public class GenericTextWatcher implements TextWatcher, View.OnKeyListener {
        private View view;
        String previousText = "";

        private GenericTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void afterTextChanged(Editable editable) {
            // TODO Auto-generated method stub
            String text = editable.toString();
            switch (view.getId()) {
                case R.id.editTextone:
                    if (text.length() == 1) {
                        /*if (previousText.length() > 0) {
                            editTextone.removeTextChangedListener(watcher1);
                            editTextone.setText(previousText);
                            editTextone.addTextChangedListener(watcher1);

                            editTexttwo.removeTextChangedListener(watcher2);
                            editTexttwo.setText(text);
                            editTexttwo.addTextChangedListener(watcher2);
                        }*/
                        requestFocusAndSelection(editTexttwo,1);
                    }
                    break;
                case R.id.editTexttwo:
                    if (text.length() == 1) {
                        /*if (previousText.length() > 0) {
                            editTexttwo.removeTextChangedListener(watcher2);
                            editTexttwo.setText(previousText);
                            editTexttwo.addTextChangedListener(watcher2);

                            editTextthree.removeTextChangedListener(watcher3);
                            editTextthree.setText(text);
                            editTextthree.addTextChangedListener(watcher3);

                        }*/
                        //  editTextthree.requestFocus();
                        requestFocusAndSelection(editTextthree,1);

                    } else if (text.length() == 0) {
                        //  editTextone.requestFocus();
                        requestFocusAndSelection(editTextone, 0);
                    }
                    break;
                case R.id.editTextthree:
                    if (text.length() == 1) {
                        /*if (previousText.length() > 0) {
                            editTextthree.removeTextChangedListener(watcher3);
                            editTextthree.setText(previousText);
                            editTextthree.addTextChangedListener(watcher3);

                            editTextfour.removeTextChangedListener(watcher4);
                            editTextfour.setText(text);
                            editTextfour.addTextChangedListener(watcher4);
                        }*/
                        //  editTextfour.requestFocus();
                        requestFocusAndSelection(editTextfour,1);
                    } else if (text.length() == 0) {
                        // editTexttwo.requestFocus();
                        requestFocusAndSelection(editTexttwo, 0);
                    }
                    break;
                case R.id.editTextfour:
                    if (text.length() == 1) {
                       /* if (previousText.length() > 0) {
                            editTextfour.removeTextChangedListener(watcher4);
                            editTextfour.setText(previousText);
                            editTextfour.addTextChangedListener(watcher4);

                            editTextFive.removeTextChangedListener(watcher5);
                            editTextFive.setText(text);
                            editTextFive.addTextChangedListener(watcher5);
                        }*/
                        //  editTextFive.requestFocus();
                        requestFocusAndSelection(editTextFive,1);
                    } else if (text.length() == 0) {
                        // editTextthree.requestFocus();
                        requestFocusAndSelection(editTextthree, 0);
                    }
                    break;
                case R.id.editTextFive:
                    if (text.length() == 1) {
                        /*if (previousText.length() > 0) {
                            editTextFive.removeTextChangedListener(watcher5);
                            editTextFive.setText(previousText);
                            editTextFive.addTextChangedListener(watcher5);

                            editTextSix.removeTextChangedListener(watcher6);
                            editTextSix.setText(text);
                            editTextSix.addTextChangedListener(watcher6);
                        }*/
                        // editTextSix.requestFocus();
                        requestFocusAndSelection(editTextSix,1);
                    } else if (text.length() == 0) {
                        //editTextfour.requestFocus();
                        requestFocusAndSelection(editTextfour, 0);
                    }
                    break;
                case R.id.editTextSix:
                    if (text.length() == 0) {
                        // editTextFive.requestFocus();
                        requestFocusAndSelection(editTextFive,0);
                    } else {
                        try {
                            final InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                        } catch (Exception e) {
                        }
                    }


                    break;
            }
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            if (arg0.length() > 0) {
                previousText = arg0.toString();
            }
        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
        }

        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            previousText = "";
            if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KEYCODE_DEL) {
                switch (view.getId()) {
                    case R.id.editTexttwo:
                        if (editTexttwo.getText().toString().trim().length() == 0)
                            editTextone.requestFocus();
                        break;
                    case R.id.editTextthree:
                        if (editTextthree.getText().toString().trim().length() == 0)
                            editTexttwo.requestFocus();
                        break;
                    case R.id.editTextfour:
                        if (editTextfour.getText().toString().trim().length() == 0)
                            editTextthree.requestFocus();
                        break;
                    case R.id.editTextFive:
                        if (editTextFive.getText().toString().trim().length() == 0)
                            editTextfour.requestFocus();
                        break;
                    case R.id.editTextSix:
                        if (editTextSix.getText().toString().trim().length() == 0)
                            editTextFive.requestFocus();
                        break;
                }

            }
            return false;
        }
    }

    private void requestFocusAndSelection(EditText et, int i) {
        et.requestFocus();
        //if (et.getText().length() > 0)
          //  et.setSelection(1);
    }


    private ArrayList findUnAskedPermissions(ArrayList wanted) {
        ArrayList result = new ArrayList();
        for (Object perm : wanted) {
            if (!hasPermission((String) perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    private boolean hasPermission(String permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    } private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter("otp"));
    }
    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }


    private IncomingSms receiver = new IncomingSms() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase("otp")) {
                final String message = intent.getStringExtra("message");
                System.out.println("aaaaaaaaa  message  "+message);
                getOtpFromMessage(message);
            }
        }
    };
    private void getOtpFromMessage(String message) {
        // This will match any 6 digit number in the message
        Pattern pattern = Pattern.compile("(|^)\\d{6}");
        Matcher matcher = pattern.matcher(message);
        if (matcher.find()) {
            System.out.println("aaaaaaa  otp "+matcher.group(0));
            String otp=matcher.group(0);
            if(editTextone!=null) {
                editTextone.setText("" + otp.charAt(0));
                editTexttwo.setText("" + otp.charAt(1));
                editTextthree.setText("" + otp.charAt(2));
                editTextfour.setText("" + otp.charAt(3));
                editTextFive.setText("" + otp.charAt(4));
                editTextSix.setText("" + otp.charAt(5));
            }
        }
    }


}
