package com.kwiky.executivemodule.Activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kwiky.executivemodule.Helpers.Utility;
import com.kwiky.executivemodule.R;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.kwiky.executivemodule.Activities.RegisterVendorActivity.UPDATE_LOCATION;

public class FindAddressInMap extends AppCompatActivity implements OnMapReadyCallback, LocationListener {

    EditText shop_location, latitude_et, longitude_et;
    private Button get_location;
    private GoogleMap mMap;
    Double lattitude=0d, longitude=0d;
    private Location currentLocation;
    private boolean isSingleLineAddress = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_address_in_map);
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.Iconblue));
        }

        shop_location = findViewById(R.id.shop_location);
        latitude_et = findViewById(R.id.latitude_et);
        longitude_et = findViewById(R.id.longitude_et);
        get_location = findViewById(R.id.get_location);
        ImageView img_current_location= findViewById(R.id.img_current_location);
        img_current_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentLocation != null)
                    updateLocation(currentLocation);
                else
                    getLocation();
            }
        });

        ArrayList<String> locArray=new ArrayList<>();
        try {
            Gson gson = new Gson();
            String carListAsString = getIntent().getStringExtra("location");
            Type type = new TypeToken<ArrayList<String>>() {
            }.getType();
            locArray = gson.fromJson(carListAsString, type);
            if(locArray!=null && locArray.size()>=3)
            {
                shop_location.setText(locArray.get(0));
                latitude_et.setText(locArray.get(1));
                longitude_et.setText(locArray.get(2));

                lattitude = Double.parseDouble(locArray.get(1));
                longitude = Double.parseDouble(locArray.get(2));
                if(mMap!=null)
                {
                    setCurrentLocationonMap();
                }
                if(locArray.size()>=4)
                    isSingleLineAddress= true;
            }
        }catch (Exception ex) {
            getLocation();
        }

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        get_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //submit
                if(longitude != 0 && lattitude != 0)
                {
                    Intent intent = new Intent();

                    Gson gson=new Gson();
                    ArrayList<String> locArray=new ArrayList<>();
                    locArray.add(shop_location.getText().toString());
                    locArray.add(latitude_et.getText().toString());
                    locArray.add(longitude_et.getText().toString());
                    String locationString = gson.toJson(locArray);
                    intent.putExtra("location",locationString);
                    setResult(UPDATE_LOCATION, intent);
                    finish();
                }
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if(longitude != 0 && lattitude != 0)
        {
            setCurrentLocationonMap();
        }
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng point) {
                updateLocation(point);
            }
        });
    }

    void getLocation() {

        try {
            LocationManager locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 5, (LocationListener) this);
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        currentLocation = location;
        updateLocation(location);
    }



    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
    Marker marker;
    private void setCurrentLocationonMap() {
        LatLng latLng = new LatLng(lattitude, longitude);
        addMarker(latLng);
    }

    private void addMarker(LatLng latLng) {
        if(marker!=null)
            marker.setVisible(false);
        marker = mMap.addMarker(new MarkerOptions().position(latLng).title("Current Location")
                .icon(BitmapFromVector(getApplicationContext(), R.drawable.ic_location_map_route)));

        //zoom to point

        CameraPosition position = new CameraPosition.Builder()
                .target(latLng)
                .zoom(15.0f)
                .build();

        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(position));

        //zoom to exact location point
        ZoomToPoints(latLng);

    }
    private void ZoomToPoints(LatLng custLatLong) {
        try {
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            builder.include(custLatLong);
            //builder.include(restLatLong);
            LatLngBounds bounds = builder.build();

            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 50);
            mMap.animateCamera(cu, new GoogleMap.CancelableCallback() {
                public void onCancel() {
                }

                public void onFinish() {
                    CameraUpdate zout = CameraUpdateFactory.zoomBy(-1.0f);//-3.0f
                    mMap.animateCamera(zout);
                }
            });
        } catch (Exception ex) {
            Log.e("failed in map", ex.getMessage());
        }
    }

    private BitmapDescriptor BitmapFromVector(Context context, int vectorResId) {
        // below line is use to generate a drawable.
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        // below line is use to set bounds to our vector drawable.
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        // below line is use to create a bitmap for our
        // drawable which we have added.
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        // below line is use to add bitmap in our canvas.
        Canvas canvas = new Canvas(bitmap);
        // vector drawable in canvas.
        vectorDrawable.draw(canvas);
        // after generating our bitmap we are returning our bitmap.
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }


    private void updateLocation(Location location) {
        try {
            Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);

            lattitude = location.getLatitude();
            longitude = location.getLongitude();
            latitude_et.setText(lattitude + "");
            longitude_et.setText(longitude + "");
            if(mMap!=null)
            {
                setCurrentLocationonMap();
            }
            String address = addresses.get(0).getAddressLine(0) + "";
            shop_location.setText(Utility.getShortcutAddress(addresses));//address;
        } catch (Exception e) {

        }
    }

    private void updateLocation(LatLng location) {
        try {
            Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
            lattitude = location.latitude;
            longitude = location.longitude;
            latitude_et.setText(lattitude + "");
            longitude_et.setText(longitude + "");

            addMarker(location);

            List<Address> addresses = geocoder.getFromLocation(lattitude, longitude, 1);
            shop_location.setText(Utility.getShortcutAddress(addresses));//address;
        } catch (Exception e) {

        }
    }
}