package com.kwiky.executivemodule.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.kwiky.executivemodule.Helpers.netwrokHelpers.ConnectionDetector;
import com.kwiky.executivemodule.Helpers.uiHelpers.UImsgs;
import com.kwiky.executivemodule.R;

public class ConnectionAlert extends Activity {

    Button retryButton;
    ConnectionDetector connectionDetector;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connection_alert);
       // getSupportActionBar().hide();
        mContext=getApplicationContext();
        retryButton = (Button) findViewById(R.id.retry_button);

        retryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!connectionDetector.isConnectingToInternet()) {
                    UImsgs.showToast(mContext, "Check Your connection");
                }
                else{
                    Intent intent = new Intent(mContext,super.getClass());
                    startActivity(intent);
                    finish();
                }
            }
        });
    }
}
