/* *
 *
 * @author : sarath
 *
 * @desc : common fields involved in form submission
 *
 * */

package com.kwiky.executivemodule.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;


import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.SparseBooleanArray;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import com.google.gson.internal.bind.util.ISO8601Utils;
import com.kwiky.executivemodule.Helpers.Utility;
import com.kwiky.executivemodule.Helpers.Validations;
import com.kwiky.executivemodule.Helpers.netwrokHelpers.ConnectionDetector;
import com.kwiky.executivemodule.Helpers.uiHelpers.UImsgs;
import com.kwiky.executivemodule.R;
import com.nileshp.multiphotopicker.photopicker.activity.PickImageActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.kwiky.executivemodule.Config.Config.Categories;
import static com.kwiky.executivemodule.Config.Config.States;
import static com.kwiky.executivemodule.Config.Config.VendorCreation;
import static com.kwiky.executivemodule.Constants.IErrors.*;


public class CommonFiledsActivity extends AppCompatActivity implements LocationListener {


    Validations validations;

    private final static int REQUEST_CHECK_SETTINGS_GPS = 0x1;
    private final static int REQUEST_ID_MULTIPLE_PERMISSIONS = 0x2;
    Double lattitude, longitude;

    Bundle bundle;
    Context mContext;
    String token;
    String sID;
    int PLACE_PICKER_REQUEST = 1;
    int count = 0;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1, SELECT_MULTIPLE_FILE = 1;
    private String userChoosenTask;
    public static String coverPhotStringImageUrl1 = "";
    public static String bannerPhotStringImageUrl1 = "";
    public int flag = 0;
    char imageSelection;
    LocationManager locationManager;
    ConnectionDetector connectionDetector;


    ArrayList<String> selectedSubCategoryList = new ArrayList<String>();
    ArrayList<String> selectedAmenitiesList = new ArrayList<String>();
    ArrayList<String> selectedServicesList = new ArrayList<String>();
    ArrayList<String> holidayList = new ArrayList<>();
    ArrayList<String> statesId = new ArrayList<>();
    ArrayList<String> states = new ArrayList<>();
    ArrayList<String> districtsId = new ArrayList<>();
    ArrayList<String> districts = new ArrayList<>();
    ArrayList<String> constituenciesId = new ArrayList<>();
    ArrayList<String> constituencies = new ArrayList<>();
    ArrayList<String> categoryId = new ArrayList<>();
    ArrayList<String> categories = new ArrayList<>();
    ArrayList<String> timings = new ArrayList<>();
    Map<String, String> bannerBase64 = new HashMap<>();
    Map<String, String> servicesMap = new HashMap<>();
    Map<String, String> amenitiesMap = new HashMap<>();
    Map<String, Object> subCategorymap = new HashMap<>();
    Map<String, String> subCategorydatamap = new HashMap<>();
    Map<String, String> servicesdatamap = new HashMap<>();
    Map<String, String> amenitiesdatamap = new HashMap<>();
    ArrayList<String> amenitiesList = new ArrayList<>();
    ArrayList<String> servicesLits = new ArrayList<>();
    ArrayList<String> subcategoriesList = new ArrayList<>();

    ArrayList<String> servicesids = new ArrayList<>();
    ArrayList<String> selectedservicesid = new ArrayList<>();

    ArrayList<String> amenitiesids = new ArrayList<>();
    ArrayList<String> selectedamenitiesids = new ArrayList<>();

    ArrayList<String> subcategoryids = new ArrayList<>();
    ArrayList<String> selectedsubcategoryids = new ArrayList<>();


    EditText newListName, location_et, pincode, complete_address, landmark, std_code, landline, mobile, helpline, email, whatsapp, facebook_link, instagram_link, website_url, twitter_link;
    TextView taptoopenmap;
    LinearLayout fieldsLayout, servicelayout;
    ScrollView scroll;
    Button next, submit;
    CheckBox terms, sun, mon, tue, wed, thu, fri, sat;
    ImageView logout, cover_photo, add_cover_photo, add_banner_photo;
    ImageView banner_photo;
    Spinner state_spinner, district_spinner, constituency_spinner, categories_spinner, everyday_opening_spinner, everyday_closing_spinner, holiday_opening_spinner, holiday_closing_spinner;
    ProgressBar progressBar;
    ListView servicesListview, aminitiesListView, subCategoryListView, selectedSubcategoryListView;

    String newListName_str, location_et_str, pincode_str, complete_address_str, landmark_str, std_code_str, landline_str, mobile_str, helpline_str, email_str, whatsapp_str, facebook_link_str, instagram_link_str, website_url_str,
            twitter_link_str, constId, catId, everyday_open_str, everyday_close_str, holiday_open_str, holiday_close_str;


    ProgressDialog progressDialog;

    //private String imagePath;
    private List<String> pathList;
    String imageEncoded;


    @Override
    protected void onResume() {
        super.onResume();

        if (!connectionDetector.isConnectingToInternet()) {
            //Toast.makeText(mContext, "P", Toast.LENGTH_SHORT).show();
            UImsgs.showToast(mContext, "Check Your connection");
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (!connectionDetector.isConnectingToInternet()) {
            //Toast.makeText(mContext, "P", Toast.LENGTH_SHORT).show();
            UImsgs.showToast(mContext, "Check Your connection");
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.common_fields);
        getSupportActionBar().hide();
        mContext = getApplicationContext();
        init();
        connectionDetector = new ConnectionDetector(mContext);

        validations = new Validations();

        servicelayout.setVisibility(View.GONE);

        //progressBar.setVisibility(View.VISIBLE);
        progressDialog = new ProgressDialog(this);
        progressDialog.setIcon(R.drawable.nextclick_logo_black);
        progressDialog.setMessage("Please wait while fetching fields data.....");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setProgress(0);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        AsyncTaskRunner asyncTaskRunner = new AsyncTaskRunner();
        asyncTaskRunner.execute();


        getLocation();
        statesDataFetcher();
        categoryDataFetcher();



        bundle = getIntent().getExtras();


        if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 101);

        }
        if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

        } else {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 101);
        }



        state_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if ((position - 1) >= 0) {
                    sID = statesId.get(position - 1);
                    districtsId.clear();
                    districts.clear();
                    districtsDataFetcher(sID);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        district_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if ((position - 1) >= 0 && sID != null) {
                    String dID = districtsId.get(position - 1);
                    constituencies.clear();
                    constituenciesId.clear();
                    constituenciesDataFetcher(sID, dID);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        add_cover_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageSelection = 'c';
                boolean selecting = selectImage();
                if (selecting) {
                    //visibilty_gone.setVisibility(View.GONE);
                }
            }
        });

        add_banner_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*imageSelection = 'b';
                boolean selecting = selectImage();
                if (selecting) {
                    //visibilty_gone.setVisibility(View.GONE);
                }*/
               /* Intent mIntent = new Intent(getApplicationContext(), PickImageActivity.class);
                mIntent.putExtra(PickImageActivity.KEY_LIMIT_MAX_IMAGE, 60);
                mIntent.putExtra(PickImageActivity.KEY_LIMIT_MIN_IMAGE, 3);
                startActivityForResult(mIntent, PickImageActivity.PICKER_REQUEST_CODE);*/

                imageSelection = 'b';
                boolean selecting = selectImage();
                if (selecting) {
                    //visibilty_gone.setVisibility(View.GONE);
                }
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                dataFetcherFromFields();
                if (validator()) {
                    servicesDataFetcher(catId);
                    fieldsLayout.setVisibility(View.GONE);
                    servicelayout.setVisibility(View.VISIBLE);
                }
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //progressBar.setVisibility(View.VISIBLE);
                progressDialog = new ProgressDialog(CommonFiledsActivity.this);
                progressDialog.setIcon(R.drawable.nextclick_logo_black);
                progressDialog.setMessage("Please wait while uploading the data.....");
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.setProgress(0);
                progressDialog.setCancelable(false);
                progressDialog.setCanceledOnTouchOutside(false);
                progressDialog.show();
                dataSender(progressDialog);
            }
        });


        aminitiesListView.setOnTouchListener(new ListView.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        break;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }

                // Handle ListView touch events.
                v.onTouchEvent(event);
                return true;
            }
        });
        servicesListview.setOnTouchListener(new ListView.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        break;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }

                // Handle ListView touch events.
                v.onTouchEvent(event);
                return true;
            }
        });
        subCategoryListView.setOnTouchListener(new ListView.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        break;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }

                // Handle ListView touch events.
                v.onTouchEvent(event);
                return true;
            }
        });
        selectedSubcategoryListView.setOnTouchListener(new ListView.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        break;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }

                // Handle ListView touch events.
                v.onTouchEvent(event);
                return true;
            }
        });
        /*Log.i("CHECK", subCategoryListView.getChildCount()+"");*/
        final ArrayAdapter<String> subcategoryadapter = new ArrayAdapter<String>(getApplicationContext(),
                android.R.layout.simple_dropdown_item_1line, selectedSubCategoryList);
        subCategoryListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int len = subCategoryListView.getCount();
                SparseBooleanArray checked = subCategoryListView.getCheckedItemPositions();

                String item = subcategoriesList.get(position);
                if (checked.get(position)) {

                    /* do whatever you want with the checked item */
                    selectedSubCategoryList.add(item);
                    selectedsubcategoryids.add(subcategoryids.get(position));
                    subcategoryadapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
                    selectedSubcategoryListView.setAdapter(subcategoryadapter);
                } else {
                    selectedSubCategoryList.remove(item);
                    selectedsubcategoryids.remove(subcategoryids.get(position));
                    subcategoryadapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
                    selectedSubcategoryListView.setAdapter(subcategoryadapter);
                }
            }
        });
        aminitiesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                int len = aminitiesListView.getCount();
                SparseBooleanArray checked = aminitiesListView.getCheckedItemPositions();

                String item = amenitiesList.get(position);
                if (checked.get(position)) {

                    /* do whatever you want with the checked item */
                    selectedamenitiesids.add(amenitiesids.get(position));
                } else {
                    selectedamenitiesids.remove(amenitiesids.get(position));
                }

            }
        });

        servicesListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                int len = servicesListview.getCount();
                SparseBooleanArray checked = servicesListview.getCheckedItemPositions();

                //String item = amenitiesList.get(position);
                if (checked.get(position)) {

                    /* do whatever you want with the checked item */
                    selectedservicesid.add(servicesids.get(position));
                } else {
                    selectedservicesid.remove(servicesids.get(position));
                }


            }
        });
        /*selectedSubcategoryListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int len = subCategoryListView.getCount();
                SparseBooleanArray checked = subCategoryListView.getCheckedItemPositions();

                if (checked.get(position)) {
                    String item = timings.get(position);
                    *//* do whatever you want with the checked item *//*
                    selectedSubCategoryList.add(item);

                    subcategoryadapter.setDropDownViewResource(android.R.fieldsLayout.simple_dropdown_item_1line);
                    selectedSubcategoryListView.setAdapter(subcategoryadapter);
                }
            }
        });*/
    }

    class AsyncTaskRunner extends AsyncTask<String, String, String> {

        private String resp;
        ProgressDialog progressDialog;

        @Override
        protected String doInBackground(String... params) {
            publishProgress("No Connection..."); // Calls onProgressUpdate()
            if (!connectionDetector.isConnectingToInternet()) {
                resp = "No Internet";
                Intent intent = new Intent(mContext, ConnectionAlert.class);
                startActivity(intent);
                finish();
            } else {
                resp = "Connected";
            }

            return resp;
        }


        @Override
        protected void onPostExecute(String result) {
            // execution of result of Long time consuming operation
               /* progressDialog.dismiss();
                finalResult.setText(result);*/
            if (!connectionDetector.isConnectingToInternet()) {
                resp = "No Internet";
                Intent intent = new Intent(mContext, ConnectionAlert.class);
                startActivity(intent);
                finish();
            } else {
                resp = "Connected";
            }


        }


        @Override
        protected void onPreExecute() {
               /* progressDialog = ProgressDialog.show(MainActivity.this,
                        "ProgressDialog",
                        "Wait for "+time.getText().toString()+ " seconds");*/
           /* if(!connectionDetector.isConnectingToInternet()){
                resp="No Internet";
                Intent intent = new Intent(mContext,ConnectionAlert.class);
                startActivity(intent);
                finish();
            }
            else {
                resp = "Connected";
            }
*/
        }


        @Override
        protected void onProgressUpdate(String... text) {
            /*finalResult.setText(text[0]);*/
            if (!connectionDetector.isConnectingToInternet()) {
                resp = "No Internet";
                Intent intent = new Intent(mContext, ConnectionAlert.class);
                startActivity(intent);
                finish();
            } else {
                resp = "Connected";
            }


        }
    }


    /*Locataion Start*/
    @SuppressLint("MissingPermission")
    void getLocation() {

        try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 5, (android.location.LocationListener) this);
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        //locationText.setText("Latitude: " + location_et.getLatitude() + "\n Longitude: " + location_et.getLongitude());


        try {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            /*locationText.setText(locationText.getText() + "\n"+addresses.get(0).getAddressLine(0)+", "+
                    addresses.get(0).getAddressLine(1)+", "+addresses.get(0).getAddressLine(2));
            */
            String address = addresses.get(0).getAddressLine(0) + "";
            String[] arr = address.split(",");
            /*Toast.makeText(this, arr.length+"", Toast.LENGTH_SHORT).show();*/

            Double lat1 = location.getLatitude();
            Double lang1 = location.getLongitude();
            lattitude = lat1;
            longitude = lang1;
            location_et.setText(address);


        } catch (Exception e) {

        }

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

//Location End


    //Image Selection Start

    private boolean selectImage() {
        final CharSequence[] items = {"Choose from Library",
                "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(CommonFiledsActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = Utility.checkPermission(CommonFiledsActivity.this);

                if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";
                    if (result) {    //Calling Gallery Marhod For Images
                        galleryIntent();
                    }

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
        return true;
    }

    //Calling Openig Gallery For Images
    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);


        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
        }

        if (resultCode == -1 && requestCode == PickImageActivity.PICKER_REQUEST_CODE) {

            this.pathList = data.getExtras().getStringArrayList(PickImageActivity.KEY_DATA_RESULT);
            if (this.pathList != null && !this.pathList.isEmpty()) {
                StringBuilder sb = new StringBuilder("");

                ArrayList<Bitmap> bitmapList = new ArrayList<>();
                for (int i = 0; i < pathList.size(); i++) {
                   /* sb.append("Photo"+(i+1)+":"+pathList.get(i));
                    sb.append("\n");*/
                    Bitmap bitmap1 = BitmapFactory.decodeFile(pathList.get(i));//assign your bitmap;
                   /* Bitmap bitmap2 = BitmapFactory.decodeResource(getResources(), drawable.user);//assign your bitmap;
                    Bitmap bitmap3 = BitmapFactory.decodeResource(getResources(), drawable.user);//assign your bitmap;
                    Bitmap bitmap4 = BitmapFactory.decodeResource(getResources(), drawable.user);//assign your bitmap;*/


                    bitmapList.add(bitmap1);

                    Bitmap mergedImg = mergeMultiple(bitmapList);

                    banner_photo.setImageBitmap(mergedImg);

                }
                //tvResult.setText(sb.toString()); // here this is textview for sample use...

                onSelectFromGalleryResult(bitmapList);
                // Toast.makeText(mContext, sb.toString() + "", Toast.LENGTH_SHORT).show();
            }
        }

    }


    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
               /* Toast.makeText(mContext, data.getData() + "", Toast.LENGTH_SHORT).show();
                Log.d("Data", data.getData().toString());*/

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        bm = Bitmap.createScaledBitmap(bm, 512, 512, false);

        if (imageSelection == 'c') {

            cover_photo.setImageBitmap(bm);

            Bitmap bitmap = ((BitmapDrawable) cover_photo.getDrawable()).getBitmap();

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

            byte[] byteArray = baos.toByteArray();
            coverPhotStringImageUrl1 = Base64.encodeToString(byteArray, Base64.DEFAULT);
            //  System.out.println("ByteArra"+coverPhotStringImageUrl1);

            flag = 1;
        }else if (imageSelection=='b'){
            banner_photo.setImageBitmap(bm);

            Bitmap bitmap = ((BitmapDrawable) banner_photo.getDrawable()).getBitmap();

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

            byte[] byteArray = baos.toByteArray();
            bannerPhotStringImageUrl1 = Base64.encodeToString(byteArray, Base64.DEFAULT);
            //  System.out.println("ByteArra"+coverPhotStringImageUrl1);
            String bannerBitString = Base64.encodeToString(byteArray, Base64.DEFAULT);
            bannerBase64.put(0 + "".trim(), bannerBitString);
            flag = 1;
        }

    }

    private void onSelectFromGalleryResult(ArrayList data) {


        bannerBase64.clear();

        for (int i = 0; i < data.size(); i++) {

            Bitmap bitmap = (Bitmap) data.get(i);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

            byte[] byteArray = baos.toByteArray();
            String bannerBitString = Base64.encodeToString(byteArray, Base64.DEFAULT);
            bannerBase64.put(i + "".trim(), bannerBitString);
            //  System.out.println("ByteArra"+coverPhotStringImageUrl1);


            flag = 1;
        }


    }

    //Image Selection End
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        int permissionLocation = ContextCompat.checkSelfPermission(CommonFiledsActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
            getLocation();
        }
    }


    public void statesDataFetcher() {


        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, States, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonData = new JSONObject(response);
                    String status = jsonData.getString("status");
                    String messagae = jsonData.getString("message");
                    if (status.equalsIgnoreCase("true") && messagae.equalsIgnoreCase("Success..!")) {

                        JSONArray dataArray = jsonData.getJSONArray("data");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataObject = dataArray.getJSONObject(i);
                            statesId.add(dataObject.getString("id"));
                            states.add(dataObject.getString("name"));
                        }
                       // progressBar.setVisibility(View.GONE);

                    } else {
                        UImsgs.showToast(mContext, messagae);
                        //Toast.makeText(LoginActivity.this, "Invalid Credentials", Toast.LENGTH_SHORT).show();
                    }


                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                            android.R.layout.simple_spinner_item, states);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    state_spinner.setAdapter(adapter);


                } catch (JSONException e) {
                    e.printStackTrace();
                    UImsgs.showToast(mContext, "No data found...!");
                    progressDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(LoginActivity.this, error+"", Toast.LENGTH_SHORT).show();

            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("X_AUTH_TOKEN", token);

                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(8000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);


    }

    public void districtsDataFetcher(String stateid) {
        progressDialog.show();
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, States + "/" + stateid, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonData = new JSONObject(response);
                    String status = jsonData.getString("status");
                    String messagae = jsonData.getString("message");
                    if (status.equalsIgnoreCase("true") && messagae.equalsIgnoreCase("Success..!")) {

                        JSONObject dataObject = jsonData.getJSONObject("data");
                        JSONArray dataArray = dataObject.getJSONArray("districts");
                        districts.add("Select");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject districtObject = dataArray.getJSONObject(i);
                            districtsId.add(districtObject.getString("id"));
                            districts.add(districtObject.getString("name"));
                        }

                    } else {
                        UImsgs.showToast(getApplicationContext(), messagae);
                        //Toast.makeText(LoginActivity.this, "Invalid Credentials", Toast.LENGTH_SHORT).show();
                    }


                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                            android.R.layout.simple_spinner_item, districts);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    district_spinner.setAdapter(adapter);
                    progressDialog.dismiss();

                } catch (JSONException e) {
                    e.printStackTrace();
                    UImsgs.showToast(mContext, "No data found...!");
                    progressDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(LoginActivity.this, error+"", Toast.LENGTH_SHORT).show();

            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("X_AUTH_TOKEN", token);

                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(8000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);


    }

    public void constituenciesDataFetcher(String stateid, String districtid) {
        progressDialog.show();
        final int err = 0;
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, States + "/" + stateid + "/" + districtid, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonData = new JSONObject(response);
                    String status = jsonData.getString("status");
                    String messagae = jsonData.getString("message");
                    if (status.equalsIgnoreCase("true") && messagae.equalsIgnoreCase("Success..!")) {

                        constituencies.add("Select");
                        JSONObject dataObject = jsonData.getJSONObject("data");
                        JSONArray dataArray = dataObject.getJSONArray("constituenceis");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject districtObject = dataArray.getJSONObject(i);
                            constituenciesId.add(districtObject.getString("id"));
                            constituencies.add(districtObject.getString("name"));
                        }

                    } else {
                        UImsgs.showToast(mContext, messagae);
                        //Toast.makeText(LoginActivity.this, "Invalid Credentials", Toast.LENGTH_SHORT).show();
                    }


                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                            android.R.layout.simple_spinner_item, constituencies);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    constituency_spinner.setAdapter(adapter);
                    progressDialog.dismiss();

                } catch (JSONException e) {
                    e.printStackTrace();
                    UImsgs.showToast(mContext, "No data found...!");
                    progressDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(LoginActivity.this, error+"", Toast.LENGTH_SHORT).show();


            }

        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("X_AUTH_TOKEN", token);

                return map;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(8000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);


    }

    public void categoryDataFetcher() {


        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Categories, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonData = new JSONObject(response);
                    String status = jsonData.getString("status");
                    String messagae = jsonData.getString("message");
                    if (status.equalsIgnoreCase("true") && messagae.equalsIgnoreCase("Success..!")) {

                        JSONArray dataArray = jsonData.getJSONArray("data");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataObject = dataArray.getJSONObject(i);
                            categoryId.add(dataObject.getString("id"));
                            categories.add(dataObject.getString("name"));
                        }

                    } else {
                        UImsgs.showToast(mContext, messagae);
                        //Toast.makeText(LoginActivity.this, "Invalid Credentials", Toast.LENGTH_SHORT).show();
                    }


                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                            android.R.layout.simple_spinner_item, categories);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    categories_spinner.setAdapter(adapter);
                    progressDialog.dismiss();

                } catch (JSONException e) {
                    e.printStackTrace();
                    UImsgs.showToast(mContext, String.valueOf(e));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(CommonFiledsActivity.this, error + "", Toast.LENGTH_SHORT).show();
                categoryDataFetcher();

            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("X_AUTH_TOKEN", token);

                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(8000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }


    public void servicesDataFetcher(final String categoryId) {
        //String tempCategories = Categories + "1";
        String tempCategories = Categories + categoryId;

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, tempCategories, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonData = new JSONObject(response);
                    Boolean type;
                    String status = jsonData.getString("status");
                    String messagae = jsonData.getString("message");
                    String datatype = jsonData.get("data").getClass().getName();
                    if (datatype.equalsIgnoreCase("java.lang.Boolean")) {
                        type = false;
                    } else
                        type = true;
                    //Toast.makeText(mContext, String.valueOf(messagae), Toast.LENGTH_SHORT).show();
                    if (type) {
                        if (status.equalsIgnoreCase("true") && messagae.equalsIgnoreCase("Success..!")) {

                            String data = jsonData.getString("data");

                            JSONObject jsonObject = jsonData.getJSONObject("data");

                            JSONArray amenitiesJsonArray = jsonObject.getJSONArray("amenities");
                            //Toast.makeText(mContext, String.valueOf(amenitiesJsonArray), Toast.LENGTH_SHORT).show();
                            if (amenitiesJsonArray.length() > 0) {
                                for (int i = 0; i < amenitiesJsonArray.length(); i++) {
                                    JSONObject ammenitiesArray = (JSONObject) amenitiesJsonArray.get(i);
                                    String amenityname = ammenitiesArray.getString("name");
                                    String amenityid = ammenitiesArray.getString("id");
                                    String amenitycatId = ammenitiesArray.getString("cat_id");
                                    amenitiesdatamap.put(amenityid, amenityname);
                                    amenitiesList.add(amenityname);
                                    amenitiesids.add(amenityid);

                                }

                                aminitiesListView.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);
                                ArrayAdapter<String> amenitiesAdapter = new ArrayAdapter<String>(getApplicationContext(),
                                        android.R.layout.simple_list_item_multiple_choice, amenitiesList);
                                amenitiesAdapter.setDropDownViewResource(android.R.layout.simple_list_item_multiple_choice);
                                aminitiesListView.setAdapter(amenitiesAdapter);
                            } else {
                                UImsgs.showToast(mContext, "Sorry...! No Amenities Found");
                            }


                            JSONArray subCategoryJsonArray = jsonObject.getJSONArray("sub_categories");
                            if (subCategoryJsonArray.length() > 0) {
                                for (int i = 0; i < subCategoryJsonArray.length(); i++) {
                                    JSONObject subCategoryObject = (JSONObject) subCategoryJsonArray.get(i);
                                    String subcategoryname = subCategoryObject.getString("name");
                                    String subcategoryid = subCategoryObject.getString("id");
                                    String subcategorycatId = subCategoryObject.getString("cat_id");
                                    subCategorydatamap.put(subcategoryid, subcategoryname);
                                    subcategoriesList.add(subcategoryname);
                                    subcategoryids.add(subcategoryid);


                                }

                                ArrayAdapter<String> subcategoryAdapter = new ArrayAdapter<String>(getApplicationContext(),
                                        android.R.layout.simple_list_item_multiple_choice, subcategoriesList);
                                subcategoryAdapter.setDropDownViewResource(android.R.layout.simple_list_item_multiple_choice);
                                subCategoryListView.setAdapter(subcategoryAdapter);
                            } else {
                                UImsgs.showToast(mContext, "Sorry...! No SubCategories Found");
                            }


                            JSONObject serviceJsonobject = jsonObject.getJSONObject("services");

                            Iterator x = serviceJsonobject.keys();
                            JSONArray servicesjsonArray = new JSONArray();


                            while (x.hasNext()) {
                                String key = (String) x.next();
                                servicesjsonArray.put(serviceJsonobject.get(key));
                            }
                            //Toast.makeText(mContext, String.valueOf(amenitiesJsonArray), Toast.LENGTH_SHORT).show();
                            if (servicesjsonArray.length() > 0) {
                                for (int i = 0; i < servicesjsonArray.length(); i++) {
                                    JSONObject servicesData = (JSONObject) servicesjsonArray.get(i);
                                    String servicename = servicesData.getString("name");
                                    String serviceid = servicesData.getString("id");
                                    String servicecatId = servicesData.getString("cat_id");
                                    servicesdatamap.put(serviceid, servicename);
                                    servicesLits.add(servicename);
                                    servicesids.add(serviceid);

                                }


                                ArrayAdapter<String> serviceAdapter = new ArrayAdapter<String>(getApplicationContext(),
                                        android.R.layout.simple_list_item_multiple_choice, servicesLits);
                                servicesListview.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);
                                serviceAdapter.setDropDownViewResource(android.R.layout.simple_list_item_multiple_choice);
                                servicesListview.setAdapter(serviceAdapter);
                            } else {
                                UImsgs.showToast(mContext, "Sorry...! No Services Found");
                            }


                        } else {
                            UImsgs.showToast(mContext, messagae);
                            //Toast.makeText(ServicesAminitiesActivity.this, "Invalid Credentials", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        UImsgs.showToast(mContext, "Sorry..! No data found for the selected category");
                        fieldsLayout.setVisibility(View.VISIBLE);
                        servicelayout.setVisibility(View.GONE);
                    }


                    /*ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                            android.R.fieldsLayout.simple_spinner_item, states);
                    adapter.setDropDownViewResource(android.R.fieldsLayout.simple_spinner_dropdown_item);
                    state_spinner.setAdapter(adapter);*/


                } catch (JSONException e) {
                    e.printStackTrace();
                    UImsgs.showToast(mContext, "No data found...!");
                    progressDialog.dismiss();
                }
              /*  ServicesAminitiesAdapter servicesAminitiesAdapter = new ServicesAminitiesAdapter(mContext,serviceAminitesList);
                amenitiesRecycle.setAdapter(servicesAminitiesAdapter);*/
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(CommonFiledsActivity.this, error + "", Toast.LENGTH_SHORT).show();

            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("X_AUTH_TOKEN", token);

                return map;
            }
        };

        /*stringRequest.setRetryPolicy(new DefaultRetryPolicy(8000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));*/
        requestQueue.add(stringRequest);


    }


    public void dataFetcherFromFields() {

        newListName_str = newListName.getText().toString().trim();
        location_et_str = location_et.getText().toString().trim();
        pincode_str = pincode.getText().toString().trim();
        complete_address_str = complete_address.getText().toString().trim();
        landmark_str = landmark.getText().toString().trim();
        std_code_str = std_code.getText().toString().trim();
        landline_str = landline.getText().toString().trim();
        mobile_str = mobile.getText().toString().trim();
        helpline_str = helpline.getText().toString().trim();
        email_str = email.getText().toString().trim();
        whatsapp_str = whatsapp.getText().toString().trim();
        facebook_link_str = facebook_link.getText().toString().trim();
        instagram_link_str = instagram_link.getText().toString().trim();
        website_url_str = website_url.getText().toString().trim();
        twitter_link_str = twitter_link.getText().toString().trim();
        /*constId= constituenciesId.get(constituency_spinner.getSelectedItemPosition()-1);
        catId= categoryId.get(categories_spinner.getSelectedItemPosition()-1);*/
        if (constituencies.size() > 0) {
            if (!constituency_spinner.getSelectedItem().toString().equalsIgnoreCase("select"))
                constId = constituenciesId.get(constituency_spinner.getSelectedItemPosition() - 1);
            else
                constId = null;
        } else
            constId = null;
        if (categories.size() > 0) {
            if (!categories_spinner.getSelectedItem().toString().equalsIgnoreCase("select"))
                catId = categoryId.get(categories_spinner.getSelectedItemPosition() - 1);
            else
                catId = null;
        } else
            catId = null;


        everyday_open_str = everyday_opening_spinner.getSelectedItem().toString().trim();
        everyday_close_str = everyday_closing_spinner.getSelectedItem().toString().trim();
        holiday_open_str = holiday_opening_spinner.getSelectedItem().toString().trim();
        holiday_close_str = holiday_closing_spinner.getSelectedItem().toString().trim();


        if (sun.isChecked()) {
            holidayList.add("1");
        }
        if (mon.isChecked()) {
            holidayList.add("2");
        }
        if (tue.isChecked()) {
            holidayList.add("3");
        }
        if (wed.isChecked()) {
            holidayList.add("4");
        }
        if (thu.isChecked()) {
            holidayList.add("5");
        }
        if (fri.isChecked()) {
            holidayList.add("6");
        }
        if (sat.isChecked()) {
            holidayList.add("7");
        }


    }

    public void dataSender(ProgressDialog progressDialog) {
        Map<String, String> socialdata = new HashMap<>();
        socialdata.put("1", facebook_link_str);
        socialdata.put("2", twitter_link_str);
        socialdata.put("3", instagram_link_str);
        socialdata.put("4", website_url_str);

        Map<String, Map<String, String>> contact = new HashMap<>();
        Map<String, String> one = new HashMap<>();
        one.put("number", mobile_str);
        one.put("code", "+91");
        Map<String, String> two = new HashMap<>();
        two.put("number", landline_str);
        two.put("code", std_code_str);
        Map<String, String> three = new HashMap<>();
        three.put("number", whatsapp_str);
        three.put("code", "+91");
        Map<String, String> four = new HashMap<>();
        four.put("number", helpline_str);
        four.put("code", "");
        contact.put("1", one);
        contact.put("2", two);
        contact.put("3", three);
        contact.put("4", four);

        Map<String, String> holiday = new HashMap<>();
        if (holidayList.size() > 0) {
            for (int i = 0; i < holidayList.size(); i++) {
                holiday.put(i + "".trim(), holidayList.get(i));
            }
        }


        Map<String, Object> mainData = new HashMap<>();

        mainData.put("name", newListName_str);
        mainData.put("email", email_str);
        mainData.put("constituency_id", constId);
        mainData.put("category_id", catId);
       /* mainData.put("constituency_id", "1");
        mainData.put("category_id", "5");*/
        mainData.put("address", complete_address_str);
        mainData.put("landmark", landmark_str);
        mainData.put("pincode", pincode_str);
        mainData.put("everyday_open_time", everyday_open_str);
        mainData.put("everyday_close_time", everyday_close_str);
        if (holiday_open_str.equalsIgnoreCase("select") | holiday_close_str.equalsIgnoreCase("select")) {
            mainData.put("holiday_open_time", "");
            mainData.put("holiday_close_time", "");
        } else {
            mainData.put("holiday_open_time", holiday_open_str);
            mainData.put("holiday_close_time", holiday_close_str);
        }
        mainData.put("cover", coverPhotStringImageUrl1);
        mainData.put("banner", bannerBase64);

        if (selectedsubcategoryids.size() > 0) {
            for (int i = 0; i < selectedsubcategoryids.size(); i++) {
                subCategorymap.put(i + "".trim(), selectedsubcategoryids.get(i));
            }
        }
        mainData.put("sub_categories", subCategorymap);

        if (selectedamenitiesids.size() > 0) {
            for (int i = 0; i < selectedamenitiesids.size(); i++) {
                amenitiesMap.put(i + "".trim(), selectedamenitiesids.get(i));
            }
        }
        mainData.put("amenities", amenitiesMap);

        if (selectedservicesid.size() > 0) {
            for (int i = 0; i < selectedservicesid.size(); i++) {
                servicesMap.put(i + "".trim(), selectedservicesid.get(i));
            }
        }
        mainData.put("services", servicesMap);
        mainData.put("location_address", location_et_str);
        mainData.put("latitude", lattitude);
        mainData.put("longitude", longitude);

        mainData.put("holidays", holiday);
        mainData.put("contacts", contact);
        mainData.put("social", socialdata);


        JSONObject json = new JSONObject(mainData);
        submission(json,progressDialog);
    }


    public void submission(JSONObject json, final ProgressDialog progressDialog) {

        final String data = json.toString();

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, VendorCreation,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {

                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("aaaaaaa vendor response  "+jsonObject.toString());
                            Boolean status = jsonObject.getBoolean("status");
                            String http = jsonObject.getString("http_code");
                            String message = jsonObject.getString("message");
                            if (status && http.equalsIgnoreCase("201") && message.equalsIgnoreCase("Success..!")) {
                                progressDialog.dismiss();
                                Intent intent = new Intent(CommonFiledsActivity.this, FinishActivity.class);
                                startActivity(intent);
                                finish();
                            } else {
                                UImsgs.showToast(mContext, "Some Error Occured.. Please provide data again...");
                                progressDialog.dismiss();
                                servicelayout.setVisibility(View.GONE);
                                fieldsLayout.setVisibility(View.VISIBLE);
                            }

                        } catch (Exception e) {
                            System.out.println("aaaaaaa catch  "+e.getMessage());
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("aaaaaaa error  "+error.getMessage());
                //Toast.makeText(LoginActivity.this, error+"", Toast.LENGTH_SHORT).show();
                UImsgs.showToast(mContext, "Some Error Occured.. Please provide data again...");
                progressDialog.dismiss();
                servicelayout.setVisibility(View.GONE);
                fieldsLayout.setVisibility(View.VISIBLE);
            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put("X_AUTH_TOKEN", token);

                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }


    public boolean validator() {

        boolean validity = true;


        if (newListName_str.length() == 0) {
            newListName.setError(EMPTY);
            newListName.requestFocus();
            validity = false;
        }
        if (location_et_str.length() == 0) {
            location_et.setError("Wait untill location fetched");
            location_et.requestFocus();
            validity = false;
        }
        if (pincode_str.length() == 0) {
            pincode.setError(EMPTY);
            pincode.requestFocus();
            validity = false;
        }
        if (pincode_str.length() < 6) {
            pincode.setError(PIN_INVALID);
            pincode.requestFocus();
            validity = false;
        }
        if (complete_address_str.length() == 0) {
            complete_address.setError(EMPTY);
            complete_address.requestFocus();
            validity = false;
        }
        if (complete_address_str.length() < 6) {
            complete_address.setError("Please provide complete details");
            complete_address.requestFocus();
            validity = false;
        }
        if (landmark_str.length() == 0) {
            landmark.setError(getResources().getString(R.string.empty));
            landmark.requestFocus();
            validity = false;
        }
        if (landmark_str.length() <= 6) {
            landmark.setError("Please provide clear details");
            landmark.requestFocus();
            validity = false;
        }
        /*if (std_code_str.length() == 0) {
            std_code.setError(EMPTY);
            std_code.requestFocus();
            validity = false;
        }*/
       /* if (std_code_str.length() > 0 && std_code_str.length() < 3) {
            std_code.setError(INVALID);
            std_code.requestFocus();
            validity = false;
        }*/
       /* if (landline_str.length() == 0) {
            landline.setError(EMPTY);
            landline.requestFocus();
            validity = false;
        }*/
       /* if (landline_str.length() > 0 && landline_str.length() < 6) {
            landline.setError(INVALID);
            landline.requestFocus();
            validity = false;
        }*/
        if (mobile_str.length() == 0) {
            mobile.setError(EMPTY);
            mobile.requestFocus();
            validity = false;
        }
        if (mobile_str.length() < 10) {
            mobile.setError(INVALID);
            mobile.requestFocus();
            validity = false;
        }
        /*if (helpline_str.length() == 0) {
            helpline.setError(EMPTY);
            helpline.requestFocus();
            validity = false;
        }*/
       /* if (helpline_str.length() > 0 && helpline_str.length() < 10) {
            helpline.setError(INVALID);
            helpline.requestFocus();
            validity = false;
        }*/

        if (validations.isBlank(email_str)) {
            email.setError(EMPTY);
            email.requestFocus();
            validity = false;
        }

        if (validations.isValidEmail(email_str)) {
            email.setError(INVALID);
            email.requestFocus();
            validity = false;
        }
       /* if (whatsapp_str.length() == 0) {
            whatsapp.setError(EMPTY);
            whatsapp.requestFocus();
            validity = false;
        }
        if (whatsapp_str.length() < 10) {
            whatsapp.setError(INVALID);
            whatsapp.requestFocus();
            validity = false;
        }
        if (facebook_link_str.length() != 0) {

            if (facebook_link_str.length()<15) {
                facebook_link.setError(INVALID);
                facebook_link.requestFocus();
                validity = false;
            }
        }

        if (instagram_link_str.length() != 0) {

            if (instagram_link_str.length() < 10) {
                instagram_link.setError(INVALID);
                instagram_link.requestFocus();
                validity = false;
            }
        }

        if (twitter_link_str.length() != 0) {

            if (twitter_link_str.length() < 10) {
                twitter_link.setError(INVALID);
                twitter_link.requestFocus();
                validity = false;
            }
        }

        if (website_url_str.length() != 0) {

            if (website_url_str.length() < 5) {
                website_url.setError(INVALID);
                website_url.requestFocus();
                validity = false;
            }
        }

        if (everyday_open_str.equalsIgnoreCase("select")) {
            everyday_opening_spinner.requestFocus();
            UImsgs.showToast(mContext, TIMEINAVLID);
            validity = false;
        }

        if (everyday_close_str.equalsIgnoreCase("select")) {
            everyday_closing_spinner.requestFocus();
            UImsgs.showToast(mContext, TIMEINAVLID);
            validity = false;
        }

*/

        if (constId == null) {
            UImsgs.showToast(mContext, "Please Select Constituency");
            constituency_spinner.requestFocus();
            validity = false;
        }
        if (catId == null) {
            UImsgs.showToast(mContext, "Please Select Category");
            categories_spinner.requestFocus();
            validity = false;
        }
        if (coverPhotStringImageUrl1.length() <= 0 || coverPhotStringImageUrl1 == null) {
            UImsgs.showToast(mContext, "Please Select Cover photo");
            cover_photo.requestFocus();
            validity = false;
        }

      /*  if (bannerBase64.size() <= 2) {
            UImsgs.showToast(mContext, "Please Select banner photos");
            banner_photo.requestFocus();
            validity = false;
        }*/
        System.out.println("aaaaaaaaa banner "+bannerBase64.size());

        if (bannerBase64.size() <= 0) {
            UImsgs.showToast(mContext, "Please Select banner photos");
            banner_photo.requestFocus();
            validity = false;
        }

        /* constId, catId,  */


        return validity;

    }

    private Bitmap mergeMultiple(ArrayList<Bitmap> parts) {
        final int IMAGE_MAX_SIZE = 1200000;
        int value = parts.size();
        int height = parts.get(0).getHeight();
        int width = parts.get(0).getWidth();

        double y = Math.sqrt(IMAGE_MAX_SIZE
                / (((double) width) / height));
        double x = (y / height) * width;
        Bitmap result = Bitmap.createScaledBitmap(parts.get(0), (int) x, (int) y, true);
        Canvas canvas = new Canvas(result);
        Paint paint = new Paint();

        for (int i = 0; i < parts.size(); i++) {
            canvas.drawBitmap(parts.get(i), parts.get(i).getWidth() * (i % 4), parts.get(i).getHeight() * (i / 4), paint);
        }
        return result;
    }

    public void init() {


        newListName = (EditText) findViewById(R.id.new_list_name);
        location_et = (EditText) findViewById(R.id.location);
        pincode = (EditText) findViewById(R.id.pincode);
        complete_address = (EditText) findViewById(R.id.complet_address);
        landmark = (EditText) findViewById(R.id.landmark);
        std_code = (EditText) findViewById(R.id.std_code);
        landline = (EditText) findViewById(R.id.landline);
        mobile = (EditText) findViewById(R.id.mobile);
        helpline = (EditText) findViewById(R.id.helpLine);
        email = (EditText) findViewById(R.id.email);
        whatsapp = (EditText) findViewById(R.id.whatsapp);
        facebook_link = (EditText) findViewById(R.id.fb_link);
        instagram_link = (EditText) findViewById(R.id.instagram_link);
        website_url = (EditText) findViewById(R.id.website_url);
        twitter_link = (EditText) findViewById(R.id.twitter_link);

        taptoopenmap = (TextView) findViewById(R.id.map_opener);

        fieldsLayout = (LinearLayout) findViewById(R.id.fieldsLayout);
        servicelayout = (LinearLayout) findViewById(R.id.service_n_aminities_layout);
        scroll = (ScrollView) findViewById(R.id.common_fields_scroll);

        next = (Button) findViewById(R.id.next);
        submit = (Button) findViewById(R.id.submit);

        terms = (CheckBox) findViewById(R.id.terms);
        sun = (CheckBox) findViewById(R.id.check_sunday);
        mon = (CheckBox) findViewById(R.id.check_monday);
        tue = (CheckBox) findViewById(R.id.check_tuesday);
        wed = (CheckBox) findViewById(R.id.check_wednesday);
        thu = (CheckBox) findViewById(R.id.check_thursday);
        fri = (CheckBox) findViewById(R.id.check_friday);
        sat = (CheckBox) findViewById(R.id.check_saturday);

        logout = (ImageView) findViewById(R.id.logout);
        cover_photo = (ImageView) findViewById(R.id.cover_photo);
        add_cover_photo = (ImageView) findViewById(R.id.add_cover_photo);
        banner_photo = (ImageView) findViewById(R.id.banner_photo);

        add_banner_photo = (ImageView) findViewById(R.id.add_banner_photo);


        state_spinner = (Spinner) findViewById(R.id.state_spinner);
        district_spinner = (Spinner) findViewById(R.id.district_spinner);
        constituency_spinner = (Spinner) findViewById(R.id.constituency_spinner);
        categories_spinner = (Spinner) findViewById(R.id.category_spinner);
        everyday_opening_spinner = (Spinner) findViewById(R.id.everyday_opening_spinner);
        everyday_closing_spinner = (Spinner) findViewById(R.id.everyday_closing_spinner);
        holiday_opening_spinner = (Spinner) findViewById(R.id.holiday_opening_spinner);
        holiday_closing_spinner = (Spinner) findViewById(R.id.holiday_closing_spinner);


        //progressBar = (ProgressBar) findViewById(R.id.progress_bar);

        states.add("Select");
        categories.add("Select");

        timings.add("Select");
        timings.add("00:00");
        timings.add("00:15");
        timings.add("00:30");
        timings.add("00:45");
        timings.add("01:00");
        timings.add("01:15");
        timings.add("01:30");
        timings.add("01:45");
        timings.add("02:00");
        timings.add("02:15");
        timings.add("02:30");
        timings.add("02:45");
        timings.add("03:00");
        timings.add("03:15");
        timings.add("03:30");
        timings.add("03:45");
        timings.add("04:00");
        timings.add("04:15");
        timings.add("04:30");
        timings.add("04:45");
        timings.add("05:00");
        timings.add("05:15");
        timings.add("05:30");
        timings.add("05:45");
        timings.add("06:00");
        timings.add("06:15");
        timings.add("06:30");
        timings.add("06:45");
        timings.add("07:00");
        timings.add("07:15");
        timings.add("07:30");
        timings.add("07:45");
        timings.add("08:00");
        timings.add("08:15");
        timings.add("08:30");
        timings.add("08:45");
        timings.add("09:00");
        timings.add("09:15");
        timings.add("09:30");
        timings.add("09:45");
        timings.add("10:00");
        timings.add("10:15");
        timings.add("10:30");
        timings.add("10:45");
        timings.add("11:00");
        timings.add("11:15");
        timings.add("11:30");
        timings.add("11:45");
        timings.add("12:00");
        timings.add("12:15");
        timings.add("12:30");
        timings.add("12:45");
        timings.add("13:00");
        timings.add("13:15");
        timings.add("13:30");
        timings.add("13:45");
        timings.add("14:00");
        timings.add("14:15");
        timings.add("14:30");
        timings.add("14:45");
        timings.add("15:00");
        timings.add("15:15");
        timings.add("15:30");
        timings.add("15:45");
        timings.add("16:00");
        timings.add("16:15");
        timings.add("16:30");
        timings.add("16:45");
        timings.add("17:00");
        timings.add("17:15");
        timings.add("17:30");
        timings.add("17:45");
        timings.add("18:00");
        timings.add("18:15");
        timings.add("18:30");
        timings.add("18:45");
        timings.add("19:00");
        timings.add("19:15");
        timings.add("19:30");
        timings.add("19:45");
        timings.add("20:00");
        timings.add("20:15");
        timings.add("20:30");
        timings.add("20:45");
        timings.add("21:00");
        timings.add("21:15");
        timings.add("21:30");
        timings.add("21:45");
        timings.add("22:00");
        timings.add("22:15");
        timings.add("22:30");
        timings.add("22:45");
        timings.add("23:00");
        timings.add("23:15");
        timings.add("23:30");
        timings.add("23:45");


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                android.R.layout.simple_spinner_item, timings);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        everyday_opening_spinner.setAdapter(adapter);
        everyday_closing_spinner.setAdapter(adapter);
        holiday_opening_spinner.setAdapter(adapter);
        holiday_closing_spinner.setAdapter(adapter);


        servicesListview = (ListView) findViewById(R.id.services_recycle);
        aminitiesListView = (ListView) findViewById(R.id.amenities_recycle);
        subCategoryListView = (ListView) findViewById(R.id.subCategory);
        subCategoryListView.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);
        selectedSubcategoryListView = (ListView) findViewById(R.id.selected_subcategory);

    }


}



