package com.kwiky.executivemodule.Activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.provider.MediaStore;
import android.telephony.gsm.SmsManager;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kwiky.executivemodule.Adapters.CategoryAdapter;
import com.kwiky.executivemodule.Config.Config;
import com.kwiky.executivemodule.Config.SubCategorySelection;
import com.kwiky.executivemodule.Helpers.CustomDialog;
import com.kwiky.executivemodule.Helpers.LoadingDialog;
import com.kwiky.executivemodule.Helpers.NetworkUtil;
import com.kwiky.executivemodule.Helpers.Utility;
import com.kwiky.executivemodule.Helpers.Validations;
import com.kwiky.executivemodule.Helpers.netwrokHelpers.ConnectionDetector;
import com.kwiky.executivemodule.Helpers.uiHelpers.UImsgs;
import com.kwiky.executivemodule.PojoClass.TermsandConditions;
import com.kwiky.executivemodule.R;
import com.kwiky.executivemodule.models.CategoryObject;
import com.kwiky.executivemodule.utilities.PreferenceManager;

import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.security.SecureRandom;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_SMS;
import static android.Manifest.permission.RECEIVE_SMS;
import static android.Manifest.permission.SEND_SMS;
import static android.view.KeyEvent.KEYCODE_DEL;
import static android.widget.GridLayout.HORIZONTAL;
import static com.kwiky.executivemodule.Config.Config.APP_ID;
import static com.kwiky.executivemodule.Config.Config.APP_ID_VALUE;
import static com.kwiky.executivemodule.Config.Config.CATEGORY_LIST;
import static com.kwiky.executivemodule.Config.Config.GET_TERMSCONDITIONS;
import static com.kwiky.executivemodule.Config.Config.MAINTENANCE;
import static com.kwiky.executivemodule.Config.Config.OOPS;
import static com.kwiky.executivemodule.Config.Config.States;
import static com.kwiky.executivemodule.Config.Config.USER_TOKEN;
import static com.kwiky.executivemodule.Config.Config.VendorCreation;
import static com.kwiky.executivemodule.utilities.ValidationMessages.EMPTY;
import static com.kwiky.executivemodule.utilities.ValidationMessages.EMPTY_NOT_ALLOWED;
import static com.kwiky.executivemodule.utilities.ValidationMessages.INVALID;
import static com.kwiky.executivemodule.utilities.ValidationMessages.INVALID_MOBILE;


public class RegisterVendorActivity extends AppCompatActivity implements LocationListener, SubCategorySelection {

    private CustomDialog customDialog;
    public String catName;
    Validations validations;
    public static final int UPDATE_LOCATION = 999;
    private boolean isCameraPermissionGranted;

    private final static int REQUEST_CHECK_SETTINGS_GPS = 0x1;
    private final static int REQUEST_ID_MULTIPLE_PERMISSIONS = 0x2;
    Double lattitude, longitude;
    String currentAddress;
    String otp_str;
    Bundle bundle;
    Context mContext;
    SharedPreferences sharedPreferences;
    PreferenceManager preferenceManager;
    String sID;
    int PLACE_PICKER_REQUEST = 1;
    int count = 0;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1, SELECT_MULTIPLE_FILE = 1;
    private String userChoosenTask;
    public static String coverPhotStringImageUrl1 = "";
    public static String bannerPhotStringImageUrl1 = "";
    public int flag = 0;
    char imageSelection;
    LocationManager locationManager;
    ConnectionDetector connectionDetector;
    EditText tv_gst_no,tv_labour_certificate_no,tv_fssai_no,tv_owner_name,pincode;

    ArrayList<String> openingTimeList = new ArrayList<String>();
    ArrayList<String> closingTimeList = new ArrayList<String>();
    TermsandConditions termsandConditions;

    ArrayList<String> selectedSubCategoryList = new ArrayList<String>();
    ArrayList<String> selectedAmenitiesList = new ArrayList<String>();
    ArrayList<String> selectedServicesList = new ArrayList<String>();
    ArrayList<String> statesId = new ArrayList<>();
    ArrayList<String> states = new ArrayList<>();
    ArrayList<String> districtsId = new ArrayList<>();
    ArrayList<String> districts = new ArrayList<>();
    ArrayList<String> constituenciesId = new ArrayList<>();
    ArrayList<String> constituencies = new ArrayList<>();
    ArrayList<String> categoryId = new ArrayList<>();
    ArrayList<String> categories = new ArrayList<>();
    ArrayList<CategoryObject> ListCategoryObject=new ArrayList<>();
    ArrayList<String> timings = new ArrayList<>();
    Map<String, String> bannerBase64 = new HashMap<>();
    Map<String, String> servicesMap = new HashMap<>();
    Map<String, String> amenitiesMap = new HashMap<>();
    Map<String, Object> subCategorymap = new HashMap<>();
    Map<String, String> subCategorydatamap = new HashMap<>();
    Map<String, String> servicesdatamap = new HashMap<>();
    Map<String, String> amenitiesdatamap = new HashMap<>();
    ArrayList<String> amenitiesList = new ArrayList<>();
    ArrayList<String> servicesLits = new ArrayList<>();
    ArrayList<String> subcategoriesList = new ArrayList<>();

    ArrayList<String> servicesids = new ArrayList<>();
    ArrayList<String> selectedservicesid = new ArrayList<>();

    ArrayList<String> amenitiesids = new ArrayList<>();
    ArrayList<String> selectedamenitiesids = new ArrayList<>();

    ArrayList<String> subcategoryids = new ArrayList<>();
    ArrayList<String> selectedsubcategoryids = new ArrayList<>();


    EditText newListName,  complete_address,
            landmark, std_code, landline, mobile, altMobile, helpline, email,
            whatsapp, verify_mobile,
            otp_E_text, referal_id,country_code,country_code1;
    TextInputEditText business_pwd,re_business_pwd;
    TextView taptoopenmap, tv_sub_categories,location_et;
    LinearLayout fieldsLayout, servicelayout;
    ListView timing_listView;
    ScrollView scroll;
    Button /*next,*/ submit;
    CheckBox terms;
    ImageView back, cover_photo, add_cover_photo, add_banner_photo;
    ImageView banner_photo;
    Spinner state_spinner, district_spinner, constituency_spinner, categories_spinner,PartSpinner;

    ListView servicesListview, aminitiesListView, subCategoryListView, selectedSubcategoryListView;
    RelativeLayout layout_category;

    WebView webView;

    String newListName_str, location_et_str, pincode_str,
            complete_address_str, landmark_str, std_code_str,
            landline_str, mobile_str, helpline_str, email_str,
            whatsapp_str, constId, password_str,re_password_str;
    public String catId;
    public ArrayList<String> selectedCategoryIDs =new ArrayList<>();

    private static final int CAMERA_REQUEST = 1888;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;


    ProgressDialog progressDialog;
    AlertDialog.Builder builder;

    String termshtml;
    private CheckBox check_sameasabove;


    //private String imagePath;
    private List<String> pathList;
    String imageEncoded;


    int range = 9;  // to generate a single number with this range, by default its 0..9
    int length = 4; // by default length is 4


    //firebase related
    private static final String TAG = "PhoneAuthActivity";

    private static final String KEY_VERIFY_IN_PROGRESS = "key_verify_in_progress";

    private static final int STATE_INITIALIZED = 1;
    private static final int STATE_CODE_SENT = 2;
    private static final int STATE_VERIFY_FAILED = 3;
    private static final int STATE_VERIFY_SUCCESS = 4;
    private static final int STATE_SIGNIN_FAILED = 5;
    private static final int STATE_SIGNIN_SUCCESS = 6;


    private boolean mVerificationInProgress = false;
    private String mVerificationId;


    private ViewGroup mPhoneNumberViews;
    private ViewGroup mSignedInViews;

    private TextView mStatusText;
    private TextView mDetailText;

    private EditText mPhoneNumberField;
    private EditText mVerificationField;

    private Button mStartButton;
    private Button mVerifyButton;
    private Button mResendButton;
    private Button mSignOutButton;
    private boolean isOtpVerified=false;
    private String verifiedmobile;


    private ArrayList permissionsToRequest;
    private ArrayList permissionsRejected = new ArrayList();
    private ArrayList permissions = new ArrayList();
    private final static int ALL_PERMISSIONS_RESULT = 101;
    private boolean checkstateclick=false,districtcheck=false,constitencycheck=false;

    @Override
    protected void onResume() {
        super.onResume();
        if (!connectionDetector.isConnectingToInternet()) {
            //Toast.makeText(mContext, "P", Toast.LENGTH_SHORT).show();
            UImsgs.showToast(mContext, "Check Your connection");
        }
    }
    @Override
    public void onPause() {
        super.onPause();
    }


    private void getOtpFromMessage(String message) {
        // This will match any 6 digit number in the message
        Pattern pattern = Pattern.compile("(|^)\\d{6}");
        Matcher matcher = pattern.matcher(message);
        if (matcher.find()) {
            System.out.println("aaaaaaa  otp "+matcher.group(0));
            String otp=matcher.group(0);
            if(editTextone!=null) {
                editTextone.setText("" + otp.charAt(0));
                editTexttwo.setText("" + otp.charAt(1));
                editTextthree.setText("" + otp.charAt(2));
                editTextfour.setText("" + otp.charAt(3));
                editTextFive.setText("" + otp.charAt(4));
                editTextSix.setText("" + otp.charAt(5));
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (!connectionDetector.isConnectingToInternet()) {
            //Toast.makeText(mContext, "P", Toast.LENGTH_SHORT).show();
            UImsgs.showToast(mContext, "Check Your connection");
        }
    }

    private String blockCharacterSet = "@~#^|$%&*!";

    private InputFilter filter = new InputFilter() {

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

            if (source != null && blockCharacterSet.contains(("" + source))) {
                return "";
            }
            return null;
        }
    };

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_register_vendor_refactor);//layout_register_vendor
      //  getSupportActionBar().hide();
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.Iconblue));
        }
        mContext = RegisterVendorActivity.this;
        customDialog=new CustomDialog(mContext);
        init();
        connectionDetector = new ConnectionDetector(mContext);


        validations = new Validations();
        preferenceManager = new PreferenceManager(mContext);

        servicelayout.setVisibility(View.GONE);

        //progressBar.setVisibility(View.VISIBLE);
        progressDialog = new ProgressDialog(this);
        progressDialog.setIcon(R.drawable.nextclick_logo_black);
        progressDialog.setMessage("Please wait while fetching fields data.....");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setProgress(0);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        AsyncTaskRunner asyncTaskRunner = new AsyncTaskRunner();
        asyncTaskRunner.execute();


        permissions.add(READ_SMS);
        permissions.add(RECEIVE_SMS);
        permissions.add(SEND_SMS);

        permissionsToRequest = findUnAskedPermissions(permissions);
        if (permissionsToRequest.size() > 0)
            requestPermissions((String[]) permissionsToRequest.toArray(new String[permissionsToRequest.size()]),
                    ALL_PERMISSIONS_RESULT);



        //Chechking Versions



        getLocation();
        if (NetworkUtil.isNetworkConnected(RegisterVendorActivity.this)) {
            try{
                if (checkstateclick){

                }else {
                    checkstateclick=true;
                    statesDataFetcher();

                }
            }catch (IllegalStateException e){

            }

        } else {
            Toast.makeText(RegisterVendorActivity.this, ""+getResources().getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
        }

        if (NetworkUtil.isNetworkConnected(RegisterVendorActivity.this)) {
            categoryDataFetcher();
        } else {
            Toast.makeText(RegisterVendorActivity.this, ""+getResources().getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
        }


        builder = new AlertDialog.Builder(this);
        terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (NetworkUtil.isNetworkConnected(RegisterVendorActivity.this)) {
                    gettermsandconditions();
                } else {
                    Toast.makeText(RegisterVendorActivity.this, ""+getResources().getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
                }




            }
        });

        bundle = getIntent().getExtras();

        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 101);

        }
        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 101);
        }


        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA},
                    MY_CAMERA_PERMISSION_CODE);
        }

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();

            }
        });
        state_spinner.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                checkstateclick= true;
                return false;
            }
        });  district_spinner.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                 districtcheck= true;
                return false;
            }
        });  constituency_spinner.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                 constitencycheck= true;
                return false;
            }
        });

        state_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
               try{
                   if (checkstateclick){
                       if(view!=null)
                       {//write your code
                           if ((position - 1) >= 0) {
                               sID = statesId.get(position - 1);
                               districtsId.clear();
                               districts.clear();
                               districtsDataFetcher(sID);
                           }
                       }

                   }else {
                       checkstateclick=false;


                }
               }catch (IllegalStateException e){

               }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        district_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                try{
                    if (districtcheck){
                        if(view!=null)
                        {//write your code
                            if ((position - 1) >= 0 && sID != null) {
                                String dID = districtsId.get(position - 1);
                                constituencies.clear();
                                constituenciesId.clear();
                                constituenciesDataFetcher(sID, dID);
                            }
                        }

                    }else {
                        districtcheck=false;


                    }
                }catch (IllegalStateException e){

                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        cover_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageSelection = 'c';
                boolean selecting = selectImage();
                if (selecting) {
                    //visibilty_gone.setVisibility(View.GONE);
                }
            }
        });

        banner_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageSelection = 'b';
                boolean selecting = selectImage();
                if (selecting) {
                    //visibilty_gone.setVisibility(View.GONE);
                }
               /* Intent mIntent = new Intent(getApplicationContext(), PickImageActivity.class);
                mIntent.putExtra(PickImageActivity.KEY_LIMIT_MAX_IMAGE, 60);
                mIntent.putExtra(PickImageActivity.KEY_LIMIT_MIN_IMAGE, 3);
                startActivityForResult(mIntent, PickImageActivity.PICKER_REQUEST_CODE);
*/

            }
        });

       /* categories_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!categories_spinner.getSelectedItem().toString().equalsIgnoreCase("select")) {
                    servicesDataFetcher(categoryId.get(categories_spinner.getSelectedItemPosition() - 1));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dataFetcherFromFields();
                boolean isValid=validator();
                if (isValid)
                {
                    if (terms.isChecked()) {
                        boolean callRegisterAPI=false;//isOtpVerified &&  mobile_str.equals(verifiedmobile)
                        if(callRegisterAPI)
                        {
                            dataSender();
                        }
                        else
                            mailValidator(progressDialog);
                    } else {
                        UImsgs.showToast(mContext, "Please accept terms and conditions");
                    }
                }
            }
        });


        final ArrayAdapter<String> subcategoryadapter = new ArrayAdapter<String>(getApplicationContext(),
                android.R.layout.simple_dropdown_item_1line, selectedSubCategoryList);
        subCategoryListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int len = subCategoryListView.getCount();
                SparseBooleanArray checked = subCategoryListView.getCheckedItemPositions();

                String item = subcategoriesList.get(position);
                if (checked.get(position)) {

                    /* do whatever you want with the checked item */
                    selectedSubCategoryList.add(item);
                    selectedsubcategoryids.add(subcategoryids.get(position));
                    subcategoryadapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
                    selectedSubcategoryListView.setAdapter(subcategoryadapter);
                } else {
                    selectedSubCategoryList.remove(item);
                    selectedsubcategoryids.remove(subcategoryids.get(position));
                    subcategoryadapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
                    selectedSubcategoryListView.setAdapter(subcategoryadapter);
                }
            }
        });
        layout_category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCategoryPopup();
            }
        });

        aminitiesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                int len = aminitiesListView.getCount();
                SparseBooleanArray checked = aminitiesListView.getCheckedItemPositions();

                String item = amenitiesList.get(position);
                if (checked.get(position)) {

                    /* do whatever you want with the checked item */
                    selectedamenitiesids.add(amenitiesids.get(position));
                } else {
                    selectedamenitiesids.remove(amenitiesids.get(position));
                }

            }
        });

        servicesListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                int len = servicesListview.getCount();
                SparseBooleanArray checked = servicesListview.getCheckedItemPositions();

                //String item = amenitiesList.get(position);
                if (checked.get(position)) {

                    /* do whatever you want with the checked item */
                    selectedservicesid.add(servicesids.get(position));
                } else {
                    selectedservicesid.remove(servicesids.get(position));
                }


            }
        });

    }

    InputFilter filter1 = new InputFilter() {
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest,
                                   int dstart, int dend) {
            for (int i=start; i<end; i++) {
                if (!Character.isLetterOrDigit(source.charAt(i)) &&
                        !Character.isLowerCase(source.charAt(i))) {
                    return "";
                }
            }
            return null;
        }
    };


    private void gettermsandconditions() {

        customDialog.show();
       /* Map<String, String> termsmpa = new HashMap<>();
        termsmpa.put("?page_id", "1");
        final String data = new JSONObject(termsmpa).toString();*/

        String url=GET_TERMSCONDITIONS+"?page_id=1";
        System.out.println("aaaaaaaaaa url  "+url);

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("cat_res", response);
                        customDialog.dismiss();
                        if (response != null) {
                            try {

                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {

                                    String title,desc;

                                    try{
                                        JSONArray jsonArray=jsonObject.getJSONArray("data");
                                        title=jsonArray.getJSONObject(0).getString("title");
                                        desc=jsonArray.getJSONObject(0).getString("desc");

                                    }catch (Exception e){
                                        JSONObject dataobject = jsonObject.getJSONObject("data");
                                        title=dataobject.getString("title");
                                        desc=dataobject.getString("desc");
                                    }


                                 //   JSONObject dataobject = jsonObject.getJSONObject("data");
                                   /*  termsandConditions=new TermsandConditions();
                                    termsandConditions.setId(dataobject.getString("id"));
                                    termsandConditions.setApp_details_id(dataobject.getString("app_details_id"));
                                    termsandConditions.setTitle(dataobject.getString("title"));
                                    termsandConditions.setPage_id(dataobject.getString("page_id"));
                                    termsandConditions.setDesc(dataobject.getString("desc"));
*/
                                    LayoutInflater inflater = getLayoutInflater();
                                    View alertLayout = inflater.inflate(R.layout.activity_web, null);

                                    webView = alertLayout.findViewById(R.id.ss_web);
                                    TextView tv_terms_id = alertLayout.findViewById(R.id.tv_terms_id);


                                    AlertDialog.Builder alert = new AlertDialog.Builder(RegisterVendorActivity.this);
                                    alert.setIcon(R.mipmap.ic_launcher);
                                    alert.setTitle(title);
                                    // this is set the view from XML inside AlertDialog
                                    alert.setView(alertLayout);
                                    alert.setCancelable(false);
                                    WebSettings webSettings = webView.getSettings();
                                    Resources res = getResources();
                                    int fontSize = 10;
                                    webSettings.setDefaultFontSize(fontSize);
                                    webSettings.setJavaScriptEnabled(true);
                                    tv_terms_id.setText(Html.fromHtml(desc));
                                    webView.loadDataWithBaseURL(null, desc, "text/html; charset=UTF-8", "utf-8", null);
                                    alert.setCancelable(true);
                                    alert.setPositiveButton("Accept", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {

                                            terms.setChecked(true);
                                         //   accepttermsandconditions(termsandConditions.getId());
                                        }
                                    })
                                           /* .setNegativeButton("Decline", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    //  Action for 'NO' Button
                                                    terms.setChecked(false);
                                                }
                                            })*/;
                                    AlertDialog dialog = alert.create();
                                    dialog.show();

                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                                showToast(mContext,"Terms and Conditions API failed :"+response);
                            }
                        } else {
                            customDialog.dismiss();
                            UImsgs.showToast(mContext, MAINTENANCE);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        customDialog.dismiss();
                        UImsgs.showToast(mContext, OOPS);
                        System.out.println("aaaaaaa  111 "+error.getMessage());
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(APP_ID, "TWc9PQ==");
                return map;
            }

           /* @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }*/
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }


    public void setSelectedCategoryIDs(ArrayList<String> selectedCategoryIDs, ArrayList<String> selectedCategoryNames) {
        this.selectedCategoryIDs=selectedCategoryIDs;
        StringBuilder str = new StringBuilder("");
        for (String eachstring : selectedCategoryNames) {
            str.append(eachstring).append(",");
        }


        String finalString=str.toString();
        if(finalString.endsWith(","))
            finalString=finalString.substring(0,finalString.length()-1);


        String text = "<font color=#F26B35> Selected Category : </font> <font color=#333333>" + catName+ "</font><br>"+
                "\n <font color=#F26B35> Sub Categories : </font> <font color=#333333>" + finalString+ "</font>";

        tv_sub_categories.setText(Html.fromHtml(text));
    }

    @Override
    public void setSelectedCategoryIDs(ArrayList<String> selectedCategoryIDs) {
        this.selectedCategoryIDs=selectedCategoryIDs;
    }

    @Override
    public void setCategory(CategoryObject category) {
        this.catId = category.getCatID();
        this.catName = category.getCatName();
    }

    class AsyncTaskRunner extends AsyncTask<String, String, String> {

        private String resp;
        ProgressDialog progressDialog;

        @Override
        protected String doInBackground(String... params) {
            publishProgress("No Connection..."); // Calls onProgressUpdate()
            if (!connectionDetector.isConnectingToInternet()) {
                resp = "No Internet";
                /*Intent intent = new Intent(mContext, ConnectionAlert.class);
                startActivity(intent);
                finish();*/
                Toast.makeText(mContext, resp, Toast.LENGTH_SHORT).show();
            } else {
                resp = "Connected";
            }

            return resp;
        }


        @Override
        protected void onPostExecute(String result) {
            // execution of result of Long time consuming operation
               /* progressDialog.dismiss();
                finalResult.setText(result);*/
            if (!connectionDetector.isConnectingToInternet()) {
                resp = "No Internet";
                /*Intent intent = new Intent(mContext, ConnectionAlert.class);
                startActivity(intent);
                finish();*/
                Toast.makeText(mContext, resp, Toast.LENGTH_SHORT).show();
            } else {
                resp = "Connected";
            }


        }


        @Override
        protected void onPreExecute() {
               /* progressDialog = ProgressDialog.show(MainActivity.this,
                        "ProgressDialog",
                        "Wait for "+time.getText().toString()+ " seconds");*/
           /* if(!connectionDetector.isConnectingToInternet()){
                resp="No Internet";
                Intent intent = new Intent(mContext,ConnectionAlert.class);
                startActivity(intent);
                finish();
            }
            else {
                resp = "Connected";
            }
*/
        }


        @Override
        protected void onProgressUpdate(String... text) {
            /*finalResult.setText(text[0]);*/
            if (!connectionDetector.isConnectingToInternet()) {
                resp = "No Internet";
               /* Intent intent = new Intent(mContext, ConnectionAlert.class);
                startActivity(intent);
                finish();*/
                Toast.makeText(mContext, resp, Toast.LENGTH_SHORT).show();
            } else {
                resp = "Connected";
            }


        }
    }


    /*Locataion Start*/
    @SuppressLint("MissingPermission")
    void getLocation() {

        try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 5, (LocationListener) this);
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        //locationText.setText("Latitude: " + location_et.getLatitude() + "\n Longitude: " + location_et.getLongitude());


        try {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            /*locationText.setText(locationText.getText() + "\n"+addresses.get(0).getAddressLine(0)+", "+
                    addresses.get(0).getAddressLine(1)+", "+addresses.get(0).getAddressLine(2));
            */
            Double lat1 = location.getLatitude();
            Double lang1 = location.getLongitude();
            lattitude = lat1;
            longitude = lang1;
            currentAddress = Utility.getShortcutAddress(addresses);

           location_et.setText(currentAddress);//by default we are not showing current location here

            pincode.setText(addresses.get(0).getPostalCode());

        } catch (Exception e) {

        }

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

//Location End


    //Image Selection Start

    private boolean selectImage() {
        final CharSequence[] items = {"Choose from Library", "Open Camera",
                "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(RegisterVendorActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.M)

            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = Utility.checkPermission(RegisterVendorActivity.this);

                if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";
                    if (result) {    //Calling Gallery Marhod For Images
                        galleryIntent();
                    }

                } else if (items[item].equals("Open Camera")) {
                    userChoosenTask = "Open Camera";
                    if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        isCameraPermissionGranted=true;
                        //requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
                        ActivityCompat.requestPermissions(RegisterVendorActivity.this, new String[]{Manifest.permission.CAMERA},
                                MY_CAMERA_PERMISSION_CODE);

                    } else {
                        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, CAMERA_REQUEST);
                    }

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
        return true;
    }

    //Calling Openig Gallery For Images
    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode== UPDATE_LOCATION) {
            updateLocation(data);
        }
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);


        }
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            if (imageSelection == 'c') {
                Bitmap photo = (Bitmap) data.getExtras().get("data");
                cover_photo.setImageBitmap(photo);

                Bitmap bitmap = ((BitmapDrawable) cover_photo.getDrawable()).getBitmap();

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

                byte[] byteArray = baos.toByteArray();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
                    coverPhotStringImageUrl1 = Base64.encodeToString(byteArray, Base64.DEFAULT);
                }
            }
            if (imageSelection == 'b') {
                bannerBase64.clear();
                Bitmap photo = (Bitmap) data.getExtras().get("data");
                banner_photo.setImageBitmap(photo);
                Bitmap bitmap = ((BitmapDrawable) banner_photo.getDrawable()).getBitmap();

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

                byte[] byteArray = baos.toByteArray();
                bannerPhotStringImageUrl1 = Base64.encodeToString(byteArray, Base64.DEFAULT);
                bannerBase64.put(0 + "".trim(), bannerPhotStringImageUrl1);
            }
            //  System.out.println("ByteArra"+coverPhotStringImageUrl1);

            flag = 1;
        }

    }


    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
               /* Toast.makeText(mContext, data.getData() + "", Toast.LENGTH_SHORT).show();
                Log.d("Data", data.getData().toString());*/

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        bm = Bitmap.createScaledBitmap(bm, 512, 512, false);

        if (imageSelection == 'c') {

            cover_photo.setImageBitmap(bm);

            Bitmap bitmap = ((BitmapDrawable) cover_photo.getDrawable()).getBitmap();

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

            byte[] byteArray = baos.toByteArray();
            coverPhotStringImageUrl1 = Base64.encodeToString(byteArray, Base64.DEFAULT);
            //  System.out.println("ByteArra"+coverPhotStringImageUrl1);

            flag = 1;
        }
        if (imageSelection == 'b') {

            banner_photo.setImageBitmap(bm);

            Bitmap bitmap = ((BitmapDrawable) banner_photo.getDrawable()).getBitmap();

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

            byte[] byteArray = baos.toByteArray();
            bannerPhotStringImageUrl1 = Base64.encodeToString(byteArray, Base64.DEFAULT);
            //  System.out.println("ByteArra"+coverPhotStringImageUrl1);
            bannerBase64.put(0 + "".trim(), bannerPhotStringImageUrl1);

            flag = 1;
        }

    }

    private void onSelectFromGalleryResult(ArrayList data) {


        bannerBase64.clear();

        for (int i = 0; i < data.size(); i++) {

            Bitmap bitmap = (Bitmap) data.get(i);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

            byte[] byteArray = baos.toByteArray();
            String bannerBitString = Base64.encodeToString(byteArray, Base64.DEFAULT);
            bannerBase64.put(i + "".trim(), bannerBitString);
            //  System.out.println("ByteArra"+coverPhotStringImageUrl1);


            flag = 1;
        }


    }


    //Image Selection End


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        switch (requestCode) {

            case ALL_PERMISSIONS_RESULT:
                for (Object perms : permissionsToRequest) {
                    if (!hasPermission((String) perms)) {
                        permissionsRejected.add(perms);
                    }
                }

                if (permissionsRejected.size() > 0) {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale((String) permissionsRejected.get(0))) {
                          /*  showMessageOKCancel("These permissions are mandatory for the application. Please allow access.",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions((String[]) permissionsRejected.toArray(new
                                                        String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    });*/
                            return;
                        }
                    }

                }

                break;
            default:
                int permissionLocation = ContextCompat.checkSelfPermission(RegisterVendorActivity.this,
                        Manifest.permission.ACCESS_FINE_LOCATION);
                if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
                    getLocation();
                }

                /*if(isCameraPermissionGranted) {
                    int cameraLocation = ContextCompat.checkSelfPermission(RegisterVendorActivity.this,
                            CAMERA);
                    if (cameraLocation == PackageManager.PERMISSION_GRANTED) {
                        isCameraPermissionGranted =false;
                        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, REQUEST_CAMERA);
                    }

                }*/
                break;
        }
    }


    public void statesDataFetcher() {
        System.out.println("aaaaaaaaaa check ");
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, States, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonData = new JSONObject(response);
                    String status = jsonData.getString("status");
                    String messagae = jsonData.getString("message");
                    if (status.equalsIgnoreCase("true") && messagae.equalsIgnoreCase("Success..!")) {

                        JSONArray dataArray = jsonData.getJSONArray("data");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataObject = dataArray.getJSONObject(i);
                            statesId.add(dataObject.getString("id"));
                            states.add(dataObject.getString("name"));
                        }
                        // progressBar.setVisibility(View.GONE);

                    } else {
                        UImsgs.showToast(mContext, messagae);
                        //Toast.makeText(LoginActivity.this, "Invalid Credentials", Toast.LENGTH_SHORT).show();
                    }


                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                            R.layout.simple_spinner_item, states);
                   // adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    state_spinner.setAdapter(adapter);
                  //  state_spinner.setPadding(0,15,0,15);

                } catch (JSONException e) {
                    e.printStackTrace();
                    UImsgs.showToast(mContext, "No data found...!");
                    progressDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(LoginActivity.this, error+"", Toast.LENGTH_SHORT).show();

            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
               // map.put("X_AUTH_TOKEN", token);

                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(8000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);


    }

    public void districtsDataFetcher(String stateid) {
        progressDialog.show();
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, States + "/" + stateid, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonData = new JSONObject(response);
                    String status = jsonData.getString("status");
                    String messagae = jsonData.getString("message");
                    if (status.equalsIgnoreCase("true") && messagae.equalsIgnoreCase("Success..!")) {

                        JSONObject dataObject = jsonData.getJSONObject("data");
                        JSONArray dataArray = dataObject.getJSONArray("districts");
                        districts.add("Select");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject districtObject = dataArray.getJSONObject(i);
                            districtsId.add(districtObject.getString("id"));
                            districts.add(districtObject.getString("name"));
                        }

                    } else {
                        UImsgs.showToast(getApplicationContext(), messagae);
                        //Toast.makeText(LoginActivity.this, "Invalid Credentials", Toast.LENGTH_SHORT).show();
                    }

                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                            android.R.layout.simple_spinner_item, districts);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    district_spinner.setAdapter(adapter);
                    progressDialog.dismiss();

                } catch (JSONException e) {
                    e.printStackTrace();
                    UImsgs.showToast(mContext, "No data found...!");
                    progressDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(LoginActivity.this, error+"", Toast.LENGTH_SHORT).show();

            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                //map.put("X_AUTH_TOKEN", token);

                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(8000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);


    }

    public void constituenciesDataFetcher(String stateid, String districtid) {
        progressDialog.show();
        final int err = 0;
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, States + "/" + stateid + "/" + districtid, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("constituencies_resp", response);

                try {
                    JSONObject jsonData = new JSONObject(response);
                    String status = jsonData.getString("status");
                    String messagae = jsonData.getString("message");
                    if (status.equalsIgnoreCase("true") && messagae.equalsIgnoreCase("Success..!")) {

                        constituencies.add("Select");
                        JSONObject dataObject = jsonData.getJSONObject("data");
                        JSONArray dataArray = dataObject.getJSONArray("constituenceis");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject districtObject = dataArray.getJSONObject(i);
                            constituenciesId.add(districtObject.getString("id"));
                            constituencies.add(districtObject.getString("name"));
                        }

                    } else {
                        UImsgs.showToast(mContext, messagae);
                        //Toast.makeText(LoginActivity.this, "Invalid Credentials", Toast.LENGTH_SHORT).show();
                    }


                    //PartSpinner.setAdapter(adapter);

                    progressDialog.dismiss();

                } catch (JSONException e) {
                    e.printStackTrace();
                    // UIMsgs.showToast(mContext, "No data found...!");
                    progressDialog.dismiss();
                }
                finally {
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                            android.R.layout.simple_spinner_item, constituencies);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    constituency_spinner.setAdapter(adapter);
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(LoginActivity.this, error+"", Toast.LENGTH_SHORT).show();


            }

        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                //map.put("X_AUTH_TOKEN", token);
                return map;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(8000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);


    }

    public void categoryDataFetcher() {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, CATEGORY_LIST, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonData = new JSONObject(response);
                    String status = jsonData.getString("status");
                    String messagae = jsonData.getString("message");
                    if (status.equalsIgnoreCase("true") && messagae.equalsIgnoreCase("Success..!")) {

                        JSONArray dataArray = jsonData.getJSONArray("data");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataObject = dataArray.getJSONObject(i);
                            CategoryObject c=new CategoryObject();
                            c.setCatID(dataObject.getString("id"));
                            c.setCatName(dataObject.getString("name"));
                            c.setCatImage(dataObject.getString("image"));
                           /* categoryId.add(c.getCatID());
                            categories.add(c.getCatName());*/
                            ListCategoryObject.add(c);
                        }

                    } else {
                        UImsgs.showToast(mContext, messagae);
                        //Toast.makeText(LoginActivity.this, "Invalid Credentials", Toast.LENGTH_SHORT).show();
                    }


                   /* ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                            android.R.layout.simple_spinner_item, categories);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    categories_spinner.setAdapter(adapter);*/
                    progressDialog.dismiss();

                } catch (JSONException e) {
                    e.printStackTrace();
                    UImsgs.showToast(mContext, String.valueOf(e));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(RegisterVendorActivity.this, error + "", Toast.LENGTH_SHORT).show();
                categoryDataFetcher();

            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                //map.put("X_AUTH_TOKEN", token);
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(8000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }


    public void servicesDataFetcher(final String categoryId) {
        //String tempCategories = Categories + "1";
        String tempCategories = CATEGORY_LIST + categoryId;

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, tempCategories, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonData = new JSONObject(response);
                    Boolean type;
                    String status = jsonData.getString("status");
                    String messagae = jsonData.getString("message");
                    String datatype = jsonData.get("data").getClass().getName();
                    if (datatype.equalsIgnoreCase("java.lang.Boolean")) {
                        type = false;
                    } else
                        type = true;
                    if (type) {
                        if (status.equalsIgnoreCase("true") && messagae.equalsIgnoreCase("Success..!")) {

                            amenitiesdatamap.clear();
                            amenitiesList.clear();
                            amenitiesids.clear();
                            amenitiesMap.clear();
                            subcategoryids.clear();
                            subcategoriesList.clear();
                            subCategorydatamap.clear();
                            subCategorymap.clear();
                            servicesids.clear();
                            servicesLits.clear();
                            servicesdatamap.clear();
                            servicesMap.clear();

                            String data = jsonData.getString("data");

                            JSONObject jsonObject = jsonData.getJSONObject("data");

                            termshtml = jsonObject.getString("terms");
                            LinearLayout aminities_layout,
                                    services_layout, sub_categories_layout;
                            aminities_layout = findViewById(R.id.aminities_layout);
                            services_layout = findViewById(R.id.services_layout);
                            sub_categories_layout = findViewById(R.id.sub_categories_layout);

                            try {
                                JSONArray amenitiesJsonArray = jsonObject.getJSONArray("amenities");
                                if (amenitiesJsonArray.length() > 0) {
                                    for (int i = 0; i < amenitiesJsonArray.length(); i++) {
                                        JSONObject ammenitiesArray = (JSONObject) amenitiesJsonArray.get(i);
                                        String amenityname = ammenitiesArray.getString("name");
                                        String amenityid = ammenitiesArray.getString("id");
                                        String amenitycatId = ammenitiesArray.getString("cat_id");
                                        amenitiesdatamap.put(amenityid, amenityname);
                                        amenitiesList.add(amenityname);
                                        amenitiesids.add(amenityid);

                                    }

                                    ViewGroup.LayoutParams params;
                                    params = aminities_layout.getLayoutParams();
                                    params.height = amenitiesList.size() * 250;
                                    aminities_layout.setLayoutParams(params);

                                    aminitiesListView.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);
                                    ArrayAdapter<String> amenitiesAdapter = new ArrayAdapter<String>(getApplicationContext(),
                                            android.R.layout.simple_list_item_multiple_choice, amenitiesList);
                                    amenitiesAdapter.setDropDownViewResource(android.R.layout.simple_list_item_multiple_choice);
                                    aminitiesListView.setAdapter(amenitiesAdapter);
                                } else {
                                    UImsgs.showToast(mContext, "Sorry...! No Amenities Found");
                                }
                            } catch (Exception ex) {
                            }

                            try {
                                JSONArray subCategoryJsonArray = jsonObject.getJSONArray("sub_categories");
                                if (subCategoryJsonArray.length() > 0) {
                                    for (int i = 0; i < subCategoryJsonArray.length(); i++) {
                                        JSONObject subCategoryObject = (JSONObject) subCategoryJsonArray.get(i);
                                        String subcategoryname = subCategoryObject.getString("name");
                                        String subcategoryid = subCategoryObject.getString("id");
                                        String subcategorycatId = subCategoryObject.getString("cat_id");
                                        subCategorydatamap.put(subcategoryid, subcategoryname);
                                        subcategoriesList.add(subcategoryname);
                                        subcategoryids.add(subcategoryid);

                                    }

                                    ViewGroup.LayoutParams params;
                                    params = sub_categories_layout.getLayoutParams();
                                    /* if(subcategoriesList.size()<=3){*/
                                    params.height = subcategoriesList.size() * 300;
                               /* }else{
                                    params.height = subcategoriesList.size()*155;
                                }*/

                                    sub_categories_layout.setLayoutParams(params);
                                } else {
                                    UImsgs.showToast(mContext, "Sorry...! No SubCategories Found");
                                }
                            } catch (Exception ex) {
                            }


                            ArrayAdapter<String> subcategoryAdapter = new ArrayAdapter<String>(getApplicationContext(),
                                    android.R.layout.simple_list_item_multiple_choice, subcategoriesList);
                            subcategoryAdapter.setDropDownViewResource(android.R.layout.simple_list_item_multiple_choice);
                            subCategoryListView.setAdapter(subcategoryAdapter);

                            try {
                                JSONObject serviceJsonobject = jsonObject.getJSONObject("services");

                                Iterator x = serviceJsonobject.keys();
                                JSONArray servicesjsonArray = new JSONArray();


                                while (x.hasNext()) {
                                    String key = (String) x.next();
                                    servicesjsonArray.put(serviceJsonobject.get(key));
                                }
                                //Toast.makeText(mContext, String.valueOf(amenitiesJsonArray), Toast.LENGTH_SHORT).show();
                                if (servicesjsonArray.length() > 0) {
                                    for (int i = 0; i < servicesjsonArray.length(); i++) {
                                        JSONObject servicesData = (JSONObject) servicesjsonArray.get(i);
                                        String servicename = servicesData.getString("name");
                                        String serviceid = servicesData.getString("id");
                                        String servicecatId = servicesData.getString("cat_id");
                                        servicesdatamap.put(serviceid, servicename);
                                        servicesLits.add(servicename);
                                        servicesids.add(serviceid);

                                    }

                                    ViewGroup.LayoutParams serviceparams = services_layout.getLayoutParams();
                                    if (servicesLits.size() <= 2) {
                                        serviceparams.height = servicesLits.size() * 500;
                                    } else {
                                        serviceparams.height = servicesLits.size() * 250;
                                    }

                                    //Toast.makeText(mContext, serviceparams.height+"", Toast.LENGTH_SHORT).show();
                                    services_layout.setLayoutParams(serviceparams);
                                    ArrayAdapter<String> serviceAdapter = new ArrayAdapter<String>(getApplicationContext(),
                                            android.R.layout.simple_list_item_multiple_choice, servicesLits);
                                    servicesListview.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);
                                    serviceAdapter.setDropDownViewResource(android.R.layout.simple_list_item_multiple_choice);
                                    servicesListview.setAdapter(serviceAdapter);
                                } else {
                                    UImsgs.showToast(mContext, "Sorry...! No Services Found");
                                }
                            } catch (Exception ex) {
                            }

                        } else {
                            UImsgs.showToast(mContext, messagae);
                            //Toast.makeText(ServicesAminitiesActivity.this, "Invalid Credentials", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        UImsgs.showToast(mContext, "Sorry..! No data found for the selected category");
                        fieldsLayout.setVisibility(View.VISIBLE);
                        servicelayout.setVisibility(View.GONE);
                    }


                    /*ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                            android.R.fieldsLayout.simple_spinner_item, states);
                    adapter.setDropDownViewResource(android.R.fieldsLayout.simple_spinner_dropdown_item);
                    state_spinner.setAdapter(adapter);*/


                } catch (JSONException e) {
                    e.printStackTrace();
                    //UIMsgs.showToast(mContext, "No data found...!");
                    progressDialog.dismiss();
                }
              /*  ServicesAminitiesAdapter servicesAminitiesAdapter = new ServicesAminitiesAdapter(mContext,serviceAminitesList);
                amenitiesRecycle.setAdapter(servicesAminitiesAdapter);*/
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(RegisterVendorActivity.this, error + "", Toast.LENGTH_SHORT).show();

            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                //map.put("X_AUTH_TOKEN", token);

                return map;
            }
        };

        /*stringRequest.setRetryPolicy(new DefaultRetryPolicy(8000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));*/
        requestQueue.add(stringRequest);
    }
    public void servicesDataFetcher_backup(final String categoryId) {
        //String tempCategories = Categories + "1";
        String tempCategories = CATEGORY_LIST + categoryId;

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, tempCategories, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonData = new JSONObject(response);
                    Boolean type;
                    String status = jsonData.getString("status");
                    String messagae = jsonData.getString("message");
                    String datatype = jsonData.get("data").getClass().getName();
                    if (datatype.equalsIgnoreCase("java.lang.Boolean")) {
                        type = false;
                    } else
                        type = true;
                    if (type) {
                        if (status.equalsIgnoreCase("true") && messagae.equalsIgnoreCase("Success..!")) {

                            amenitiesdatamap.clear();
                            amenitiesList.clear();
                            amenitiesids.clear();
                            amenitiesMap.clear();
                            subcategoryids.clear();
                            subcategoriesList.clear();
                            subCategorydatamap.clear();
                            subCategorymap.clear();
                            servicesids.clear();
                            servicesLits.clear();
                            servicesdatamap.clear();
                            servicesMap.clear();

                            String data = jsonData.getString("data");

                            JSONObject jsonObject = jsonData.getJSONObject("data");

                            termshtml = jsonObject.getString("terms");
                            LinearLayout aminities_layout,
                                    services_layout, sub_categories_layout;
                            aminities_layout = findViewById(R.id.aminities_layout);
                            services_layout = findViewById(R.id.services_layout);
                            sub_categories_layout = findViewById(R.id.sub_categories_layout);


                            JSONArray amenitiesJsonArray = jsonObject.getJSONArray("amenities");
                            //Toast.makeText(mContext, String.valueOf(amenitiesJsonArray), Toast.LENGTH_SHORT).show();
                            if (amenitiesJsonArray.length() > 0) {

                                for (int i = 0; i < amenitiesJsonArray.length(); i++) {
                                    JSONObject ammenitiesArray = (JSONObject) amenitiesJsonArray.get(i);
                                    String amenityname = ammenitiesArray.getString("name");
                                    String amenityid = ammenitiesArray.getString("id");
                                    String amenitycatId = ammenitiesArray.getString("cat_id");
                                    amenitiesdatamap.put(amenityid, amenityname);
                                    amenitiesList.add(amenityname);
                                    amenitiesids.add(amenityid);

                                }

                                ViewGroup.LayoutParams params;
                                params = aminities_layout.getLayoutParams();
                                params.height = amenitiesList.size() * 250;
                                aminities_layout.setLayoutParams(params);

                                aminitiesListView.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);
                                ArrayAdapter<String> amenitiesAdapter = new ArrayAdapter<String>(getApplicationContext(),
                                        android.R.layout.simple_list_item_multiple_choice, amenitiesList);
                                amenitiesAdapter.setDropDownViewResource(android.R.layout.simple_list_item_multiple_choice);
                                aminitiesListView.setAdapter(amenitiesAdapter);
                            } else {
                                UImsgs.showToast(mContext, "Sorry...! No Amenities Found");
                            }


                            JSONArray subCategoryJsonArray = jsonObject.getJSONArray("sub_categories");
                            if (subCategoryJsonArray.length() > 0) {
                                for (int i = 0; i < subCategoryJsonArray.length(); i++) {
                                    JSONObject subCategoryObject = (JSONObject) subCategoryJsonArray.get(i);
                                    String subcategoryname = subCategoryObject.getString("name");
                                    String subcategoryid = subCategoryObject.getString("id");
                                    String subcategorycatId = subCategoryObject.getString("cat_id");
                                    subCategorydatamap.put(subcategoryid, subcategoryname);
                                    subcategoriesList.add(subcategoryname);
                                    subcategoryids.add(subcategoryid);


                                }

                                ViewGroup.LayoutParams params;
                                params = sub_categories_layout.getLayoutParams();
                                /* if(subcategoriesList.size()<=3){*/
                                params.height = subcategoriesList.size() * 300;
                               /* }else{
                                    params.height = subcategoriesList.size()*155;
                                }*/

                                sub_categories_layout.setLayoutParams(params);

                                ArrayAdapter<String> subcategoryAdapter = new ArrayAdapter<String>(getApplicationContext(),
                                        android.R.layout.simple_list_item_multiple_choice, subcategoriesList);
                                subcategoryAdapter.setDropDownViewResource(android.R.layout.simple_list_item_multiple_choice);
                                subCategoryListView.setAdapter(subcategoryAdapter);
                            } else {
                                UImsgs.showToast(mContext, "Sorry...! No SubCategories Found");
                            }


                            JSONObject serviceJsonobject = jsonObject.getJSONObject("services");

                            Iterator x = serviceJsonobject.keys();
                            JSONArray servicesjsonArray = new JSONArray();


                            while (x.hasNext()) {
                                String key = (String) x.next();
                                servicesjsonArray.put(serviceJsonobject.get(key));
                            }
                            //Toast.makeText(mContext, String.valueOf(amenitiesJsonArray), Toast.LENGTH_SHORT).show();
                            if (servicesjsonArray.length() > 0) {
                                for (int i = 0; i < servicesjsonArray.length(); i++) {
                                    JSONObject servicesData = (JSONObject) servicesjsonArray.get(i);
                                    String servicename = servicesData.getString("name");
                                    String serviceid = servicesData.getString("id");
                                    String servicecatId = servicesData.getString("cat_id");
                                    servicesdatamap.put(serviceid, servicename);
                                    servicesLits.add(servicename);
                                    servicesids.add(serviceid);

                                }

                                ViewGroup.LayoutParams serviceparams = services_layout.getLayoutParams();
                                if (servicesLits.size() <= 2) {
                                    serviceparams.height = servicesLits.size() * 500;
                                } else {
                                    serviceparams.height = servicesLits.size() * 250;
                                }

                                //Toast.makeText(mContext, serviceparams.height+"", Toast.LENGTH_SHORT).show();
                                services_layout.setLayoutParams(serviceparams);
                                ArrayAdapter<String> serviceAdapter = new ArrayAdapter<String>(getApplicationContext(),
                                        android.R.layout.simple_list_item_multiple_choice, servicesLits);
                                servicesListview.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);
                                serviceAdapter.setDropDownViewResource(android.R.layout.simple_list_item_multiple_choice);
                                servicesListview.setAdapter(serviceAdapter);
                            } else {
                                UImsgs.showToast(mContext, "Sorry...! No Services Found");
                            }


                        } else {
                            UImsgs.showToast(mContext, messagae);
                            //Toast.makeText(ServicesAminitiesActivity.this, "Invalid Credentials", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        UImsgs.showToast(mContext, "Sorry..! No data found for the selected category");
                        fieldsLayout.setVisibility(View.VISIBLE);
                        servicelayout.setVisibility(View.GONE);
                    }


                    /*ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                            android.R.fieldsLayout.simple_spinner_item, states);
                    adapter.setDropDownViewResource(android.R.fieldsLayout.simple_spinner_dropdown_item);
                    state_spinner.setAdapter(adapter);*/


                } catch (JSONException e) {
                    e.printStackTrace();
                    //UIMsgs.showToast(mContext, "No data found...!");
                    progressDialog.dismiss();
                }
              /*  ServicesAminitiesAdapter servicesAminitiesAdapter = new ServicesAminitiesAdapter(mContext,serviceAminitesList);
                amenitiesRecycle.setAdapter(servicesAminitiesAdapter);*/
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(RegisterVendorActivity.this, error + "", Toast.LENGTH_SHORT).show();

            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                //map.put("X_AUTH_TOKEN", token);

                return map;
            }
        };

        /*stringRequest.setRetryPolicy(new DefaultRetryPolicy(8000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));*/
        requestQueue.add(stringRequest);


    }


    public void dataFetcherFromFields() {

        newListName_str = newListName.getText().toString().trim();
        location_et_str = location_et.getText().toString().trim();
        pincode_str = pincode.getText().toString().trim();
        complete_address_str = complete_address.getText().toString().trim();
        landmark_str = landmark.getText().toString().trim();
        std_code_str = std_code.getText().toString().trim();
        landline_str = landline.getText().toString().trim();
        mobile_str = mobile.getText().toString().trim();
        helpline_str = helpline.getText().toString().trim();
        email_str = email.getText().toString().trim();
        password_str = business_pwd.getText().toString().trim();
        re_password_str = re_business_pwd.getText().toString().trim();
        whatsapp_str = whatsapp.getText().toString().trim();

        /*constId= constituenciesId.get(constituency_spinner.getSelectedItemPosition()-1);
        catId= categoryId.get(categories_spinner.getSelectedItemPosition()-1);*/
        newListName.setFilters(new InputFilter[]{filter});
       /* newListName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(final CharSequence s, int start, int before, int count) {
                //Char at newly inserted pos.
                char currentChar = s.charAt(start + before);

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });*/
       // newListName.setFilters(new InputFilter[] { filter });
        if (constituencies.size() > 0) {
            if (!constituency_spinner.getSelectedItem().toString().equalsIgnoreCase("select"))
                constId = constituenciesId.get(constituency_spinner.getSelectedItemPosition() - 1);
            else
                constId = null;
        } else
            constId = null;
       /* if (categories.size() > 0) {
            if (!categories_spinner.getSelectedItem().toString().equalsIgnoreCase("select"))
                catId = categoryId.get(categories_spinner.getSelectedItemPosition() - 1);
            else
                catId = null;
        } else
            catId = null;*/



    }

    public class GenericTextWatcher implements TextWatcher, View.OnKeyListener {
        private View view;
        String previousText = "";

        private GenericTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void afterTextChanged(Editable editable) {
            // TODO Auto-generated method stub
            String text = editable.toString();
            switch (view.getId()) {
                case R.id.editTextone:
                    if (text.length() == 1) {
                        if (previousText.length() > 0) {
                            editTextone.removeTextChangedListener(watcher1);
                            editTextone.setText(previousText);
                            editTextone.addTextChangedListener(watcher1);

                            editTexttwo.removeTextChangedListener(watcher2);
                            editTexttwo.setText(text);
                            editTexttwo.addTextChangedListener(watcher2);
                        }
                        editTexttwo.requestFocus();
                    }
                    break;
                case R.id.editTexttwo:
                    if (text.length() == 1) {
                        if (previousText.length() > 0) {
                            editTexttwo.removeTextChangedListener(watcher2);
                            editTexttwo.setText(previousText);
                            editTexttwo.addTextChangedListener(watcher2);

                            editTextthree.removeTextChangedListener(watcher3);
                            editTextthree.setText(text);
                            editTextthree.addTextChangedListener(watcher3);

                        }
                        editTextthree.requestFocus();

                    } else if (text.length() == 0)
                        editTextone.requestFocus();
                    break;
                case R.id.editTextthree:
                    if (text.length() == 1) {
                        if (previousText.length() > 0) {
                            editTextthree.removeTextChangedListener(watcher3);
                            editTextthree.setText(previousText);
                            editTextthree.addTextChangedListener(watcher3);

                            editTextfour.removeTextChangedListener(watcher4);
                            editTextfour.setText(text);
                            editTextfour.addTextChangedListener(watcher4);
                        }
                        editTextfour.requestFocus();
                    } else if (text.length() == 0)
                        editTexttwo.requestFocus();
                    break;
                case R.id.editTextfour:
                    if (text.length() == 1) {
                        if (previousText.length() > 0) {
                            editTextfour.removeTextChangedListener(watcher4);
                            editTextfour.setText(previousText);
                            editTextfour.addTextChangedListener(watcher4);

                            editTextFive.removeTextChangedListener(watcher5);
                            editTextFive.setText(text);
                            editTextFive.addTextChangedListener(watcher5);
                        }
                        editTextFive.requestFocus();
                    } else if (text.length() == 0)
                        editTextthree.requestFocus();
                    break;
                case R.id.editTextFive:
                    if (text.length() == 1) {
                        if (previousText.length() > 0) {
                            editTextFive.removeTextChangedListener(watcher5);
                            editTextFive.setText(previousText);
                            editTextFive.addTextChangedListener(watcher5);

                            editTextSix.removeTextChangedListener(watcher6);
                            editTextSix.setText(text);
                            editTextSix.addTextChangedListener(watcher6);
                        }
                        editTextSix.requestFocus();
                    } else if (text.length() == 0)
                        editTextfour.requestFocus();
                    break;
                case R.id.editTextSix:
                    if (text.length() == 0) {

                        editTextFive.requestFocus();
                    } else {
                        try {
                            final InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                            Log.e(TAG, "afterTextChanged: hide keyboard");
                        } catch (Exception e) {
                            Log.e(TAG, "afterTextChanged: " + e.toString());
                        }
                    }


                    break;
            }
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
            Log.d(TAG, "beforeTextChanged: " + arg0);
            if (arg0.length() > 0) {
                previousText = arg0.toString();
            }
        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
        }

        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            previousText = "";
            Log.d(TAG, "onKey: keyCode = " + keyCode + ", event = " + event.toString());
            if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KEYCODE_DEL) {
                switch (view.getId()) {
                    case R.id.editTexttwo:
                        if (editTexttwo.getText().toString().trim().length() == 0)
                            editTextone.requestFocus();
                        break;
                    case R.id.editTextthree:
                        if (editTextthree.getText().toString().trim().length() == 0)
                            editTexttwo.requestFocus();
                        break;
                    case R.id.editTextfour:
                        if (editTextfour.getText().toString().trim().length() == 0)
                            editTextthree.requestFocus();
                        break;
                    case R.id.editTextFive:
                        if (editTextFive.getText().toString().trim().length() == 0)
                            editTextfour.requestFocus();
                        break;
                    case R.id.editTextSix:
                        if (editTextSix.getText().toString().trim().length() == 0)
                            editTextFive.requestFocus();
                        else if (editTextSix.getText().toString().trim().length() == 1)
                            try {

                                //  ((BaseActivity) getActivity()).hideSoftKeyboard();
                            } catch (Exception e) {
                                Log.e(TAG, "afterTextChanged: " + e.toString());
                            }
                        break;
                }

            }
            return false;
        }
    }
    GenericTextWatcher watcher1,watcher2,watcher3,watcher4,watcher5,watcher6;
    EditText editTextone,editTexttwo,editTextthree, editTextfour,editTextFive,editTextSix;
    private void showVerifyOTPDialog(String otp, final String mobilenumber) {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.verify_otp_dialog);

        LinearLayout layout_root= (LinearLayout) dialog.findViewById(R.id.layout_root);
        DisplayMetrics displayMetrics = mContext.getResources().getDisplayMetrics();
        ViewGroup.LayoutParams layoutParams = layout_root.getLayoutParams();
        layoutParams.width = displayMetrics.widthPixels-20;
        layout_root.setLayoutParams(layoutParams);

        Button applyButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        ImageView closeButton = (ImageView) dialog.findViewById(R.id.closeButton);
         editTextone = (EditText) dialog.findViewById(R.id.editTextone);
        editTexttwo = (EditText) dialog.findViewById(R.id.editTexttwo);
         editTextthree = (EditText) dialog.findViewById(R.id.editTextthree);
        editTextfour = (EditText) dialog.findViewById(R.id.editTextfour);
        editTextFive = (EditText) dialog.findViewById(R.id.editTextFive);
        editTextSix = (EditText) dialog.findViewById(R.id.editTextSix);
        TextView tv_error = (TextView) dialog.findViewById(R.id.tv_error);
        final TextView tv_timer_resend = (TextView) dialog.findViewById(R.id.tv_timer_resend);
        new CountDownTimer(30000, 1000) {

            public void onTick(long millisUntilFinished) {
                tv_timer_resend.setText("Resends remaining: " + millisUntilFinished / 1000);
                //here you can have your logic to set text to edittext
            }

            public void onFinish() {
                tv_timer_resend.setText("Resend Otp");
            }

        }.start();
        tv_timer_resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendOtp(mobilenumber);
            }
        });
        if(otp!=null&& !otp.isEmpty() && otp.length() >= 6)
        {
            tv_error.setVisibility(View.VISIBLE);
            editTextone.setText(otp.substring(0, 1));
            editTexttwo.setText(otp.substring(1, 2));
            editTextthree.setText(otp.substring(2, 3));
            editTextfour.setText(otp.substring(3, 4));
            editTextFive.setText(otp.substring(4, 5));
            editTextSix.setText(otp.substring(5, 6));
        }


         watcher1 = new GenericTextWatcher(editTextone);
         watcher2 = new GenericTextWatcher(editTexttwo);
         watcher3 = new GenericTextWatcher(editTextthree);
         watcher4 = new GenericTextWatcher(editTextfour);
         watcher5 = new GenericTextWatcher(editTextFive);
         watcher6 = new GenericTextWatcher(editTextSix);
        editTextone.addTextChangedListener(watcher1);
        editTextone.setOnKeyListener(watcher1);
        editTexttwo.addTextChangedListener(watcher2);
        editTexttwo.setOnKeyListener(watcher2);
        editTextthree.addTextChangedListener(watcher3);
        editTextthree.setOnKeyListener(watcher3);
        editTextfour.addTextChangedListener(watcher4);
        editTextfour.setOnKeyListener(watcher4);
        editTextFive.addTextChangedListener(watcher5);
        editTextFive.setOnKeyListener(watcher5);
        editTextSix.addTextChangedListener(watcher6);
        editTextSix.setOnKeyListener(watcher6);




        applyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String otp = editTextone.getText().toString() + editTexttwo.getText().toString() +
                        editTextthree.getText().toString() + editTextfour.getText().toString() +
                        editTextFive.getText().toString() + editTextSix.getText().toString();

                if (otp != null && !otp.isEmpty() && otp.length() >= 6) {
                    dialog.dismiss();
                    callVerifyOTPApi(otp,""+mobilenumber);
                }
            }
        });
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }
    private void sendOtp(final String mobile) {
        LoadingDialog.loadDialog(mContext);
        Map<String, String> fcmMap = new HashMap<>();
        fcmMap.put("mobile", mobile);
        fcmMap.put("passowrd", password_str);
        JSONObject json = new JSONObject(fcmMap);
        System.out.println("aaaaaaa request "+json.toString());
        final String data = json.toString();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Config.SEND_OTP, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (response != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        System.out.println("aaaaaaa response "+jsonObject.toString());
                        boolean status = jsonObject.getBoolean("status");
                        if (status) {
                            showVerifyOTPDialog(null,mobile);
                        } else {
                            UImsgs.showToast(mContext, Html.fromHtml(jsonObject.getString("message")).toString());
                        }
                    } catch (Exception e) {
                        UImsgs.showToast(mContext, "Something went wrong");
                        e.printStackTrace();
                    } finally {
                        LoadingDialog.dialog.dismiss();
                    }
                } else {
                    LoadingDialog.dialog.dismiss();
                    UImsgs.showToast(mContext, "Server under maintenance");
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                LoadingDialog.dialog.dismiss();
                System.out.println("aaaaaaa error  "+error.getMessage());
                UImsgs.showToast(mContext, "Something went wrong");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                //  map.put(AUTH_TOKEN, preferenceManager.getString(TOKEN_KEY));
                map.put(APP_ID, "TWc9PQ==");
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    private void callVerifyOTPApi(final String otp, final String mobilenumer) {

        LoadingDialog.loadDialog(mContext);

        Map<String, String> fcmMap = new HashMap<>();
        fcmMap.put("mobile", mobilenumer);
        fcmMap.put("otp", otp);
        JSONObject json = new JSONObject(fcmMap);
        final String data = json.toString();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Config.VERIFY_OTP, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.d("VolleyResponse", "VERIFY_OTP: respnse " + response);

                    JSONObject jsonObject = new JSONObject(response);
                    boolean status = jsonObject.getBoolean("status");
                    if (!status) {
                        //  Toast.makeText(getApplicationContext(), "Your order has been failed with " + response, Toast.LENGTH_SHORT).show();
                        //  showErrorDialog("Incorrect otp");
                        //or
                        //show alert on same dialog

                        UImsgs.showToast(mContext, Html.fromHtml(jsonObject.getString("message")).toString());

                        //show alert on same dialog
                        showVerifyOTPDialog(otp,mobilenumer);
                    } else {
                        isOtpVerified=true;
                        verifiedmobile=mobilenumer;
                        dataSender();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    LoadingDialog.dialog.dismiss();
                } finally {
                    LoadingDialog.dialog.dismiss();
                }
                //Toast.makeText(mContext, "responce "+response, Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoadingDialog.dialog.dismiss();
                Log.d("fetch user error", error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(APP_ID, "TWc9PQ==");
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    public void dataSender() {

        progressDialog = new ProgressDialog(RegisterVendorActivity.this);
        progressDialog.setIcon(R.drawable.nextclick_logo_black);
        progressDialog.setMessage("Please wait while uploading the data.....");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setProgress(0);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
       /* Map<String, String> socialdata = new HashMap<>();
        socialdata.put("1", facebook_link_str);
        socialdata.put("2", twitter_link_str);
        socialdata.put("3", instagram_link_str);
        socialdata.put("4", website_url_str);

        Map<String, Map<String, String>> contact = new HashMap<>();
        Map<String, String> two = new HashMap<>();
        two.put("number", landline_str);
        two.put("code", std_code_str);
        Map<String, String> three = new HashMap<>();
        three.put("number", whatsapp_str);
        three.put("code", "+91");
        Map<String, String> four = new HashMap<>();
        four.put("number", helpline_str);
        four.put("code", "");
        contact.put("2", two);
        contact.put("3", three);
        contact.put("4", four);

        Map<String, String> holiday = new HashMap<>();
        if (holidayList.size() > 0) {
            for (int i = 0; i < holidayList.size(); i++) {
                holiday.put(i + "".trim(), holidayList.get(i));
            }
        }

        Map<String, Object> timingsMap = new HashMap<>();

        for (int i = 0; i < openingTimeList.size(); i++) {

            Map<String, Object> insideTimings = new HashMap<>();
            insideTimings.put("start_time", openingTimeList.get(i));
            insideTimings.put("end_time", closingTimeList.get(i));

            timingsMap.put(i + "".trim(), insideTimings);

        }*/


        Map<String, Object> mainData = new HashMap<>();

      //  mainData.put("ref_id", referal_id.getText().toString().trim());
        mainData.put("name", newListName_str);//businessname
        mainData.put("category_id", catId);//business cat
        mainData.put("location_address", location_et_str);
        mainData.put("latitude", String.valueOf(lattitude));
        mainData.put("longitude", String.valueOf(longitude));
        mainData.put("address", complete_address_str);
        mainData.put("constituency_id", constId);
        mainData.put("pincode",pincode.getText().toString());
        mainData.put("email", email_str);
        mainData.put("primary_number", mobile_str);
        mainData.put("password", password_str);
        mainData.put("cover", coverPhotStringImageUrl1);

            try {
                if (bannerBase64 != null && bannerBase64.size() > 0) {
                    mainData.put("banner", bannerBase64);//bannerBase64.get("0")
                    /*
                    Map<String, Object> imageMap = new HashMap<>();

                    for (int i = 0; i < bannerBase64.size(); i++) {
                      //  mainData.put("banner", bannerBase64.get(i));
                        imageMap.put(i + "".trim(), bannerBase64.get(i));
                    }
                    mainData.put("banner", imageMap);*/
                }
            }
            catch (Exception ex){}

        //new fields
        Map<String, Map<String, String>> contact = new HashMap<>();
        Map<String, String> two = new HashMap<>();
       // two.put("number", landline_str);
        //two.put("code", std_code_str);
        Map<String, String> three = new HashMap<>();
        three.put("number", whatsapp_str);
        three.put("code", "+91");
        Map<String, String> four = new HashMap<>();
        four.put("number", altMobile.getText().toString());
        four.put("code",country_code1.getText().toString());

        Map<String, String> one = new HashMap<>();
        one.put("number", mobile_str);
        one.put("code", country_code.getText().toString());

        contact.put("1", one);
        contact.put("2", two);
        contact.put("3", three);
        contact.put("4", four);

        mainData.put("contacts", contact);
        StringBuilder str = new StringBuilder("");
        Map<String, String> sub_cats = new HashMap<>();
        int index=0;
        for (String eachstring : selectedCategoryIDs) {
            //str.append(eachstring).append(",");
            sub_cats.put(""+index,eachstring);
            index++;
        }

        mainData.put("sub_category_id",sub_cats);//sub catid
        mainData.put("owner_name",tv_owner_name.getText().toString());//optional
        mainData.put("gst_number",tv_gst_no.getText().toString());
        mainData.put("labour_certificate_number",tv_labour_certificate_no.getText().toString());
        mainData.put("fssai_number",tv_fssai_no.getText().toString());


        //JSONObject json = new JSONObject(mainData);
        //submission(json, progressDialog);
        ObjectMapper mapperObj = new ObjectMapper();
        try {
            String jsonString = mapperObj.writeValueAsString(mainData);
            JSONObject json = new JSONObject(mainData);
          //  System.out.println("aaaaaaaa  json  "+json.toString());
            if (NetworkUtil.isNetworkConnected(RegisterVendorActivity.this)) {
                submission(jsonString, progressDialog);
            } else {
                Toast.makeText(RegisterVendorActivity.this, ""+getResources().getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
            }

        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(mContext, "Please select less size images", Toast.LENGTH_SHORT).show();
        }
    }

    public void dataClearance() {


        selectedSubCategoryList.clear();
        selectedAmenitiesList.clear();
        selectedServicesList.clear();


        coverPhotStringImageUrl1 = "".trim();
        banner_photo.setImageBitmap(null);
        cover_photo.setImageBitmap(null);
        bannerBase64.clear();
        servicesMap.clear();
        amenitiesMap.clear();
        subCategorymap.clear();
        subCategorydatamap.clear();
        servicesdatamap.clear();
        amenitiesdatamap.clear();
        amenitiesList.clear();
        servicesLits.clear();
        subcategoriesList.clear();

        servicesids.clear();
        selectedservicesid.clear();

        amenitiesids.clear();
        selectedamenitiesids.clear();

        subcategoryids.clear();
        selectedsubcategoryids.clear();

    }


    public void mailValidator(final ProgressDialog progressDialog) {



        /*RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, EMAIL_VERIFICATION,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {

                            try {

                                JSONObject object = new JSONObject(response);
                                String status = object.getString("status");
                                if (status.equalsIgnoreCase("true")) {

                                    Toast.makeText(CommonFiledsActivity.this, "OTP has been sent to your mail", Toast.LENGTH_SHORT).show();
                                    //clearCache();

                                    LayoutInflater inflater = getLayoutInflater();
                                    View alertLayout = inflater.inflate(R.layout.otp_layout, null);

                                    otp_E_text =alertLayout.findViewById(R.id.otp_next);

                                    AlertDialog.Builder alert = new AlertDialog.Builder(CommonFiledsActivity.this);
                                    alert.setIcon(R.mipmap.ic_launcher);
                                    alert.setTitle("OTP");
                                    // this is set the view from XML inside AlertDialog
                                    alert.setView(alertLayout);
                                    alert.setCancelable(false);


                                    alert.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {

                                            String otp_verify = otp_E_text.getText().toString().trim();
                                            if(otp_verify.length()==4){
                                                if(otp_str.equalsIgnoreCase(otp_verify)){
                                                    dataSender();

                                                }
                                                else{
                                                    Toast.makeText(mContext, "Invalid otp Please press submit button again", Toast.LENGTH_LONG).show();
                                                }
                                            }


                                        }
                                    });

                                    AlertDialog dialog = alert.create();
                                    dialog.show();




                                } else {
                                    progressDialog.dismiss();
                                    String message = object.getString("message");
                                    Toast.makeText(CommonFiledsActivity.this, Html.fromHtml(message), Toast.LENGTH_SHORT).show();

                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                progressDialog.dismiss();
                                Toast.makeText(CommonFiledsActivity.this, e + "", Toast.LENGTH_SHORT).show();

                            }
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(CommonFiledsActivity.this, *//*error +*//* "Please check the internet connection", Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", email_str);
                params.put("sub", SUBJECT);
                params.put("mes", "HI SIR/MA'AM your asset : " +newListName_str.toUpperCase()+"is going to be connected with NEXTCLICK.\n Your OTP is : "+otp_str+" \n Please provide this otp to our executive to complete the REGISTRATION");

                return params;
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);*/
        progressDialog.dismiss();
       // dataSender();
        if (NetworkUtil.isNetworkConnected(RegisterVendorActivity.this)) {
            sendOtp(mobile_str);
        } else {
            Toast.makeText(RegisterVendorActivity.this, ""+getResources().getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
        }

    }

    public void submission(final String json, final ProgressDialog progressDialog) {

        final String data = json;
        Log.v("requesr register ",data);
        System.out.println("aaaaaaaaaaa  request regeter  "+json.toString());

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, VendorCreation,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        try {

                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("aaaaaaaaaa  response register "+jsonObject.toString());
                            Boolean status = jsonObject.getBoolean("status");
                            String http = jsonObject.getString("http_code");
                            String message = jsonObject.getString("message");
                           // String data = jsonObject.getString("data");
                            System.out.println("aaaaaaa responce "+jsonObject);
                            if (status ) {

                                //signOut();//need to be check
                                /*Intent intent = new Intent(CommonFiledsActivity.this, FinishActivity.class);
                                startActivity(intent);
                                finish();*/
                               // UIMsgs.showAlert(mContext,);
                               // showAlert(mContext,""+message);
                            //    savetermsandconditions(progressDialog,message);
                                 UImsgs.showToast(mContext, "Successfully Submitted");
                              //  onBackPressed();
                            //    dataClearance();
                                finish();
                            } else {
                                UImsgs.showToast(mContext, message
                                );
                                Log.d("er msg", Html.fromHtml(message).toString());
                                progressDialog.dismiss();
                                servicelayout.setVisibility(View.GONE);
                                fieldsLayout.setVisibility(View.VISIBLE);
                            }

                        } catch (Exception e) {
                            System.out.println("aaaaaaaa  catchh "+e.getMessage());
                            UImsgs.showToast(mContext,e.getMessage());
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("aaaaaaaa  catchh "+error.getMessage());
                //Toast.makeText(LoginActivity.this, error+"", Toast.LENGTH_SHORT).show();
                UImsgs.showToast(mContext, "Some Error Occured.. Please provide data again...");
                progressDialog.dismiss();
                servicelayout.setVisibility(View.GONE);
                fieldsLayout.setVisibility(View.VISIBLE);
            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put("X_AUTH_TOKEN", preferenceManager.getString(USER_TOKEN));
                System.out.println("aaaaaa token "+ preferenceManager.getString(USER_TOKEN));
                return map;
            }

            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    public boolean validator()
    {
        boolean validity = true;

        String[] emailafterdot=email_str.split(".");


        if (newListName_str.length() == 0) {
            newListName.setError(EMPTY_NOT_ALLOWED);
            newListName.requestFocus();
            validity = false;
        }
        else if (catId == null) {
            showToast(mContext, "Please Select Business Categories");
            validity = false;
        }
        else if (selectedCategoryIDs == null || selectedCategoryIDs.size() == 0) {
            showToast(mContext, "Please Select Business Categories");
            validity = false;
        }
        else if(tv_owner_name.getText().toString()==null || tv_owner_name.getText().toString().isEmpty())
        {
            tv_owner_name.setError(EMPTY_NOT_ALLOWED);
            tv_owner_name.requestFocus();
            validity = false;
        }
        /*else if(tv_gst_no.getText().toString()==null || tv_gst_no.getText().toString().isEmpty())
        {
            tv_gst_no.setError(EMPTY_NOT_ALLOWED);
            tv_gst_no.requestFocus();
            validity = false;
        }*/
        else if(!Validations.IsEmpty(tv_gst_no.getText()) && Validations.isValidGST(tv_gst_no.getText().toString()))
        {
            tv_gst_no.setError(INVALID);
            tv_gst_no.requestFocus();
            validity = false;
        }
        /*
        else if(tv_labour_certificate_no.getText().toString()==null || tv_labour_certificate_no.getText().toString().isEmpty())
        {
            tv_labour_certificate_no.setError(EMPTY_NOT_ALLOWED);
            tv_labour_certificate_no.requestFocus();
            validity = false;
        }*/
        else if(!Validations.IsEmpty(tv_labour_certificate_no.getText()) && Validations.isValidLabourCertificationNumber(tv_labour_certificate_no.getText().toString()))
        {
            tv_labour_certificate_no.setError(INVALID);
            tv_labour_certificate_no.requestFocus();
            validity = false;
        }
        /*else if(tv_fssai_no.getText().toString()==null || tv_fssai_no.getText().toString().isEmpty())
        {
            tv_fssai_no.setError(EMPTY_NOT_ALLOWED);
            tv_fssai_no.requestFocus();
            validity = false;
        }*/
        else if(!Validations.IsEmpty(tv_fssai_no.getText()) && Validations.isValidFSSAI(tv_fssai_no.getText().toString()))
        {
            tv_fssai_no.setError(INVALID);
            tv_fssai_no.requestFocus();
            validity = false;
        }
        else if (location_et_str.length() == 0) {
            location_et.setError("Wait untill location fetched");
            location_et.requestFocus();
            validity = false;
        }
        else if (complete_address_str.length() == 0) {
            complete_address.setError(EMPTY_NOT_ALLOWED);
            complete_address.requestFocus();
            validity = false;
        }
        else if (complete_address_str.length() < 6) {
            complete_address.setError("Please provide complete details");
            complete_address.requestFocus();
            validity = false;
        }
        else if (constId == null || constId.length() == 0) {
            showToast(mContext, "Please Select Constituency");
            constituency_spinner.requestFocus();
            validity = false;
        }
        else if (pincode_str.length() == 0) {
            pincode.setError(EMPTY);
            pincode.requestFocus();
            validity = false;
        }
        else if (pincode_str.startsWith("0") ||  pincode_str.length() < 6) {
            pincode.setError("PIN CODE is  Invalid");
            pincode.requestFocus();
            validity = false;
        }
        else if (validations.isBlank(email_str)) {
            email.setError(EMPTY);
            email.requestFocus();
            validity = false;
        }
        else if (validations.isValidEmail(email_str)) {
            email.setError(INVALID);
            email.requestFocus();
            validity = false;
        }
        else if (mobile_str.length() == 0) {
            mobile.setError(EMPTY);
            mobile.requestFocus();
            validity = false;
        }
        else if (mobile_str.length() < 10) {
            mobile.setError(INVALID_MOBILE);
            mobile.requestFocus();
            validity = false;
        }
        else if (mobile_str.startsWith("0") || (mobile_str.startsWith("1")) || mobile_str.startsWith("2")||
                mobile_str.startsWith("3")|| mobile_str.startsWith("4")||mobile_str.startsWith("5")){
            mobile.setError(INVALID_MOBILE);
            mobile.requestFocus();
            validity = false;
        }

        else  if (password_str.length() == 0) {
            business_pwd.setError(EMPTY);
            business_pwd.requestFocus();
            validity = false;
        }
        else if (password_str.length() < 4) {
            business_pwd.setError("Minimum 4 Characters");
            business_pwd.requestFocus();
            validity = false;
        } else  if (re_password_str.length() == 0) {
            re_business_pwd.setError(EMPTY);
            re_business_pwd.requestFocus();
            validity = false;
        }
        else if (re_password_str.length() < 4) {
            re_business_pwd.setError("Minimum 4 Characters");
            re_business_pwd.requestFocus();
            validity = false;
        }else if (!password_str.equalsIgnoreCase(re_password_str)){
            re_business_pwd.setError("Password Not Match");
            re_business_pwd.requestFocus();
            validity = false;
        }




        else if (coverPhotStringImageUrl1 == null || coverPhotStringImageUrl1.length() <= 0) {
            showToast(mContext, "Please Select Cover Image");
            cover_photo.requestFocus();
            validity = false;
        }
        else if (bannerBase64 == null || bannerBase64.size() <= 0) {//2
            showToast(mContext, "Please Select Banner Image");
            banner_photo.requestFocus();
            validity = false;
        }


        /* constId, catId,  */


        return validity;
    }

    private void showToast(Context mContext, String message) {
        Toast.makeText(mContext, ""+message, Toast.LENGTH_SHORT).show();

    }

    private Bitmap mergeMultiple(ArrayList<Bitmap> parts) {
        final int IMAGE_MAX_SIZE = 1200000;
        int value = parts.size();
        int height = parts.get(0).getHeight();
        int width = parts.get(0).getWidth();

        double y = Math.sqrt(IMAGE_MAX_SIZE
                / (((double) width) / height));
        double x = (y / height) * width;
        Bitmap result = Bitmap.createScaledBitmap(parts.get(0), (int) x, (int) y, true);
        Canvas canvas = new Canvas(result);
        Paint paint = new Paint();

        for (int i = 0; i < parts.size(); i++) {
            canvas.drawBitmap(parts.get(i), parts.get(i).getWidth() * (i % 4), parts.get(i).getHeight() * (i / 4), paint);
        }
        return result;
    }

    public void init() {

        otp_str = generateRandomNumber();
        newListName = (EditText) findViewById(R.id.new_list_name);
        location_et =findViewById(R.id.location);
        RelativeLayout location_layout= (RelativeLayout) findViewById(R.id.location_layout);
        ImageView refresh_loc= findViewById(R.id.refresh_loc);
        refresh_loc.setOnClickListener(new View.OnClickListener() {
                                           @Override
                                           public void onClick(View v) {


                                               getLocation();
                                           }
                                       });

                location_et.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        getLocation();
                        /*
                Gson gson=new Gson();
                ArrayList<String> locArray=new ArrayList<>();
                locArray.add(currentAddress);
                locArray.add(""+lattitude);
                locArray.add(""+longitude);
                String locationString = gson.toJson(locArray);
                Intent intent=new Intent(getApplicationContext(), FindAddressInMap.class);
                intent.putExtra("location",locationString);
                startActivityForResult(intent,UPDATE_LOCATION);*/
                    }
                });
        pincode = (EditText) findViewById(R.id.pincode);
        complete_address = (EditText) findViewById(R.id.complet_address);
        landmark = (EditText) findViewById(R.id.landmark);
        std_code = (EditText) findViewById(R.id.std_code);
        country_code = (EditText) findViewById(R.id.country_code);
        country_code1 = (EditText) findViewById(R.id.country_code1);
        landline = (EditText) findViewById(R.id.landline);
        mobile = (EditText) findViewById(R.id.mobile);
        altMobile = (EditText) findViewById(R.id.mobile1);
        helpline = (EditText) findViewById(R.id.helpLine);
        email = (EditText) findViewById(R.id.email);
        whatsapp = (EditText) findViewById(R.id.whatsapp);
        check_sameasabove = (CheckBox) findViewById(R.id.check_sameasabove);

        verify_mobile = (EditText) findViewById(R.id.verifymobile);
        //otp = (EditText) findViewById(R.id.otp);

        taptoopenmap = (TextView) findViewById(R.id.map_opener);

        tv_sub_categories=findViewById(R.id.tv_sub_categories);
        fieldsLayout = (LinearLayout) findViewById(R.id.fieldsLayout);
        servicelayout = (LinearLayout) findViewById(R.id.service_n_aminities_layout);
        scroll = (ScrollView) findViewById(R.id.common_fields_scroll);

        //net = (Button) findViewById(R.id.next);
        submit = (Button) findViewById(R.id.submit);

        terms = (CheckBox) findViewById(R.id.terms);

        back = (ImageView) findViewById(R.id.back);
        cover_photo = (ImageView) findViewById(R.id.cover_photo);
        add_cover_photo = (ImageView) findViewById(R.id.add_cover_photo);
        banner_photo = (ImageView) findViewById(R.id.banner_photo);

        add_banner_photo = (ImageView) findViewById(R.id.add_banner_photo);


        state_spinner = (Spinner) findViewById(R.id.state_spinner);
        district_spinner = (Spinner) findViewById(R.id.district_spinner);
        constituency_spinner = (Spinner) findViewById(R.id.constituency_spinner);
        categories_spinner = (Spinner) findViewById(R.id.category_spinner);
        //PartSpinner=(Spinner)findViewById(R.id.spinnerPart);


        referal_id = (EditText) findViewById(R.id.referal_id);
        business_pwd = (TextInputEditText) findViewById(R.id.business_pwd);
        re_business_pwd = (TextInputEditText) findViewById(R.id.re_business_pwd);


        states.add("Select");
        categories.add("Select");

        layout_category=findViewById(R.id.layout_category);

        servicesListview = (ListView) findViewById(R.id.services_recycle);
        aminitiesListView = (ListView) findViewById(R.id.amenities_recycle);
        subCategoryListView = findViewById(R.id.subCategory);
        subCategoryListView.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);
        selectedSubcategoryListView = (ListView) findViewById(R.id.selected_subcategory);

        newListName.setFilters(new InputFilter[] { filter });


        tv_gst_no=findViewById(R.id.tv_gst_no);
        tv_labour_certificate_no=findViewById(R.id.tv_labour_certificate_no);
        tv_fssai_no=findViewById(R.id.tv_fssai_no);
        tv_owner_name=findViewById(R.id.tv_owner_name);
        pincode=findViewById(R.id.pincode);

        check_sameasabove.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    whatsapp.setText(mobile.getText().toString().trim());
                }else {
                    whatsapp.setText("");
                }
            }
        });
    }

    public String generateRandomNumber() {
        int randomNumber;

        SecureRandom secureRandom = new SecureRandom();
        String s = "";
        for (int i = 0; i < length; i++) {
            int number = secureRandom.nextInt(range);
            if (number == 0 && i == 0) { // to prevent the Zero to be the first number as then it will reduce the length of generated pin to three or even more if the second or third number came as zeros
                i = -1;
                continue;
            }
            s = s + number;
        }

        //randomNumber = Integer.parseInt(s);

        return s;
    }


    public void sendSMS(String phoneNo, String msg) {
        try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNo, null, msg, null, null);
            Toast.makeText(getApplicationContext(), "Message Sent",
                    Toast.LENGTH_LONG).show();
        } catch (Exception ex) {
            Toast.makeText(getApplicationContext(), ex.getMessage().toString(),
                    Toast.LENGTH_LONG).show();
            ex.printStackTrace();
        }
    }

    private boolean validatePhoneNumber() {
        String phoneNumber = mPhoneNumberField.getText().toString();
        if (TextUtils.isEmpty(phoneNumber)) {
            mPhoneNumberField.setError("Invalid phone number.");
            return false;
        }

        return true;
    }

    private void enableViews(View... views) {
        for (View v : views) {
            v.setEnabled(true);
        }
    }

    private void showCategoryPopup() {
        //categories

        if(ListCategoryObject!=null && ListCategoryObject.size()>1)
        {
            final ArrayList<String> selectedCategories=new ArrayList<>();
            final Dialog dialog = new Dialog(this);

            View contentView = View.inflate(mContext, R.layout.select_categories_new, null);//R.layout.select_categories
            //context = contentView.getContext();
            dialog.setContentView(contentView);

            LinearLayout layout_root= (LinearLayout) dialog.findViewById(R.id.layout_root);
            DisplayMetrics displayMetrics = mContext.getResources().getDisplayMetrics();
            ViewGroup.LayoutParams layoutParams = layout_root.getLayoutParams();
            layoutParams.width = displayMetrics.widthPixels;
            layoutParams.height = displayMetrics.heightPixels;
            layout_root.setLayoutParams(layoutParams);


            Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
            ImageView closeButton = (ImageView) dialog.findViewById(R.id.closeButton);

            RecyclerView recyclerView_categories=dialog.findViewById(R.id.recyclerView_categories);
            RecyclerView recyclerView_subcategories=dialog.findViewById(R.id.recyclerView_subcategories);

            TextView tv_no_categories=dialog.findViewById(R.id.tv_no_categories);
            TextView tv_no_sub_categories=dialog.findViewById(R.id.tv_no_sub_categories);
            final TextView tv_error=dialog.findViewById(R.id.tv_error);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            recyclerView_categories.setLayoutManager(linearLayoutManager);

            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView_categories.getContext(),
                    linearLayoutManager.getOrientation());
            recyclerView_categories.addItemDecoration(dividerItemDecoration);

            LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(mContext);
            linearLayoutManager1.setOrientation(LinearLayoutManager.VERTICAL);
            recyclerView_subcategories.setLayoutManager(linearLayoutManager1);


            CategoryAdapter categoryAdapter = new CategoryAdapter(mContext, ListCategoryObject,
                    recyclerView_subcategories,tv_no_categories,tv_no_sub_categories,selectedCategoryIDs,selectedCategories,
                    this);
            recyclerView_categories.setAdapter(categoryAdapter);

            closeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            dialogButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (selectedCategoryIDs.size() > 0) {
                        dialog.dismiss();
                        setSelectedCategoryIDs(selectedCategoryIDs,selectedCategories);
                    }
                    else {
                        tv_error.setVisibility(View.VISIBLE);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                tv_error.setVisibility(View.GONE);
                            }
                        },2000);
                    }
                }
            });

            Window window = dialog.getWindow();
            window.setBackgroundDrawableResource(android.R.color.transparent);
            window.setGravity(Gravity.BOTTOM);
             dialog.show();
        }
    }


    public void updateLocation(Intent intent)
    {
        ArrayList<String> locArray=new ArrayList<>();
        try {
            Gson gson = new Gson();
            String carListAsString = intent.getStringExtra("location");
            Type type = new TypeToken<ArrayList<String>>() {
            }.getType();
            locArray = gson.fromJson(carListAsString, type);
            if(locArray!=null && locArray.size()>=3)
            {
                location_et.setText(locArray.get(0));
                lattitude = Double.parseDouble(locArray.get(1));
                longitude = Double.parseDouble(locArray.get(2));
            }
        }catch (Exception ex) {
            getLocation();
        }
    }
    private ArrayList findUnAskedPermissions(ArrayList wanted) {
        ArrayList result = new ArrayList();
        for (Object perm : wanted) {
            if (!hasPermission((String) perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    private boolean hasPermission(String permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    } private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }
}
