package com.kwiky.executivemodule.authentication;


import static android.Manifest.permission.CAMERA;
import static com.kwiky.executivemodule.Activities.RegisterVendorActivity.UPDATE_LOCATION;
import static com.kwiky.executivemodule.Config.Config.APP_ID;
import static com.kwiky.executivemodule.Config.Config.APP_ID_VALUE;
import static com.kwiky.executivemodule.Config.Config.GET_TERMSCONDITIONS;
import static com.kwiky.executivemodule.Config.Config.MAINTENANCE;
import static com.kwiky.executivemodule.Config.Config.OOPS;
import static com.kwiky.executivemodule.Config.Config.REGISTER;
import static com.kwiky.executivemodule.Config.Config.SEND_OTP;
import static com.kwiky.executivemodule.Config.Config.USER_TOKEN;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kwiky.executivemodule.Activities.FindAddressInMap;
import com.kwiky.executivemodule.Config.Config;
import com.kwiky.executivemodule.Helpers.CustomDialog;
import com.kwiky.executivemodule.Helpers.NetworkUtil;
import com.kwiky.executivemodule.Helpers.Utility;
import com.kwiky.executivemodule.Helpers.uiHelpers.UImsgs;
import com.kwiky.executivemodule.R;
import com.kwiky.executivemodule.models.Exicutivetypes;
import com.kwiky.executivemodule.utilities.AppConstants;
import com.kwiky.executivemodule.utilities.GpsUtils;
import com.kwiky.executivemodule.utilities.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SignUpKYCActivity extends AppCompatActivity implements LocationListener {

    private EditText et_last_name, et_first_name, et_location, et_pincode,et_email, et_mobile, et_adhar_number,et_password;
    private ImageView img_back_aadhar,img_front_aadhar,img_reference_aadhar,profile_photo, add_cover_photo, img_adhar, img_adhar_preview, img_bankpassbook;
    private Bitmap bm_adhar,bm_bankpasbook,bm_profileimage,bm_front_adhar,bm_back_adhar;
    private String adhar64="",bankpas64="",profile64="",adhar_front64="",adhar_back64="",exicutive_type_id="1";
    private Spinner state_spinner, district_spinner, constituency_spinner;
    private CheckBox cb_terms;
    private Button btn_submit;
    LocationManager locationManager;
    Geocoder geocoder;
    List<Address> addresses;
    Spinner item_type_spinner;
    private LocationCallback locationCallback;
    private FusedLocationProviderClient mFusedLocationClient;
    private double wayLatitude = 0.0, wayLongitude = 0.0;
    private LocationRequest locationRequest;
    private boolean isGPS = false;
    private boolean isContinue = false;
    char imageSelection;
    private int whichimage;
    private String userChoosenTask;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private boolean isCameraPermissionGranted;
    private String base64image;
    private CustomDialog customDialog;
    private ArrayList permissionsToRequest;
    private ArrayList permissionsRejected = new ArrayList();
    private ArrayList permissions = new ArrayList();
    private final static int ALL_PERMISSIONS_RESULT = 101;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;
    private static final String MOBILE_PATTERN = "^[6-9]\\d{9}$";
    private Pattern patternMobile = Pattern.compile(MOBILE_PATTERN);
    private static final String AADHAR_PATTERN = "^[2-9]{1}[0-9]{11}$";//"[0-9]{12}";
    private Pattern patternAADHAR = Pattern.compile(AADHAR_PATTERN);
    private static final String EMAIL_PATTERN = "[a-z0-9.]+@[a-z]+\\.[a-z]{2,3}";
    private Pattern patternEMAILID = Pattern.compile(EMAIL_PATTERN);
    private Matcher matcher;
    private boolean skipOTPVerification=true;
    PreferenceManager preferenceManager;
    private ArrayList<Exicutivetypes> exicutive_types = new ArrayList<>();

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_kycactivity);
        // getSupportActionBar().hide();

        preferenceManager = new PreferenceManager(getApplicationContext());
        init();


        permissions.add(CAMERA);

        permissionsToRequest = findUnAskedPermissions(permissions);
        if (permissionsToRequest.size() > 0)
            requestPermissions((String[]) permissionsToRequest.toArray(new String[permissionsToRequest.size()]),
                    ALL_PERMISSIONS_RESULT);


        new GpsUtils(this).turnGPSOn(new GpsUtils.onGpsListener() {
            @Override
            public void gpsStatus(boolean isGPSEnable) {
                // turn on GPS
                isGPS = isGPSEnable;
            }
        });

        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    if (location != null) {
                        wayLatitude = location.getLatitude();
                        wayLongitude = location.getLongitude();
                        if (!isContinue) {
                            try {
                                addresses = geocoder.getFromLocation(wayLatitude, wayLongitude, 1);
                                if (addresses != null && addresses.size() > 0) {
                                    String address = addresses.get(0).getAddressLine(0);
                                    String city = addresses.get(0).getLocality();
                                    String state = addresses.get(0).getAdminArea();
                                    String zip = addresses.get(0).getPostalCode();
                                    String country = addresses.get(0).getCountryName();
                                    System.out.println("aaaaaaaaa  address " + address + " 1 " + addresses.get(0).getAddressLine(1));
                                    currentAddress = Utility.getShortcutAddress(addresses);//address;

                                    //et_location.setText(address);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } else {
                           /* stringBuilder.append(wayLatitude);
                            stringBuilder.append("-");
                            stringBuilder.append(wayLongitude);
                            stringBuilder.append("\n\n");
*/
                            try {
                                addresses = geocoder.getFromLocation(wayLatitude, wayLongitude, 1);
                                String address = addresses.get(0).getAddressLine(0);
                                String city = addresses.get(0).getLocality();
                                String state = addresses.get(0).getAdminArea();
                                String zip = addresses.get(0).getPostalCode();
                                String country = addresses.get(0).getCountryName();
                                System.out.println("aaaaaaaaa  address " + address + " 1 " + addresses.get(0).getAddressLine(1));
                                // et_location.setText(address);
                                String locality=addresses.get(0).getSubLocality();
                                currentAddress = Utility.getShortcutAddress(addresses);//address;
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        if (!isContinue && mFusedLocationClient != null) {
                            mFusedLocationClient.removeLocationUpdates(locationCallback);
                        }
                    }
                }
            }
        };

        isContinue = false;
        getLocation();
        getLocationChange();

        add_cover_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage(0);
            }
        });
        img_adhar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage(1);
            }
        });
        img_bankpassbook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage(2);
            }
        });

    }
    private ArrayList findUnAskedPermissions(ArrayList wanted) {
        ArrayList result = new ArrayList();
        for (Object perm : wanted) {
            if (!hasPermission((String) perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    private boolean hasPermission(String permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    } private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    String currentAddress;

    public void init() {
        et_last_name = findViewById(R.id.et_last_name);
        et_first_name = findViewById(R.id.et_first_name);
        profile_photo = findViewById(R.id.profile_photo);
        add_cover_photo = findViewById(R.id.add_cover_photo);
        et_location = findViewById(R.id.et_location);
        state_spinner = findViewById(R.id.state_spinner);
        district_spinner = findViewById(R.id.district_spinner);
        constituency_spinner = findViewById(R.id.constituency_spinner);
        et_pincode = findViewById(R.id.et_pincode);
        et_mobile = findViewById(R.id.et_mobile);
        et_adhar_number = findViewById(R.id.et_adharnumber);
        img_adhar = findViewById(R.id.img_adhar);
        img_adhar_preview = findViewById(R.id.img_adhar_preview);
        img_bankpassbook = findViewById(R.id.img_bankpassbook);
        cb_terms = findViewById(R.id.cb_terms);
        btn_submit = findViewById(R.id.btn_submit);
        et_password = findViewById(R.id.et_password);
        et_email = findViewById(R.id.et_email);
        img_reference_aadhar = findViewById(R.id.img_reference_aadhar);
        img_front_aadhar = findViewById(R.id.img_front_aadhar);
        img_back_aadhar = findViewById(R.id.img_back_aadhar);
        item_type_spinner = findViewById(R.id.item_type_spinner);

        RelativeLayout location_layout= (RelativeLayout) findViewById(R.id.location_layout);
        location_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Gson gson=new Gson();
                ArrayList<String> locArray=new ArrayList<>();

                locArray.add(currentAddress);
                locArray.add(""+wayLatitude);
                locArray.add(""+wayLongitude);
                locArray.add("singleLineAddress");
                String locationString = gson.toJson(locArray);
                Intent intent=new Intent(getApplicationContext(), FindAddressInMap.class);
                intent.putExtra("location",locationString);
                startActivityForResult(intent,UPDATE_LOCATION);
            }
        });

        geocoder = new Geocoder(SignUpKYCActivity.this, Locale.getDefault());
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10 * 1000); // 10 seconds
        locationRequest.setFastestInterval(5 * 1000); // 5 seconds
       // item_type_spinner.setVisibility(View.GONE);
        getTypes();


        customDialog=new CustomDialog(SignUpKYCActivity.this);
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValid()){
                    if (cb_terms.isChecked()){
                       /* Map<String, String> dataMap = new HashMap<>();
                        dataMap.put("first_name", et_first_name.getText().toString());
                        dataMap.put("last_name", et_last_name.getText().toString());
                        dataMap.put("mobile", et_mobile.getText().toString());
                        dataMap.put("email", et_email.getText().toString());
                        dataMap.put("password", et_password.getText().toString());
                        dataMap.put("aadhar_number", et_adhar_number.getText().toString());
                        dataMap.put("latitude", ""+wayLatitude);
                        dataMap.put("longitude", ""+wayLongitude);
                        dataMap.put("geo_lcoation_address", ""+et_location.getText().toString());
                        //  dataMap.put("aadhar_card_image", ""+adhar64);
                        dataMap.put("bank_passbook_image", ""+bankpas64);
                        dataMap.put("profile", ""+profile64);
                        dataMap.put("aadhar_card_image_front", ""+adhar_front64);
                        dataMap.put("aadhar_card_image_back", ""+adhar_back64);

                        JSONObject json = new JSONObject(dataMap);
                        register(json);
                        System.out.println("aaaaaaaaaa   json  "+json);*/

                        if(skipOTPVerification)
                        {
                            callLoginAPI();
                        }
                        else
                            sendOtp(et_mobile.getText().toString());
                    }else {
                        UImsgs.showToast(SignUpKYCActivity.this,"Plese accept terms and conditions");
                    }

                }
            }
        });
        cb_terms.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    if (NetworkUtil.isNetworkConnected(SignUpKYCActivity.this)) {
                        gettermsandconditions();
                    } else {
                        Toast.makeText(SignUpKYCActivity.this, ""+getResources().getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });
        img_reference_aadhar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                previewImage(R.drawable.aadhar_refernce);
            }
        });
        img_front_aadhar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage(101);
            }
        });
        img_back_aadhar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage(102);
            }
        });
    }

    public void previewImage(Integer image) {
        //image preivew
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.image_dialog);
        Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        TextView txt_upload_message = (TextView) dialog.findViewById(R.id.txt_upload_message);
        ImageView closeButton = (ImageView) dialog.findViewById(R.id.closeButton);
        closeButton.setVisibility(View.GONE);//looking not good
        ImageView img_preview = (ImageView) dialog.findViewById(R.id.img_preview);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        Glide.with(this)
                .load(image)
                .placeholder(R.drawable.no_image)
                .into(img_preview);


        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();
    }
    private void showVerifyOTPDialog(String otp, final String mobilenumber) {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.verify_otp_dialog);
        Button applyButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        ImageView closeButton = (ImageView) dialog.findViewById(R.id.closeButton);
        final EditText editTextone = (EditText) dialog.findViewById(R.id.editTextone);
        final EditText editTexttwo = (EditText) dialog.findViewById(R.id.editTexttwo);
        final EditText editTextthree = (EditText) dialog.findViewById(R.id.editTextthree);
        final EditText editTextfour = (EditText) dialog.findViewById(R.id.editTextfour);
        final EditText editTextFive = (EditText) dialog.findViewById(R.id.editTextFive);
        final EditText editTextSix = (EditText) dialog.findViewById(R.id.editTextSix);
        TextView tv_error = (TextView) dialog.findViewById(R.id.tv_error);
        final TextView tv_timer_resend = (TextView) dialog.findViewById(R.id.tv_timer_resend);

        new CountDownTimer(30000, 1000) {

            public void onTick(long millisUntilFinished) {
                tv_timer_resend.setText("Resends remaining: " + millisUntilFinished / 1000);
                //here you can have your logic to set text to edittext
            }

            public void onFinish() {
                tv_timer_resend.setText("Resend Otp");
            }

        }.start();
        tv_timer_resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendOtp(mobilenumber);
            }
        });

        if(otp!=null&& !otp.isEmpty() && otp.length() >= 6)
        {
            tv_error.setVisibility(View.VISIBLE);
            editTextone.setText(otp.substring(0, 1));
            editTexttwo.setText(otp.substring(1, 2));
            editTextthree.setText(otp.substring(2, 3));
            editTextfour.setText(otp.substring(3, 4));
            editTextFive.setText(otp.substring(4, 5));
            editTextSix.setText(otp.substring(5, 6));
        }

        editTextone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (editTextone.length() == 1) {
                    editTextone.clearFocus();
                    editTexttwo.requestFocus();
                    editTexttwo.setCursorVisible(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        editTexttwo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (editTexttwo.length() == 1) {
                    editTexttwo.clearFocus();
                    editTextthree.requestFocus();
                    editTextthree.setCursorVisible(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        editTextthree.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (editTextthree.length() == 1) {
                    editTextthree.clearFocus();
                    editTextfour.requestFocus();
                    editTextfour.setCursorVisible(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        editTextfour.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (editTextfour.length() == 1) {
                    editTextfour.clearFocus();
                    editTextFive.requestFocus();
                    editTextFive.setCursorVisible(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        editTextFive.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (editTextFive.length() == 1) {
                    editTextFive.clearFocus();
                    editTextSix.requestFocus();
                    editTextSix.setCursorVisible(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        applyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String otp = editTextone.getText().toString() + editTexttwo.getText().toString() +
                        editTextthree.getText().toString() + editTextfour.getText().toString() +
                        editTextFive.getText().toString() + editTextSix.getText().toString();

                if (otp != null && !otp.isEmpty() && otp.length() >= 6) {
                    dialog.dismiss();
                    callVerifyOTPApi(otp,mobilenumber);
                }
            }
        });
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();
    }
    private void sendOtp(final String mobile) {
        customDialog.show();
        Map<String, String> fcmMap = new HashMap<>();
        fcmMap.put("mobile", mobile);
        fcmMap.put("password", et_password.getText().toString());
        JSONObject json = new JSONObject(fcmMap);
        final String data = json.toString();

        System.out.println("sendOtp request "+json.toString());
        RequestQueue requestQueue = Volley.newRequestQueue(SignUpKYCActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                SEND_OTP, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (response != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        System.out.println("aaaaaaa response "+jsonObject.toString());
                        boolean status = jsonObject.getBoolean("status");
                        if (status) {
                            showVerifyOTPDialog(null,mobile);
                        } else {
                            UImsgs.showToast(SignUpKYCActivity.this, Html.fromHtml(jsonObject.getString("message")).toString());
                        }
                    } catch (Exception e) {
                        UImsgs.showToast(SignUpKYCActivity.this, "Something went wrong");
                        e.printStackTrace();
                    } finally {
                        customDialog.dismiss();
                    }
                } else {
                    customDialog.dismiss();
                    UImsgs.showToast(SignUpKYCActivity.this, "Server under maintenance");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customDialog.dismiss();
                System.out.println("aaaaaaa error  "+error.getMessage());
                UImsgs.showToast(SignUpKYCActivity.this, "Something went wrong");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                //  map.put(AUTH_TOKEN, preferenceManager.getString(TOKEN_KEY));
                map.put(APP_ID, APP_ID_VALUE);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    private void callVerifyOTPApi(final String otp, final String mobilenumber) {

        customDialog.show();

        Map<String, String> fcmMap = new HashMap<>();
        fcmMap.put("mobile", et_mobile.getText().toString());
        fcmMap.put("otp", otp);
        JSONObject json = new JSONObject(fcmMap);
        final String data = json.toString();
        RequestQueue requestQueue = Volley.newRequestQueue(SignUpKYCActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Config.VERIFY_OTP, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.d("VolleyResponse", "VERIFY_OTP: respnse " + response);

                    JSONObject jsonObject = new JSONObject(response);
                    System.out.println("aaaaaaaa verifyotp  "+jsonObject.toString());
                    boolean status = jsonObject.getBoolean("status");
                    if (!status) {

                        UImsgs.showToast(SignUpKYCActivity.this, Html.fromHtml(jsonObject.getString("message")).toString());
                        //show alert on same dialog
                        showVerifyOTPDialog(otp,mobilenumber);
                    } else {
                        callLoginAPI();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    customDialog.dismiss();
                } finally {
                    customDialog.dismiss();
                }
                //Toast.makeText(mContext, "responce "+response, Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customDialog.dismiss();
                Log.d("fetch user error", error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(APP_ID, APP_ID_VALUE);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void callLoginAPI() {
        Map<String, Object> dataMap = new HashMap<>();
        dataMap.put("intent", Utility.ExecutiveIntent);
        dataMap.put("aadhar", et_adhar_number.getText().toString());
        dataMap.put("bank_passbook_image", ""+bankpas64);
        dataMap.put("aadhar_card_image_front", ""+adhar_front64);
        dataMap.put("aadhar_card_image_back", ""+adhar_back64);

        Map<String, String> businessAddress = new HashMap<>();//business_address
        businessAddress.put("location", ""+et_location.getText().toString());
        businessAddress.put("lat",""+ wayLatitude);
        businessAddress.put("lng", ""+wayLongitude);
        businessAddress.put("executive_type_id", ""+exicutive_type_id);
        dataMap.put("executive_address", businessAddress);

        JSONObject json = new JSONObject(dataMap);
        System.out.println("aaaaaaaaaa   json  "+json);
        if (NetworkUtil.isNetworkConnected(SignUpKYCActivity.this)) {
            register(json);
        } else {
            Toast.makeText(SignUpKYCActivity.this, ""+getResources().getString(R.string.nointernet), Toast.LENGTH_SHORT).show();
        }


    }

    private void gettermsandconditions() {
        customDialog.show();
        Map<String, String> termsmpa = new HashMap<>();
        termsmpa.put("?page_id=", "1");
        final String data = new JSONObject(termsmpa).toString();
        String url=GET_TERMSCONDITIONS+"?page_id=1";
        System.out.println("aaaaaaaaaa url  "+url);

        RequestQueue requestQueue = Volley.newRequestQueue(SignUpKYCActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("cat_res", response);
                        customDialog.dismiss();
                        if (response != null) {
                            try {

                                JSONObject jsonObject = new JSONObject(response);
                                System.out.println("aaaaaaaa gettransac  "+jsonObject.toString());
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {

                                    String title,desc;

                                    try{
                                        JSONArray jsonArray=jsonObject.getJSONArray("data");
                                        title=jsonArray.getJSONObject(0).getString("title");
                                        desc=jsonArray.getJSONObject(0).getString("desc");

                                    }catch (Exception e){
                                        JSONObject dataobject = jsonObject.getJSONObject("data");
                                        title=dataobject.getString("title");
                                        desc=dataobject.getString("desc");
                                    }


                                    LayoutInflater inflater = getLayoutInflater();
                                    View alertLayout = inflater.inflate(R.layout.activity_web, null);

                                    TextView tv_terms_id = alertLayout.findViewById(R.id.tv_terms_id);


                                    AlertDialog.Builder alert = new AlertDialog.Builder(SignUpKYCActivity.this);
                                    alert.setIcon(R.mipmap.ic_launcher);
                                    alert.setTitle(title);
                                    // this is set the view from XML inside AlertDialog
                                    alert.setView(alertLayout);
                                    alert.setCancelable(false);
                                    tv_terms_id.setText(Html.fromHtml(desc));
                                    alert.setCancelable(true);
                                    alert.setPositiveButton("Accept", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            cb_terms.setChecked(true);
                                        }
                                    });
                                    AlertDialog dialog = alert.create();
                                    dialog.show();

                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {

                            UImsgs.showToast(SignUpKYCActivity.this, getString(R.string.maintenance));
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        customDialog.dismiss();
                        UImsgs.showToast(SignUpKYCActivity.this, getString(R.string.oops));
                        System.out.println("aaaaaaa  111 "+error.getMessage());
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put("APP_ID", "TXc9PQ==");
                return map;
            }

           /* @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }*/
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }
    private void register(JSONObject json) {
        customDialog.show();
        final String data = json.toString();
        RequestQueue requestQueue = Volley.newRequestQueue(SignUpKYCActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.MainAppVendorRegistration, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    System.out.println("aaaaa  response "+response.toString());
                    try {
                        customDialog.dismiss();
                        JSONObject jsonObject = new JSONObject(response);
                        System.out.println("aaaaaaaaa jsonobject "+jsonObject.toString());
                        boolean status = jsonObject.getBoolean("status");
                        if (status) {
                            String message = jsonObject.getString("message");
                            Toast.makeText(SignUpKYCActivity.this, ""+message, Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                            Toast.makeText(SignUpKYCActivity.this, Html.fromHtml(jsonObject.getString("message")), Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        System.out.println("aaaaaaa catch  "+e.getMessage());
                        customDialog.dismiss();
                        //  UiMsgs.showToast(mContext, "Something went wrong");
                        e.printStackTrace();
                    }
                } else {
                    System.out.println("aaaaaaa catch Server under maintenance ");
                    customDialog.dismiss();
                    // UiMsgs.showToast(mContext, "Server under maintenance");
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customDialog.dismiss();
                System.out.println("aaaaaaa error  "+error.getMessage());
                //   UiMsgs.showToast(mContext, "Something went wrong");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                //map.put(APP_ID, APP_ID_VALUE);
                map.put("X_AUTH_TOKEN", preferenceManager.getString(USER_TOKEN));
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }
    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(SignUpKYCActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(SignUpKYCActivity.this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(SignUpKYCActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    AppConstants.LOCATION_REQUEST);

        } else {
            if (isContinue) {
                mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
            } else {
                mFusedLocationClient.getLastLocation().addOnSuccessListener(SignUpKYCActivity.this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        if (location != null) {
                            wayLatitude = location.getLatitude();
                            wayLongitude = location.getLongitude();
                            List<Address> addresses = null;
                            try {
                                addresses = geocoder.getFromLocation(wayLatitude, wayLongitude, 1);
                                if (addresses != null && addresses.size() > 0) {
                                    String address = addresses.get(0).getAddressLine(0);
                                    String city = addresses.get(0).getLocality();
                                    String state = addresses.get(0).getAdminArea();
                                    String zip = addresses.get(0).getPostalCode();
                                    String country = addresses.get(0).getCountryName();
                                    String locality=addresses.get(0).getSubLocality();
                                    System.out.println("aaaaaaa address " + address);
                                    currentAddress = Utility.getShortcutAddress(addresses);//address;
                                    // et_location.setText(address);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                        } else {
                            if (ActivityCompat.checkSelfPermission(SignUpKYCActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(SignUpKYCActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                // TODO: Consider calling
                                //    ActivityCompat#requestPermissions
                                // here to request the missing permissions, and then overriding
                                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                //                                          int[] grantResults)
                                // to handle the case where the user grants the permission. See the documentation
                                // for ActivityCompat#requestPermissions for more details.
                                return;
                            }
                            mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
                        }
                    }
                });
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        switch (requestCode) {

            case MY_CAMERA_PERMISSION_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(cameraIntent, REQUEST_CAMERA);
                }
                break;
            case ALL_PERMISSIONS_RESULT:
                for (Object perms : permissionsToRequest) {
                    if (!hasPermission((String) perms)) {
                        permissionsRejected.add(perms);
                    }
                }

                if (permissionsRejected.size() > 0) {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale((String) permissionsRejected.get(0))) {
                          /*  showMessageOKCancel("These permissions are mandatory for the application. Please allow access.",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions((String[]) permissionsRejected.toArray(new
                                                        String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    });*/
                            return;
                        }
                    }

                }

                break;
            default:
                int permissionLocation = ContextCompat.checkSelfPermission(SignUpKYCActivity.this,
                        Manifest.permission.ACCESS_FINE_LOCATION);
                if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
                    getLocation();
                }
                if(isCameraPermissionGranted) {
                    int cameraLocation = ContextCompat.checkSelfPermission(SignUpKYCActivity.this,
                            CAMERA);
                    if (cameraLocation == PackageManager.PERMISSION_GRANTED) {
                        isCameraPermissionGranted =false;
                        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, REQUEST_CAMERA);
                    }

                }
                break;
        }
    }

    void getLocationChange() {

        try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 5, (android.location.LocationListener) this);
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(@NonNull Location location) {
        try {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            /*locationText.setText(locationText.getText() + "\n"+addresses.get(0).getAddressLine(0)+", "+
                    addresses.get(0).getAddressLine(1)+", "+addresses.get(0).getAddressLine(2));
            */
            String address = addresses.get(0).getAddressLine(0) + "";
            String[] arr = address.split(",");
            /*Toast.makeText(this, arr.length+"", Toast.LENGTH_SHORT).show();*/

            Double lat1 = location.getLatitude();
            Double lang1 = location.getLongitude();
            wayLatitude = lat1;
            wayLongitude = lang1;
            System.out.println("aaaaaaaaa  address 1 "+address);
            String city = addresses.get(0).getLocality();
            String locality=addresses.get(0).getSubLocality();
            currentAddress = Utility.getShortcutAddress(addresses);//address;
            //et_location.setText(address);


        } catch (Exception e) {

        }
    }
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }
    @Override
    public void onProviderEnabled(String provider) {

    }
    @Override
    public void onProviderDisabled(String provider) {

    }


    private void selectImage(int whichimage) {
        this.whichimage=whichimage;
        final CharSequence[] items = {getString(R.string.take_photo), getString(R.string.choose_from_library),
                getString(R.string.cancel) };

        AlertDialog.Builder builder = new AlertDialog.Builder(SignUpKYCActivity.this);
        builder.setTitle(getString(R.string.add_photo));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result= Utility.checkPermission(SignUpKYCActivity.this);

                if (items[item].equals(getString(R.string.take_photo))) {
                    userChoosenTask =getString(R.string.take_photo);
                    if(result) {
                        if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                            //requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
                            isCameraPermissionGranted = true;
                            ActivityCompat.requestPermissions(SignUpKYCActivity.this, new String[]{Manifest.permission.CAMERA},
                                    MY_CAMERA_PERMISSION_CODE);
                        } else {
                            // cameraIntent();
                            isCameraPermissionGranted = false;
                            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(cameraIntent, REQUEST_CAMERA);
                        }
                    }
                    else
                        isCameraPermissionGranted=true;
                } else if (items[item].equals(getString(R.string.choose_from_library))) {
                    userChoosenTask =getString(R.string.choose_from_library);
                    if(result)
                        galleryIntent();

                } else if (items[item].equals(getString(R.string.cancel))){
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }
    private void galleryIntent()
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"),SELECT_FILE);
    }

    private void cameraIntent()
    {
        if(isCameraPermissionGranted) {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, REQUEST_CAMERA);
        }
        else
        {
            Toast.makeText(getApplicationContext(), getString(R.string.accept_camera_permission), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode== UPDATE_LOCATION) {
            updateLocation(data);
        }
        if (resultCode == Activity.RESULT_OK) {
            // onCaptureImageResult(data);
            if (requestCode == AppConstants.GPS_REQUEST) {
                isGPS = true; // flag maintain before get location
            } if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }

    private void updateLocation(Intent intent) {
        ArrayList<String> locArray=new ArrayList<>();
        try {
            Gson gson = new Gson();
            String carListAsString = intent.getStringExtra("location");
            Type type = new TypeToken<ArrayList<String>>() {
            }.getType();
            locArray = gson.fromJson(carListAsString, type);
            if(locArray!=null && locArray.size()>=3)
            {
                wayLatitude = Double.parseDouble(locArray.get(1));
                wayLongitude = Double.parseDouble(locArray.get(2));
                et_location.setText(locArray.get(0));
                currentAddress=locArray.get(0);
            }
        }catch (Exception ex) {
            getLocation();
        }
    }

    public String convert(Bitmap bitmap) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, outputStream);
        return Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);
    }
    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm=null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
                base64image=convert(bm);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 50, stream);
        byte[] imageInByte = stream.toByteArray();
        long lengthbmp = imageInByte.length/1024;

        System.out.println("aaaaaaaaaa  size "+lengthbmp);
        setimage(whichimage,bm);

    }
    public void setimage(int whichimage,Bitmap bitmap){
        switch (whichimage){
            case 0:
                profile_photo.setImageBitmap(bitmap);
                bm_profileimage=bitmap;
                profile64=convert(bitmap);
                break;
            case 1:
                img_adhar_preview.setImageBitmap(bitmap);
                bm_adhar=bitmap;
                adhar64=convert(bitmap);
                break;
            case 2:
                img_bankpassbook.setImageBitmap(bitmap);
                bm_bankpasbook=bitmap;
                bankpas64=convert(bitmap);
                break;
            case 101:
                img_front_aadhar.setImageBitmap(bitmap);
                bm_front_adhar=bitmap;
                adhar_front64=convert(bitmap);
                break;
            case 102:
                img_back_aadhar.setImageBitmap(bitmap);
                bm_back_adhar=bitmap;
                adhar_back64=convert(bitmap);
                break;
        }
    }
    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 50, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        setimage(whichimage,thumbnail);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        byte[] imageInByte = stream.toByteArray();
        long lengthbmp = imageInByte.length/1024;

        System.out.println("aaaaaaaaaa  size "+lengthbmp);


    }

    public static void setEditTextErrorMethod(EditText editText, String err_msg){
        editText.setError(err_msg);
        editText.requestFocus();
    }

    private boolean isValid() {
        boolean valid = true;


       /* int password_length=et_password.getText().toString().trim().length();

        if (et_first_name.getText().toString().trim().isEmpty()) {
            setEditTextErrorMethod(et_first_name, getString(R.string.empty));
            valid = false;
        }  else if (et_mobile.getText().toString().trim().isEmpty()) {
            setEditTextErrorMethod(et_mobile, getString(R.string.empty));
            valid = false;
        } else if (et_mobile.getText().toString().trim().length() <10) {
            setEditTextErrorMethod(et_mobile, getString(R.string.invalid_mobile));
            valid = false;
        }else if (!isValidPhoneNumber(et_mobile.getText().toString().trim())){
            setEditTextErrorMethod(et_mobile, getString(R.string.invalid_mobile_number));
            valid = false;
        }else if (!validateMOBILENUMBER(et_mobile.getText().toString())) {
            setEditTextErrorMethod(et_mobile, getString(R.string.invalid_mobile_number));
            valid = false;
        }
        else if (et_email.getText().toString().trim().isEmpty()) {
            setEditTextErrorMethod(et_email, getString(R.string.empty));
            valid = false;
        }else if (!isValidEmail(et_email.getText().toString().trim())){
            setEditTextErrorMethod(et_email, getString(R.string.invalid_mail_id));
            valid = false;
        }
        else if (et_password.getText().toString().trim().isEmpty()) {
            setEditTextErrorMethod(et_password, getString(R.string.empty));
            valid = false;
        } else if (password_length < 6) {
            setEditTextErrorMethod(et_password, getString(R.string.invalid_password));
            valid = false;
        }*/
        if (et_location.getText().toString().isEmpty()) {
            setEditTextErrorMethod(et_location, getString(R.string.empty));
            valid = false;
        }
        else if (et_adhar_number.getText().toString().isEmpty()){
            setEditTextErrorMethod(et_adhar_number, getString(R.string.empty));
            valid = false;
        }else if (et_adhar_number.getText().toString().length()<12){
            setEditTextErrorMethod(et_adhar_number, getString(R.string.min_digits_required_12));
            valid = false;
        }else if (!validatenAADHAR(et_adhar_number.getText().toString())) {
            setEditTextErrorMethod(et_adhar_number, getString(R.string.upload_valid_aadhaar));
            valid=false;
        }
        else if (adhar_front64.isEmpty()) {
            Toast.makeText(this, getString(R.string.upload_aadhar_front), Toast.LENGTH_SHORT).show();
            valid = false;
        }else if (adhar_back64.isEmpty()) {
            Toast.makeText(this, getString(R.string.upload_aadhar_back), Toast.LENGTH_SHORT).show();
            valid = false;
        } else if (bankpas64.isEmpty()) {
            Toast.makeText(this, getString(R.string.upload_passbook), Toast.LENGTH_SHORT).show();
            valid = false;
        }
        /*else if (profile64.isEmpty()) {
            Toast.makeText(this, getString(R.string.upload_profile), Toast.LENGTH_SHORT).show();
            valid = false;
        }*/

        return valid;

    }
    private boolean isValidPhoneNumber(CharSequence phoneNumber) {
        if (!TextUtils.isEmpty(phoneNumber)) {
            return Patterns.PHONE.matcher(phoneNumber).matches();
        }
        return false;
    }
    public boolean validateMOBILENUMBER(String mobile){
        matcher = patternMobile.matcher(mobile);
        return matcher.matches();
    }
    public boolean validatenAADHAR(String aadhar){
        matcher = patternAADHAR.matcher(aadhar);
        return matcher.matches();
    }
    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    ArrayList<String> executiveIDs = new ArrayList<>();
    private void getTypes() {

        RequestQueue requestQueue = Volley.newRequestQueue(SignUpKYCActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Config.ECITIVE_TYPES,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("tax_res", response);
                        if (response != null) {
                            try {

                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    JSONArray dataArray = jsonObject.getJSONArray("data");
                                    if (dataArray.length() > 0) {
                                        exicutive_types = new ArrayList<>();

                                        for (int i = 0; i < dataArray.length(); i++) {
                                            JSONObject categoryObject = dataArray.getJSONObject(i);
                                            Exicutivetypes exicutivetypes=new Exicutivetypes();
                                            exicutivetypes.setId(categoryObject.getString("id"));
                                            exicutivetypes.setExecutive_type(categoryObject.getString("executive_type"));
                                            exicutive_types.add(exicutivetypes);
                                            executiveIDs.add(exicutivetypes.getExecutive_type());
                                        }

                                      /*  ArrayAdapter<String> adapter = new ArrayAdapter<String>(SignUpKYCActivity.this, android.R.layout.simple_spinner_item, exicutive_types);
                                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                        item_type_spinner.setAdapter(adapter);
                                        item_type_spinner.setSelection(1);*/

                                        exicutive_type_id=exicutive_types.get(0).getId();

                                        ArrayAdapter userAdapter = new ArrayAdapter(SignUpKYCActivity.this, R.layout.spinner_types,
                                                executiveIDs);


                                        item_type_spinner.setAdapter(userAdapter);
                                        item_type_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                            @Override
                                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                                // Get the value selected by the user
                                                // e.g. to store it as a field or immediately call a method
                                                exicutive_type_id=exicutive_types.get(position).getId();
                                            }

                                            @Override
                                            public void onNothingSelected(AdapterView<?> parent) {
                                            }
                                        });
                                    }
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            UIMsgs.showToast(SignUpKYCActivity.this, MAINTENANCE);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        UIMsgs.showToast(SignUpKYCActivity.this, OOPS);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }
}