package com.kwiky.executivemodule.authentication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.kwiky.executivemodule.Helpers.CustomDialog;
import com.kwiky.executivemodule.R;
import com.kwiky.executivemodule.utilities.PreferenceManager;

public class WelcomeActivity extends AppCompatActivity   implements View.OnClickListener {

    private Context mContext;
    PreferenceManager preferenceManager;
    private CustomDialog mCustomDialog;

    //variables
    ImageView signup_back_button;
    Button signup_next_button, signup_login_button;
    private TextView signup_title_text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);


        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
        }
        init();
    }

    private void init() {
        mContext = WelcomeActivity.this;
        mCustomDialog = new CustomDialog(mContext);
        preferenceManager = new PreferenceManager(mContext);

        signup_back_button = findViewById(R.id.signup_back_button);
        signup_next_button = findViewById(R.id.signup_next_button);
        signup_login_button = findViewById(R.id.signup_login_button);
        signup_title_text = findViewById(R.id.signup_title_text);

        signup_back_button.setOnClickListener(this);
    }

    public void callNextSignupScreen(View view) {
        Intent intent = new Intent(WelcomeActivity.this, SignUpKYCActivity.class);

        //Add transition
       /* Pair[] pairs = new Pair[4];
        pairs[0] = new Pair<View, String>(signup_back_button, "transition_back_arrow_btn");
        pairs[1] = new Pair<View, String>(signup_title_text, "transition_title_text");
        pairs[2] = new Pair<View, String>(signup_next_button, "transition_next_btn");
        pairs[3] = new Pair<View, String>(signup_login_button, "transition_login_btn");

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(WelcomeActivity.this, pairs);
            startActivity(intent, options.toBundle());
        } else
        */
        {
            startActivity(intent);
        }

        finish();
    }
    public void callLoginActivity(View view) {
        // Intent intent = new Intent(WelcomeActivity.this, SigninActivity.class);
        //  startActivity(intent);
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.signup_back_button:
                finish();
                break;
        }
    }
}