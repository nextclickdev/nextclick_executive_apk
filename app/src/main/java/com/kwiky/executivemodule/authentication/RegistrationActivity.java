package com.kwiky.executivemodule.authentication;

import static android.Manifest.permission.READ_SMS;
import static android.Manifest.permission.RECEIVE_SMS;
import static android.Manifest.permission.SEND_SMS;
import static android.view.KeyEvent.KEYCODE_DEL;

import static com.kwiky.executivemodule.Config.Config.APP_ID;
import static com.kwiky.executivemodule.Config.Config.APP_ID_VALUE;
import static com.kwiky.executivemodule.Config.Config.REGISTER_USER;
import static com.kwiky.executivemodule.Config.Config.USER_TOKEN;
import static com.kwiky.executivemodule.Constants.IErrors.EMPTY;
import static com.kwiky.executivemodule.Constants.IErrors.INVALID;
import static com.kwiky.executivemodule.utilities.ValidationMessages.INVALID_MOBILE;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityOptions;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Pair;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.kwiky.executivemodule.Config.Config;
import com.kwiky.executivemodule.Helpers.CustomDialog;
import com.kwiky.executivemodule.Helpers.Utility;
import com.kwiky.executivemodule.Helpers.Validations;
import com.kwiky.executivemodule.Helpers.uiHelpers.IncomingSms;
import com.kwiky.executivemodule.Helpers.uiHelpers.UImsgs;
import com.kwiky.executivemodule.R;
import com.kwiky.executivemodule.utilities.PreferenceManager;

import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegistrationActivity extends AppCompatActivity implements View.OnClickListener {

    private TextInputEditText etUserId, etPassword;
    private Button btnLogin;
    private String user_id_str;
    private Context mContext;
    PreferenceManager preferenceManager;
    private CustomDialog mCustomDialog;
    private boolean isOtpVerified=false;
    private String verifiedmobile;
    Validations validations;
    ProgressDialog progressDialog;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1, SELECT_MULTIPLE_FILE = 1;
    private String userChoosenTask;
    public static String profile64 = "";

    //variables
    ImageView signup_back_button;
    Button signup_next_button, signup_login_button;
    private TextView signup_title_text;
    ImageView img_profile;

    TextInputEditText tv_user_first_name,tv_user_last_name,tv_user_mail,tv_user_mobile,tv_user_password,tv_user_re_password;


    private ArrayList permissionsToRequest;
    private ArrayList permissionsRejected = new ArrayList();
    private ArrayList permissions = new ArrayList();
    private final static int ALL_PERMISSIONS_RESULT = 101;

    private static final int CAMERA_REQUEST = 1888;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
        }
        init();

        mobile_str = getIntent().getStringExtra("mobile");
        if(mobile_str!=null && !mobile_str.isEmpty())
        {
            verifiedmobile=mobile_str;
            isOtpVerified=true;
        }

        //showVerifyOTPDialog("","");

        permissions.add(READ_SMS);
        permissions.add(RECEIVE_SMS);
        permissions.add(SEND_SMS);

       /* permissionsToRequest = findUnAskedPermissions(permissions);
        if (permissionsToRequest.size() > 0) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions((String[]) permissionsToRequest.toArray(new String[permissionsToRequest.size()]),
                        ALL_PERMISSIONS_RESULT);
            }
        }*/

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            /*if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA,Manifest.permission.READ_EXTERNAL_STORAGE}, MY_CAMERA_PERMISSION_CODE);
            }
             Utility.checkPermission(RegistrationActivity.this);*/

            if (ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ) {

                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CAMERA, android.Manifest.permission.READ_EXTERNAL_STORAGE}, 101);

            }
        }
    }

    private void init() {
        mContext = RegistrationActivity.this;
        mCustomDialog = new CustomDialog(mContext);
        preferenceManager = new PreferenceManager(mContext);
        validations = new Validations();

        signup_back_button = findViewById(R.id.signup_back_button);
        signup_next_button = findViewById(R.id.signup_next_button);
        signup_login_button = findViewById(R.id.signup_login_button);
        signup_title_text = findViewById(R.id.signup_title_text);

        tv_user_first_name= findViewById(R.id.tv_user_first_name);
        tv_user_last_name= findViewById(R.id.tv_user_last_name);
        tv_user_mail= findViewById(R.id.tv_user_mail);
        tv_user_mobile= findViewById(R.id.tv_user_mobile);
        tv_user_password= findViewById(R.id.tv_user_password);
        tv_user_re_password= findViewById(R.id.tv_user_re_password);

        signup_back_button.setOnClickListener(this);
        img_profile=findViewById(R.id.img_profile);

        img_profile.setOnClickListener(this);
    }

    public void callNextSignupScreen(View view) {
        boolean isValid=validator();
        if (isValid) {
            if (isOtpVerified && mobile_str.equals(verifiedmobile)) {
                dataSender();
            } else {
                sendOtp(mobile_str);
            }
        }
        /*  callNextScreen();*/
    }

    String  mobile_str, email_str,
            first_name_str,last_name_str, password_str,re_password_str;
    private boolean validator() {
        boolean validity = true;
        first_name_str=tv_user_first_name.getText().toString();
        last_name_str=tv_user_last_name.getText().toString();
        email_str=tv_user_mail.getText().toString();
        //mobile_str=tv_user_mobile.getText().toString();
        password_str=tv_user_password.getText().toString();
        re_password_str=tv_user_re_password.getText().toString();
        if (profile64 ==null || profile64.isEmpty()) {
            UImsgs.showToast(mContext, "Please upload the profile picture");
            validity = false;
        }
        else if (first_name_str.length() == 0) {
            tv_user_first_name.setError("Please Enter First Name");
            tv_user_first_name.requestFocus();
            validity = false;
        }
        else if (last_name_str.length() == 0) {
            tv_user_last_name.setError("Please Enter Last Name");
            tv_user_last_name.requestFocus();
            validity = false;
        }
        else if (validations.isBlank(email_str)) {
            tv_user_mail.setError(EMPTY);
            tv_user_mail.requestFocus();
            validity = false;
        }
        else if (validations.isValidEmail(email_str)) {
            tv_user_mail.setError(INVALID);
            tv_user_mail.requestFocus();
            validity = false;
        }
        else if (mobile_str.length() == 0) {
            tv_user_mobile.setError(EMPTY);
            tv_user_mobile.requestFocus();
            validity = false;
        }
        else if (mobile_str.length() < 10) {
            tv_user_mobile.setError(INVALID_MOBILE);
            tv_user_mobile.requestFocus();
            validity = false;
        }
        else if (mobile_str.startsWith("0") || (mobile_str.startsWith("1")) || mobile_str.startsWith("2")||
                mobile_str.startsWith("3")|| mobile_str.startsWith("4")||mobile_str.startsWith("5")){
            tv_user_mobile.setError(INVALID_MOBILE);
            tv_user_mobile.requestFocus();
            validity = false;
        }
        else  if (password_str.length() == 0) {
            tv_user_password.setError(EMPTY);
            tv_user_password.requestFocus();
            validity = false;
        }
        else if (password_str.length() < 6) {
            tv_user_password.setError("Minimum 6 Characters");
            tv_user_password.requestFocus();
            validity = false;
        } else  if (re_password_str.length() == 0) {
            tv_user_re_password.setError(EMPTY);
            tv_user_re_password.requestFocus();
            validity = false;
        }
        else if (re_password_str.length() < 6) {
            tv_user_re_password.setError("Minimum 6 Characters");
            tv_user_re_password.requestFocus();
            validity = false;
        }else if (!password_str.equalsIgnoreCase(re_password_str)){
            tv_user_re_password.setError("Password Not Match");
            tv_user_re_password.requestFocus();
            validity = false;
        }

        return validity;
    }

    boolean skipAnim = false;
    private void callNextScreen() {
        Intent intent = new Intent(RegistrationActivity.this,WelcomeActivity.class);//SignUpKYCActivity, WelcomeActivity

        //Add transition
        Pair[] pairs = new Pair[4];
        pairs[0] = new Pair<View, String>(signup_back_button, "transition_back_arrow_btn");
        pairs[1] = new Pair<View, String>(signup_title_text, "transition_title_text");
        pairs[2] = new Pair<View, String>(signup_next_button, "transition_next_btn");
        pairs[3] = new Pair<View, String>(signup_login_button, "transition_login_btn");

        if (!skipAnim &&  android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(RegistrationActivity.this, pairs);
            startActivity(intent, options.toBundle());
        } else {
            startActivity(intent);
        }
        finish();//should not come back
    }

    public void callLoginActivity(View view) {
        //Intent intent = new Intent(RegistrationActivity.this, SigninActivity.class);
        //  startActivity(intent);
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.signup_back_button:
                finish();
                break;
            case R.id.img_profile:
                selectImage();
                break;
        }
    }

    private void sendOtp(final String mobile) {
        //LoadingDialog.loadDialog(mContext);

        progressDialog = new ProgressDialog(RegistrationActivity.this);
        progressDialog.setIcon(R.drawable.nextclick_logo_black);
        progressDialog.setMessage("Please wait while sending the otp.....");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setProgress(0);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        Map<String, String> fcmMap = new HashMap<>();
        fcmMap.put("mobile", mobile);
        fcmMap.put("passowrd", password_str);
        JSONObject json = new JSONObject(fcmMap);
        System.out.println("aaaaaaa request "+json.toString());
        final String data = json.toString();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Config.SEND_OTP, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (response != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        System.out.println("aaaaaaa response "+jsonObject.toString());
                        boolean status = jsonObject.getBoolean("status");
                        if (status) {
                            showVerifyOTPDialog(jsonObject.getJSONObject("data").getString("otp"),mobile,true);//null
                        } else {
                            UIMsgs.showToast(mContext, Html.fromHtml(jsonObject.getString("message")).toString());
                        }
                    } catch (Exception e) {
                        UIMsgs.showToast(mContext, "Something went wrong", Toast.LENGTH_SHORT);
                        e.printStackTrace();
                    } finally {
                        progressDialog.dismiss();
                    }
                } else {
                    progressDialog.dismiss();
                    UIMsgs.showToast(mContext, "Server under maintenance", Toast.LENGTH_SHORT);
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                progressDialog.dismiss();
                System.out.println("aaaaaaa error  "+error.getMessage());
                UIMsgs.showToast(mContext, "Something went wrong");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                //  map.put(AUTH_TOKEN, preferenceManager.getString(TOKEN_KEY));
                map.put(APP_ID, APP_ID_VALUE);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    private void callVerifyOTPApi(final String otp,final String mobilenumer) {
        progressDialog.setMessage("Please wait while validating the otp.....");
        progressDialog.show();

        Map<String, String> fcmMap = new HashMap<>();
        fcmMap.put("mobile", mobilenumer);
        fcmMap.put("otp", otp);
        JSONObject json = new JSONObject(fcmMap);
        final String data = json.toString();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Config.VERIFY_OTP, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                try {
                    Log.d("VolleyResponse", "VERIFY_OTP: respnse " + response);

                    JSONObject jsonObject = new JSONObject(response);
                    boolean status = jsonObject.getBoolean("status");
                    if (!status) {
                        UIMsgs.showToast(mContext, Html.fromHtml(jsonObject.getString("message")).toString());
                        showVerifyOTPDialog(otp,mobilenumer,false);
                    } else {
                        isOtpVerified=true;
                        verifiedmobile=mobilenumer;
                        dataSender();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //Toast.makeText(mContext, "responce "+response, Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Log.d("fetch user error", error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(APP_ID, APP_ID_VALUE);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    public void dataSender() {

        progressDialog = new ProgressDialog(RegistrationActivity.this);
        progressDialog.setIcon(R.drawable.nextclick_logo_black);
        progressDialog.setMessage("Please wait while uploading the data.....");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setProgress(0);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();


        Map<String, Object> mainData = new HashMap<>();

        mainData.put("first_name", first_name_str);
        mainData.put("last_name", last_name_str);
        mainData.put("display_name", first_name_str);
        mainData.put("email", email_str);
        mainData.put("phone", mobile_str);
        mainData.put("password", password_str);
        mainData.put("profile_image", ""+profile64);
        mainData.put("primary_intent", Utility.ExecutiveIntent);//Possible Intents: ["user", "delivery_partner", "vendor", "executive"]
       /* Map<String, Map<String, String>> contact = new HashMap<>();
        Map<String, String> one = new HashMap<>();
        one.put("number", mobile_str);
        one.put("code", "+91");
        contact.put("1", one);
        mainData.put("contacts", contact);*/
        ObjectMapper mapperObj = new ObjectMapper();
        try {
            String jsonString = mapperObj.writeValueAsString(mainData);
            registerUser(jsonString, progressDialog);
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(mContext, "Please select less size images", Toast.LENGTH_SHORT).show();
        }
    }


    GenericTextWatcher watcher1,watcher2,watcher3,watcher4,watcher5,watcher6;
    EditText editTextone,editTexttwo,editTextthree, editTextfour,editTextFive,editTextSix;
    private void showVerifyOTPDialog(String otp, final String mobilenumber, boolean isTest) {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.verify_otp_dialog);


        LinearLayout layout_root= (LinearLayout) dialog.findViewById(R.id.layout_root);
        DisplayMetrics displayMetrics = mContext.getResources().getDisplayMetrics();
        ViewGroup.LayoutParams layoutParams = layout_root.getLayoutParams();
        layoutParams.width = displayMetrics.widthPixels-50;
        layout_root.setLayoutParams(layoutParams);

        Button applyButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        ImageView closeButton = (ImageView) dialog.findViewById(R.id.closeButton);
        editTextone = (EditText) dialog.findViewById(R.id.editTextone);
        editTexttwo = (EditText) dialog.findViewById(R.id.editTexttwo);
        editTextthree = (EditText) dialog.findViewById(R.id.editTextthree);
        editTextfour = (EditText) dialog.findViewById(R.id.editTextfour);
        editTextFive = (EditText) dialog.findViewById(R.id.editTextFive);
        editTextSix = (EditText) dialog.findViewById(R.id.editTextSix);
        TextView tv_error = (TextView) dialog.findViewById(R.id.tv_error);
        final TextView tv_timer_resend = (TextView) dialog.findViewById(R.id.tv_timer_resend);
        new CountDownTimer(30000, 1000) {

            public void onTick(long millisUntilFinished) {
                tv_timer_resend.setText("Resends remaining: " + millisUntilFinished / 1000);
            }

            public void onFinish() {
                tv_timer_resend.setText("Resend Otp");
            }

        }.start();
        tv_timer_resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendOtp(mobilenumber);
            }
        });
        if(otp!=null&& !otp.isEmpty() && otp.length() >= 6)
        {
            if(!isTest)
                tv_error.setVisibility(View.VISIBLE);
            editTextone.setText(otp.substring(0, 1));
            editTexttwo.setText(otp.substring(1, 2));
            editTextthree.setText(otp.substring(2, 3));
            editTextfour.setText(otp.substring(3, 4));
            editTextFive.setText(otp.substring(4, 5));
            editTextSix.setText(otp.substring(5, 6));
        }


        watcher1 = new GenericTextWatcher(editTextone);
        watcher2 = new GenericTextWatcher(editTexttwo);
        watcher3 = new GenericTextWatcher(editTextthree);
        watcher4 = new GenericTextWatcher(editTextfour);
        watcher5 = new GenericTextWatcher(editTextFive);
        watcher6 = new GenericTextWatcher(editTextSix);
        editTextone.addTextChangedListener(watcher1);
        editTextone.setOnKeyListener(watcher1);
        editTexttwo.addTextChangedListener(watcher2);
        editTexttwo.setOnKeyListener(watcher2);
        editTextthree.addTextChangedListener(watcher3);
        editTextthree.setOnKeyListener(watcher3);
        editTextfour.addTextChangedListener(watcher4);
        editTextfour.setOnKeyListener(watcher4);
        editTextFive.addTextChangedListener(watcher5);
        editTextFive.setOnKeyListener(watcher5);
        editTextSix.addTextChangedListener(watcher6);
        editTextSix.setOnKeyListener(watcher6);


        applyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String otp = editTextone.getText().toString() + editTexttwo.getText().toString() +
                        editTextthree.getText().toString() + editTextfour.getText().toString() +
                        editTextFive.getText().toString() + editTextSix.getText().toString();

                if (otp != null && !otp.isEmpty() && otp.length() >= 6) {
                    dialog.dismiss();
                    callVerifyOTPApi(otp,mobilenumber);
                }
            }
        });
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }


    public class GenericTextWatcher implements TextWatcher, View.OnKeyListener {
        private View view;
        String previousText = "";

        private GenericTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void afterTextChanged(Editable editable) {
            // TODO Auto-generated method stub
            String text = editable.toString();
            switch (view.getId()) {
                case R.id.editTextone:
                    if (text.length() == 1) {
                        if (previousText.length() > 0) {
                            editTextone.removeTextChangedListener(watcher1);
                            editTextone.setText(previousText);
                            editTextone.addTextChangedListener(watcher1);

                            editTexttwo.removeTextChangedListener(watcher2);
                            editTexttwo.setText(text);
                            editTexttwo.addTextChangedListener(watcher2);
                        }
                        requestFocusAndSelection(editTexttwo,1);
                    }
                    break;
                case R.id.editTexttwo:
                    if (text.length() == 1) {
                        if (previousText.length() > 0) {
                            editTexttwo.removeTextChangedListener(watcher2);
                            editTexttwo.setText(previousText);
                            editTexttwo.addTextChangedListener(watcher2);

                            editTextthree.removeTextChangedListener(watcher3);
                            editTextthree.setText(text);
                            editTextthree.addTextChangedListener(watcher3);

                        }
                        //  editTextthree.requestFocus();
                        requestFocusAndSelection(editTextthree,1);

                    } else if (text.length() == 0) {
                        //  editTextone.requestFocus();
                        requestFocusAndSelection(editTextone, 0);
                    }
                    break;
                case R.id.editTextthree:
                    if (text.length() == 1) {
                        if (previousText.length() > 0) {
                            editTextthree.removeTextChangedListener(watcher3);
                            editTextthree.setText(previousText);
                            editTextthree.addTextChangedListener(watcher3);

                            editTextfour.removeTextChangedListener(watcher4);
                            editTextfour.setText(text);
                            editTextfour.addTextChangedListener(watcher4);
                        }
                        //  editTextfour.requestFocus();
                        requestFocusAndSelection(editTextfour,1);
                    } else if (text.length() == 0) {
                        // editTexttwo.requestFocus();
                        requestFocusAndSelection(editTexttwo, 0);
                    }
                    break;
                case R.id.editTextfour:
                    if (text.length() == 1) {
                        if (previousText.length() > 0) {
                            editTextfour.removeTextChangedListener(watcher4);
                            editTextfour.setText(previousText);
                            editTextfour.addTextChangedListener(watcher4);

                            editTextFive.removeTextChangedListener(watcher5);
                            editTextFive.setText(text);
                            editTextFive.addTextChangedListener(watcher5);
                        }
                        //  editTextFive.requestFocus();
                        requestFocusAndSelection(editTextFive,1);
                    } else if (text.length() == 0) {
                        // editTextthree.requestFocus();
                        requestFocusAndSelection(editTextthree, 0);
                    }
                    break;
                case R.id.editTextFive:
                    if (text.length() == 1) {
                        if (previousText.length() > 0) {
                            editTextFive.removeTextChangedListener(watcher5);
                            editTextFive.setText(previousText);
                            editTextFive.addTextChangedListener(watcher5);

                            editTextSix.removeTextChangedListener(watcher6);
                            editTextSix.setText(text);
                            editTextSix.addTextChangedListener(watcher6);
                        }
                        // editTextSix.requestFocus();
                        requestFocusAndSelection(editTextSix,1);
                    } else if (text.length() == 0) {
                        //editTextfour.requestFocus();
                        requestFocusAndSelection(editTextfour, 0);
                    }
                    break;
                case R.id.editTextSix:
                    if (text.length() == 0) {
                        // editTextFive.requestFocus();
                        requestFocusAndSelection(editTextFive,0);
                    } else {
                        try {
                            final InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                        } catch (Exception e) {
                        }
                    }


                    break;
            }
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            if (arg0.length() > 0) {
                previousText = arg0.toString();
            }
        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
        }

        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            previousText = "";
            if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KEYCODE_DEL) {
                switch (view.getId()) {
                    case R.id.editTexttwo:
                        if (editTexttwo.getText().toString().trim().length() == 0)
                            editTextone.requestFocus();
                        break;
                    case R.id.editTextthree:
                        if (editTextthree.getText().toString().trim().length() == 0)
                            editTexttwo.requestFocus();
                        break;
                    case R.id.editTextfour:
                        if (editTextfour.getText().toString().trim().length() == 0)
                            editTextthree.requestFocus();
                        break;
                    case R.id.editTextFive:
                        if (editTextFive.getText().toString().trim().length() == 0)
                            editTextfour.requestFocus();
                        break;
                    case R.id.editTextSix:
                        if (editTextSix.getText().toString().trim().length() == 0)
                            editTextFive.requestFocus();
                        break;
                }

            }
            return false;
        }
    }

    private void requestFocusAndSelection(EditText et, int i) {
        et.requestFocus();
        if (et.getText().length() > 0)
            et.setSelection(1);
    }


    public void registerUser(final String json, final ProgressDialog progressDialog) {

        final String data = json;
        Log.v("requesr register ",data);
        System.out.println("aaaaaaaaaaa  request regeter  "+json.toString());

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, REGISTER_USER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        try {

                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("aaaaaaaaaa  response register "+jsonObject.toString());
                            Boolean status = jsonObject.getBoolean("status");
                            String http = jsonObject.getString("http_code");
                            String message = jsonObject.getString("message");
                            // String data = jsonObject.getString("data");
                            System.out.println("aaaaaaa responce "+jsonObject);
                            if (status) {
                                // UIMsgs.showToast(mContext, "Account Successfully Created");
                                UIMsgs.showToast(mContext, "User Created Successfully");
                                getUserToken();
                                //callNextScreen();
                            } else {
                                try {
                                    if (message!=null && message.equals("DUPLICATE_IDENTITY")) {
                                        UIMsgs.showToast(mContext, "It looks like you already have an account with NextClick, please proceed with the login");
                                    }
                                    else
                                        UIMsgs.showToast(mContext, message);
                                }
                                catch (Exception ex) {
                                    UIMsgs.showToast(mContext, message);
                                }
                                Log.d("er msg", Html.fromHtml(message).toString());
                                progressDialog.dismiss();
                            }

                        } catch (Exception e) {
                            System.out.println("aaaaaaaa  catchh "+e.getMessage());
                            UIMsgs.showToast(mContext,e.getMessage());
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("aaaaaaaa  catchh "+error.getMessage());
                //Toast.makeText(LoginActivity.this, error+"", Toast.LENGTH_SHORT).show();
                UIMsgs.showToast(mContext, "Some Error Occured.. Please provide data again...");
                progressDialog.dismiss();
            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                return map;
            }

            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void getUserToken() {
        Map<String, String> dataMap = new HashMap<>();
        dataMap.put("identity", mobile_str);
        dataMap.put("password", password_str);
        dataMap.put("intent", Utility.ExecutiveIntent);//Possible Intents: ["user", "delivery_partner", "vendor", "executive"]
        JSONObject json = new JSONObject(dataMap);
        login(json);
    }

    private void login(JSONObject json) {
        mCustomDialog.show();
        final String data = json.toString();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.Login, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    Log.d("login_res", response);
                    try {
                        mCustomDialog.dismiss();
                        JSONObject jsonObject = new JSONObject(response);
                        boolean status = jsonObject.getBoolean("status");
                        if (status) {
                            JSONObject dataObject = jsonObject.getJSONObject("data");
                            String token = dataObject.getString("token");
                            try {
                                preferenceManager.putString(USER_TOKEN, token);
                                callNextScreen();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(mContext, Html.fromHtml(jsonObject.getString("data")), Toast.LENGTH_SHORT).show();
                            callLoginActivity(null);
                        }
                    }catch (Exception e) {
                        mCustomDialog.dismiss();
                        UIMsgs.showToast(mContext, "Something went wrong");
                        e.printStackTrace();
                        callLoginActivity(null);
                    }
                } else {
                    mCustomDialog.dismiss();
                    UIMsgs.showToast(mContext, "Server under maintenance");
                    callLoginActivity(null);
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mCustomDialog.dismiss();
                UIMsgs.showToast(mContext, "Something went wrong");
                callLoginActivity(null);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(APP_ID, APP_ID_VALUE);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    private IncomingSms receiver = new IncomingSms() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase("otp")) {
                final String message = intent.getStringExtra("message");
                System.out.println("aaaaaaaaa  message  "+message);
                getOtpFromMessage(message);
            }
        }
    };
    private void getOtpFromMessage(String message) {
        // This will match any 6 digit number in the message
        Pattern pattern = Pattern.compile("(|^)\\d{6}");
        Matcher matcher = pattern.matcher(message);
        if (matcher.find()) {
            System.out.println("aaaaaaa  otp "+matcher.group(0));
            String otp=matcher.group(0);
            if(editTextone!=null) {
                editTextone.setText("" + otp.charAt(0));
                editTexttwo.setText("" + otp.charAt(1));
                editTextthree.setText("" + otp.charAt(2));
                editTextfour.setText("" + otp.charAt(3));
                editTextFive.setText("" + otp.charAt(4));
                editTextSix.setText("" + otp.charAt(5));
            }
        }
    }


    private ArrayList findUnAskedPermissions(ArrayList wanted) {
        ArrayList result = new ArrayList();
        for (Object perm : wanted) {
            if (!hasPermission((String) perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    private boolean hasPermission(String permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    } private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    /*@Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter("otp"));
    }
    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }*/


    private boolean selectImage() {
        final CharSequence[] items = {"Choose from Library", "Open Camera",
                "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(RegistrationActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.M)

            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";
                    boolean result = Utility.checkPermission(RegistrationActivity.this);
                    if (result) {    //Calling Gallery Marhod For Images
                        galleryIntent();
                    }

                } else if (items[item].equals("Open Camera")) {
                    userChoosenTask = "Open Camera";
                    if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
                    } else {
                        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, CAMERA_REQUEST);
                    }

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
        return true;
    }

    //Calling Openig Gallery For Images
    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
        }
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            img_profile.setImageBitmap(photo);

            Bitmap bitmap = ((BitmapDrawable) img_profile.getDrawable()).getBitmap();

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

            byte[] byteArray = baos.toByteArray();
            profile64 = Base64.encodeToString(byteArray, Base64.DEFAULT);

        }

    }


    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
               /* Toast.makeText(mContext, data.getData() + "", Toast.LENGTH_SHORT).show();
                Log.d("Data", data.getData().toString());*/

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        bm = Bitmap.createScaledBitmap(bm, 512, 512, false);

        img_profile.setImageBitmap(bm);

        Bitmap bitmap = ((BitmapDrawable) img_profile.getDrawable()).getBitmap();

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

        byte[] byteArray = baos.toByteArray();
        profile64 = Base64.encodeToString(byteArray, Base64.DEFAULT);
    }

}