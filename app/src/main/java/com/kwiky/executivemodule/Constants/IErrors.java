package com.kwiky.executivemodule.Constants;

public interface IErrors {

    String EMPTY = "empty field not allowed";
    String NAME_EMPTY="Please enter Name !!";
    String INVALID = "Invalid";
    String ERROR_LISTENER = "error message";
    String ERROR_CHECK_INTERNET = "please check internet";
    String PASSWORD = "Password minimum 6 digit & no special symbols";
    String INVALID_EMAIL = "invalid email";
    String INVALID_PHONE = "invalid mobile number";
    String EMPTY_PINCODE = "Please enter pincode";

    String MOBILE_Email_EMPTY = "Please enter Mobile Number or Email !!";
    String MOBILE_EMPTY = "Please enter Mobile Number !!";
    String MOBILE_INVALID = "Please enter valid Mobile Number !!";

    String PASSWORD_EMPTY = "Please enter Password !!";

    String PASSWORD_INVALID = "Please enter valid Password !!";
    String PASSWORD_SAME = "Please enter same Passwords !!";

    String FULLNAME_EMPTY = "Please enter Full Name !!";
    String FULLNAME_INVALID = "Name must be greater than three !!";

    String EMAIL_EMPTY = "Please enter E-Mail !!";
    String EMAIL_INVALID = "Please enter valid E-Mail !!";

    String ADDRESS_EMPTY = "Please enter Address !!";
    String City_EMPTY = "Please enter  City Name !!";
    String State_EMPTY = "Please enter Name  !!";
    String Country_EMPTY = "Please enter Country Name !!";
    String ChekBox_EMPTY = "Please accept terms and Conditions !!";
    String Zip_EMPTY = "Please enter Zip Code !!";
    String Company_EMPTY = "Please enter Company Name !!";
    String INTERNET_CONNECT = "Please connect to the Internet !!";

    String DATE_INVALID = "Please select proper Date !!";

    String PHOTO_ID_EMPTY = "Please select Photo Id !!";

    String PHOTO_EMPTY = "Please select photo !!";

    String HOUSE_NAME_EMPTY = "Please enter House Name !!";
    String FLAT_NO_EMPTY = "Please enter Flat number !!";
    String STREET_NAME_EMPTY = "Please enter Street Name !!";
    String PIN_EMPTY = "Please enter Pincode !!";
    String PIN_INVALID ="Please enter valid Pincode !!";
    String SELECT_LOCATION="Please select Location";

    String OLD_PASSWORD_EMPTY="Please enter Old Password";
    String NEW_PASSWORD_EMPTY="Please enter new Password";
    String CONFIRM_PASSWORD_EMPTY="Please confirm Password";
    String CONFIRM_PASSWORD_MISMATCH="Confirm password not match";

    String GST_INVALID = "Please Enter Valid GST";
    String IECode="Please Enter Valid IE Code";
    String TIMEINAVLID ="Please select valid time";


}
