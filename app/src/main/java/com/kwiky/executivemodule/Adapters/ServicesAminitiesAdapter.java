package com.kwiky.executivemodule.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.kwiky.executivemodule.PojoClass.ServiceAminityModelData;
import com.kwiky.executivemodule.R;

import java.util.List;

public class ServicesAminitiesAdapter extends RecyclerView.Adapter<ServicesAminitiesAdapter.ViewHolder> {
    Context applicationContext;
    List<ServiceAminityModelData> serviceAminitesList;

    public ServicesAminitiesAdapter(Context applicationContext, List<ServiceAminityModelData> serviceAminitesList) {
        this.applicationContext = applicationContext;
        this.serviceAminitesList = serviceAminitesList;
    }

    @NonNull
    @Override
    public ServicesAminitiesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(applicationContext).inflate(R.layout.acitivtes_aminites_recycle, parent, false);
        //return new HomeFragmentAdapter.Business_head_list (itemView);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ServicesAminitiesAdapter.ViewHolder holder, int position) {
        ServiceAminityModelData serviceAminityModelData = serviceAminitesList.get(position);

        holder.textAmenity.setText(serviceAminityModelData.getName());

    }

    @Override
    public int getItemCount() {
        return serviceAminitesList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CheckBox checkAminites;
        TextView textAmenity;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            checkAminites = itemView.findViewById(R.id.check_aminites);
            textAmenity = itemView.findViewById(R.id.text_amenity);


        }
    }
}
