package com.kwiky.executivemodule.Adapters;

import static android.widget.LinearLayout.HORIZONTAL;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.kwiky.executivemodule.Config.SubCategorySelection;
import com.kwiky.executivemodule.R;
import com.kwiky.executivemodule.models.CategoryObject;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.kwiky.executivemodule.Config.Config.CATEGORY_LIST;


public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {
    private SubCategorySelection registerVendorActivity;

    private ArrayList<CategoryObject> categories;
    private Context context;
    Integer selectedIndex=-1;
    TextView tv_no_categories;
    private ArrayList<String> selectedCategoryIDs,selectedCategories;
    public CategoryAdapter(Context mContext, ArrayList<CategoryObject> categories,
                           RecyclerView recyclerView_subcategories, TextView tv_no_categories, TextView tv_no_sub_categories,
                           ArrayList<String> selectedCategoryIDs,ArrayList<String> selectedCategories,
                           SubCategorySelection registerVendorActivity) {
        this.context = mContext;
        this.categories = categories;
       // this.recyclerView_subcategories = recyclerView_subcategories;
        this.tv_no_categories=tv_no_categories;
        //this.tv_no_sub_categories=tv_no_sub_categories;
        this.registerVendorActivity = registerVendorActivity;
        this.selectedCategoryIDs=new ArrayList<>();//selectedCategoryIDs;
        this.selectedCategories=selectedCategories;
    }

    @Override
    public CategoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.category_layout_new, parent, false);
        return new CategoryAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final CategoryAdapter.ViewHolder holder, final int position) {
        /*if(position ==0)
        {
            holder.layout_category.setVisibility(View.GONE);
            return;
        }*/
        //int position = holder.getAdapterPosition();

        holder.layout_sub_category.setVisibility(View.GONE);
        holder.layout_category.setVisibility(View.VISIBLE);
        /*if (position == selectedIndex) {
            holder.layout_category.setBackgroundColor(context.getResources().getColor(R.color.orange));
            holder.tv_category_name.setTextColor(Color.WHITE);
        }
        else {
            holder.layout_category.setBackgroundColor(Color.WHITE);
            holder.tv_category_name.setTextColor(Color.BLACK);
        }*/

        if (position == selectedIndex) {
            getSubCategories(holder, categories.get(position).getCatID());
        }

        holder.layout_category.setBackgroundColor(Color.WHITE);
        holder.tv_category_name.setTextColor(Color.BLACK);

        holder.tv_category_name.setText(categories.get(position).getCatName());

        if (!categories.get(position).getCatImage().isEmpty()) {

            Glide.with(context)
                    .load(categories.get(position).getCatImage())
                    .placeholder(R.drawable.no_image)
                    .into(holder.img_category);
        }

        holder.layout_category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedIndex=position;
                registerVendorActivity.setCategory(categories.get(position));
                notifyDataSetChanged();
            }
        });
    }

    private void getSubCategories(final CategoryAdapter.ViewHolder holder, final String categoryID) {

        holder.layout_sub_category.setVisibility(View.VISIBLE);
        final ArrayList<CategoryObject> subcategoriesList = new ArrayList<>();

        String tempCategories = CATEGORY_LIST + categoryID;

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, tempCategories, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonData = new JSONObject(response);
                    Boolean type;
                    String status = jsonData.getString("status");
                    String messagae = jsonData.getString("message");
                    String datatype = jsonData.get("data").getClass().getName();
                    if (datatype.equalsIgnoreCase("java.lang.Boolean")) {
                        type = false;
                    } else
                        type = true;
                    if (type) {
                        if (status.equalsIgnoreCase("true")) {
                            JSONObject jsonObject = jsonData.getJSONObject("data");
                            try {
                                JSONArray subCategoryJsonArray = jsonObject.getJSONArray("sub_categories");
                                if (subCategoryJsonArray.length() > 0) {
                                    for (int i = 0; i < subCategoryJsonArray.length(); i++) {
                                        JSONObject subCategoryObject = (JSONObject) subCategoryJsonArray.get(i);
                                        CategoryObject c =new CategoryObject();
                                        c.setCatName(subCategoryObject.getString("name"));
                                        c.setCatID(subCategoryObject.getString("id"));
                                        if(subCategoryObject.has("image"))
                                            c.setCatImage(subCategoryObject.getString("image"));
                                        subcategoriesList.add(c);
                                      /*  for (int j = 0; j <20; j++) {

                                        }*/
                                    }
                                }
                            } catch (Exception ex) {
                            }

                            if(subcategoriesList!=null && subcategoriesList.size()>0) {
                                selectedCategoryIDs.clear();//for one sub category ;otherwise no need
                                selectedCategories.clear();
                                SubCategoryAdapter categoryAdapter = new SubCategoryAdapter(context, categoryID, subcategoriesList,
                                        selectedCategoryIDs,selectedCategories,registerVendorActivity);
                                holder.recyclerView_subcategories.setAdapter(categoryAdapter);
                                holder.tv_no_sub_categories.setVisibility(View.GONE);


                                holder.recyclerView_subcategories.setVisibility(View.VISIBLE);
                            }
                            else
                            {
                                holder.tv_no_sub_categories.setVisibility(View.VISIBLE);
                                holder.recyclerView_subcategories.setVisibility(View.GONE);
                            }
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                //map.put("X_AUTH_TOKEN", token);

                return map;
            }
        };

        requestQueue.add(stringRequest);

    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tv_category_name,tv_no_sub_categories;
        LinearLayout layout_category,layout_sub_category;
        ImageView img_category;
        private RecyclerView recyclerView_subcategories;

        public ViewHolder(View itemView) {
            super(itemView);
            layout_category= itemView.findViewById(R.id.layout_category);
            layout_sub_category= itemView.findViewById(R.id.layout_sub_category);
            tv_category_name= itemView.findViewById(R.id.tv_category_name);
            img_category= itemView.findViewById(R.id.img_category);
            recyclerView_subcategories= itemView.findViewById(R.id.recyclerView_subcategories);


            tv_no_sub_categories=itemView.findViewById(R.id.tv_no_sub_categories);
            LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(context);
            linearLayoutManager1.setOrientation(LinearLayoutManager.VERTICAL);
            recyclerView_subcategories.setLayoutManager(linearLayoutManager1);

            /*DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView_subcategories.getContext(),
                    linearLayoutManager1.getOrientation());
            recyclerView_subcategories.addItemDecoration(dividerItemDecoration);*/
        }
    }
}
