package com.kwiky.executivemodule.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.kwiky.executivemodule.PojoClass.VendorsPojo;
import com.kwiky.executivemodule.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class VendorsAdapter extends RecyclerView.Adapter<VendorsAdapter.ViewHolder>   {

    List<VendorsPojo> data;

    Context context;

    public VendorsAdapter(Context activity,ArrayList<VendorsPojo> vendorsList) {
        this.context = activity;
        this.data = vendorsList;

    }


    @NonNull
    @Override
    public VendorsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.vendorlistchange, parent, false);
        //return new HostelRecentlyAdapter.Business_head_list (itemView);
        return new VendorsAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull VendorsAdapter.ViewHolder holder, int position) {
        VendorsPojo vendorsPojo = data.get(position);

        holder.vendor_name.setText(vendorsPojo.getName());
        holder.customer_address.setText(vendorsPojo.getAddress());
        holder.vendor_landmark.setText(vendorsPojo.getShop_address());//getLandmark
        holder.vendor_email.setText(vendorsPojo.getEmail());
       /* DecimalFormat df = new DecimalFormat("#.#");
        df.setRoundingMode(RoundingMode.CEILING);
        holder.vendor_distance.setText(df.format(vendorsPojo.getDistance())+" KM away");*/
        Glide.with(context)
                .load(vendorsPojo.getImage())
                .placeholder(R.drawable.no_image)
                .into(holder.vendor_image);


        if(vendorsPojo.getVendorstatus().equalsIgnoreCase("2")){
            holder.vendor_status.setText("Pending");
            holder.vendor_status_card.setCardBackgroundColor(Color.parseColor("#ff0000"));
        }else if (vendorsPojo.getVendorstatus().equalsIgnoreCase("1")){
            holder.vendor_status.setText("Approved");
            holder.vendor_status_card.setCardBackgroundColor(Color.parseColor("#008000"));
        }else{
            holder.vendor_status.setText("Approved");
            holder.vendor_status_card.setCardBackgroundColor(Color.parseColor("#ff0000"));
        }

       /* holder.vendor_view_shop_card.setVisibility(View.INVISIBLE);
        holder.vendor_view_book_card.setVisibility(View.INVISIBLE);
        holder.vendor_view_ondemand_card.setVisibility(View.INVISIBLE);
        holder.layout_getcall.setVisibility(View.INVISIBLE);
        holder.layout_ondemand.setVisibility(View.GONE);
        holder.vendor_view_plus_card.setVisibility(View.GONE);*/
       /* boolean is_having_lead = Boolean.parseBoolean(data.get(position).getIs_having_lead());
        if (!is_having_lead){
            holder.layout_getcall.setVisibility(View.INVISIBLE);
        }else {
            holder.layout_getcall.setVisibility(View.VISIBLE);
        }*/

        String currentString = vendorsPojo.getCreated_at();
        String[] separated = currentString.split(" ");
        String date=separated[0];
        String time=separated[1];

        String[] datespilt=date.split("-");
        int datest=Integer.parseInt(datespilt[2]);
        int month=Integer.parseInt(datespilt[1]);
        int year=Integer.parseInt(datespilt[0]);

        String[] timespilt=time.split(":");
        String hour=timespilt[0];
        String minitu=timespilt[1];
        String sec=timespilt[2];

        Calendar calendar=Calendar.getInstance();

        calendar.set(year, month, datest,
                Integer.parseInt(hour), Integer.parseInt(minitu), Integer.parseInt(sec));

        System.out.println("aaaaaa time "+datest+" "+month+" "+year+" "+hour+" "+minitu+" "+sec+"  "+calendar.get(Calendar.MONTH));

        String text= getFormattedDate(calendar.getTimeInMillis());
        System.out.println("aaaaaaa text "+text);
        holder.tv_cerate.setText(""+text.trim());

    }
    public String getFormattedDate(long smsTimeInMilis) {
        Calendar smsTime = Calendar.getInstance();
        smsTime.setTimeInMillis(smsTimeInMilis);

        Calendar now = Calendar.getInstance();

        final String timeFormatString = "h:mm aa";
        final String dateTimeFormatString = "EEE, MMM d, h:mm aa";
        final long HOURS = 60 * 60 * 60;
        System.out.println("aaaaaaaa check "+now.get(Calendar.DATE) +"  "+smsTime.get(Calendar.DATE));

        if (now.get(Calendar.DATE) == smsTime.get(Calendar.DATE) ) {
            return "Today " + DateFormat.format(timeFormatString, smsTime);
            // return "Today \n" + DateFormat.format(timeFormatString, smsTime);
        } else if (now.get(Calendar.DATE) - smsTime.get(Calendar.DATE) == 1  ){
            return "Yesterday " + DateFormat.format(timeFormatString, smsTime);
        } else if (now.get(Calendar.YEAR) == smsTime.get(Calendar.YEAR)) {
            smsTime.set(Calendar.MONTH,smsTime.get(Calendar.MONTH)-1);
            return DateFormat.format(dateTimeFormatString+"\n", smsTime).toString();
        } else {
            return DateFormat.format("MMM dd yyyy, h:mm aa", smsTime).toString();
        }
    }
    @Override
    public int getItemCount() {
       // return data.size();
        return data.size();
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public int getItemViewType(int position) {return position;}

    public void setchenge(ArrayList<VendorsPojo> totalvendorlist) {
        this.data=totalvendorlist;
        notifyDataSetChanged();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView vendor_name, customer_address,vendor_landmark,vendor_email,
                vendor_status,tv_cerate;
        ImageView vendor_image;
        LinearLayout item_layout;
        CardView vendor_status_card;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            item_layout = itemView.findViewById(R.id.item_layout);
            vendor_image = itemView.findViewById(R.id.vendor_image);
            vendor_name = itemView.findViewById(R.id.vendor_name);
            customer_address = itemView.findViewById(R.id.customer_address);
            vendor_landmark = itemView.findViewById(R.id.vendor_landmark);
            vendor_email = itemView.findViewById(R.id.vendor_email);
            vendor_status = itemView.findViewById(R.id.vendor_status);
            vendor_status_card = itemView.findViewById(R.id.vendor_status_card);
            tv_cerate = itemView.findViewById(R.id.tv_cerate);

        }
    }
}
