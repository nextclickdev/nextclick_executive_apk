package com.kwiky.executivemodule.viewModels;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;


import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Chanchal Kishore on 17/07/19.
 */

public class LoginVm {}/*{
    private static final String TAG = "Volley";
    private ActivityMainBinding loginBinding;
    private Context context;
    private String emailOrMobileNum;
    private VolleyHelper volleyHelper;
    String email;
    CustomEditText emailEdit, newPasswordEdit;


    public LoginVm(Context mContext, ActivityMainBinding loginBinding) {
        this.loginBinding = loginBinding;
        this.context = mContext;
        volleyHelper = new VolleyHelper(context);
        setInitialState();
    }

    private void setInitialState() {

    }

    public void loginClick(View v) {
        if (loginBinding.email.getText().length() == 0) {
            UImsgs.showSnackBar(v, context.getString(R.string.enter_proper_email_ph));
        } else if (loginBinding.password.getText().length() < 5) {
            UImsgs.showSnackBar(v, context.getString(R.string.enter_proper_password));
        } else {
            try {
                JSONObject jsonObject = new JSONObject();
                emailOrMobileNum = loginBinding.email.getText().toString();

                jsonObject.put("email", emailOrMobileNum);
                jsonObject.put("password", loginBinding.password.getText().toString());
                //Toast.makeText(context, "Toast", Toast.LENGTH_SHORT).show();
                //Toast.makeText(context, String.valueOf(jsonObject), Toast.LENGTH_SHORT).show();
                //volleyHelper.signIn(jsonObject);
                //volleyHelper.signIn(emailOrMobileNum,loginBinding.password.getText().toString());
                //signIn(jsonObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void signIn(JSONObject jsonObject) {
        JsonObjectRequest jsonRequest = new JsonObjectRequest( Constants.CUSTOM_SIGNIN_URL, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject loginResponse) {
                        Log.d(TAG, loginResponse.toString());
                        try {
                            JSONObject jsonObject = loginResponse.getJSONObject("status");

                           *//* mPreferenceManager.putString("firstname", jsonObject.getString("firstname"));
                            mPreferenceManager.putString("lastname", jsonObject.getString("lastname"));
                            mPreferenceManager.putString("mobile", jsonObject.getString("mobile"));
                            mPreferenceManager.putString("email", jsonObject.getString("email"));
                            mPreferenceManager.putString("consumer_id", jsonObject.getString("id"));
                            mPreferenceManager.putString("referal_code", jsonObject.getString("referal_code"));*//*


                            //pushFcmToken();

                            EventBus.getDefault().post(new LoginEvent(true, loginResponse.getString("message"), loginResponse));
                        } catch (JSONException e) {
                            e.printStackTrace();

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Error handling
                Log.d(TAG, error.toString());
                error.printStackTrace();
                JSONObject errorJson = null;
                // As of f605da3 the following should work
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        // Now you can use any deserializer to make sense of data
                        errorJson = new JSONObject(res);
                        Log.d(TAG, "onErrorResponse: " + errorJson);
                        // Toast.makeText(mContext, errorJson.getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (UnsupportedEncodingException | JSONException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    }
                }
                if (error.networkResponse != null) {
                    try {
                        if (errorJson != null){
                            EventBus.getDefault().post(new LoginEvent(false, error.networkResponse.statusCode, errorJson.getString("message")));
                        }
                        else{
                            EventBus.getDefault().post(new LoginEvent(false, error.networkResponse.statusCode, ""));
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    EventBus.getDefault().post(new LoginEvent(false, 504, ""));
                }
            }
        }) {
            @Override
            public Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                // params.put("Authorization", getAuthHeader());
                return params;
            }
        };
        jsonRequest.setShouldCache(false);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(10000, 2, 2));
        CustomerServicesApp.getInstance().addToRequestQueue(jsonRequest);
    }

    public void signUpClick(View view) {
        //context.startActivity(new Intent(context, RegisterActivity.class));
    }

    public void forgetPassword(View view) {
        payFromWalletPopup();
    }

    private void payFromWalletPopup() {
        *//*final Dialog fogetPasswordDialog = new Dialog(context);
        fogetPasswordDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        fogetPasswordDialog.setContentView(R.layout.forget_password_popup);
        fogetPasswordDialog.getWindow().getAttributes().windowAnimations = R.style.dialog_animation;

        emailEdit = (CustomEditText) fogetPasswordDialog.findViewById(R.id.email_mobile_edit);
        Button submitBtn = (Button) fogetPasswordDialog.findViewById(R.id.submit_password);
        Button cancelBtn = (Button) fogetPasswordDialog.findViewById(R.id.cancel_btn);


        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fogetPasswordDialog.dismiss();
            }
        });
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                email = emailEdit.getText().toString();
                if (email.length() == 0) {
                    UImsgs.showSnackBar(v, context.getString(R.string.enter_proper_email_ph));
                } else {
                    JSONObject json = new JSONObject();
                    try {
                        json.put("email_id", email);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    volleyHelper.forgetPassword(json);
                    fogetPasswordDialog.dismiss();
                }
            }
        });
        fogetPasswordDialog.show();*//*
    }

}*/
