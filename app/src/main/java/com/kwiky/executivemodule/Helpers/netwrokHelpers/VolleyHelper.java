package com.kwiky.executivemodule.Helpers.netwrokHelpers;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import com.kwiky.executivemodule.Constants.Constants;
import com.kwiky.executivemodule.utilities.PreferenceManager;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;



/**
 * Created by Chanchal Kishore on 10/07/19.
 */

public class VolleyHelper /*implements StringConstants*/ {
    private static final String TAG = "Volley";
    private Context mContext;
    private PreferenceManager mPreferenceManager;

    public VolleyHelper(Context context) {
        this.mContext = context;
        mPreferenceManager = new PreferenceManager(context);

    }

    public void getSplashScreen() {}/*{
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Constants.BASE_URL_TEMP + Constants.CUSTOMER_SPLASH_SCREEN, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject splashResponse) {
                        Log.d(TAG, splashResponse.toString());
                        try {
                            EventBus.getDefault().post(new SplashEvent(true, splashResponse.getString("data"), splashResponse));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Error handling
                Log.d(TAG, error.toString());
                error.printStackTrace();
                JSONObject errorJson = null;
                // As of f605da3 the following should work
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        // Now you can use any deserializer to make sense of data
                        errorJson = new JSONObject(res);
                        Log.d(TAG, "onErrorResponse: " + errorJson);
                        // Toast.makeText(mContext, errorJson.getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (UnsupportedEncodingException | JSONException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    }
                }

                if (error.networkResponse != null) {
                    try {
                        if (errorJson != null)
                            EventBus.getDefault().post(new SplashEvent(false, error.networkResponse.statusCode, errorJson.getString("message")));
                        else
                            EventBus.getDefault().post(new SplashEvent(false, error.networkResponse.statusCode, ""));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    EventBus.getDefault().post(new SplashEvent(false, 504, ""));
                }


            }
        }) {
            @Override
            public Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                // params.put("Authorization", getAuthHeader());
                return params;
            }
        };
        jsonRequest.setShouldCache(false);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(10000, 2, 2));
        ExecutiveServicesApp.getInstance().addToRequestQueue(jsonRequest);
        //requstQueue.add(jsonRequest);
    }*/

    public void signIn(JSONObject loginJson) {}/*{
        Log.d(TAG, "SignIN: API" +  Constants.CUSTOM_SIGNIN_URL + " " + loginJson);
        JsonObjectRequest jsonRequest = new JsonObjectRequest( Constants.CUSTOM_SIGNIN_URL, loginJson,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject loginResponse) {

                        Log.d(TAG, loginResponse.toString());
                        try {
                            JSONObject jsonObject = loginResponse.getJSONObject("status");

                            mPreferenceManager.putString("firstname", jsonObject.getString("firstname"));
                            mPreferenceManager.putString("lastname", jsonObject.getString("lastname"));
                            mPreferenceManager.putString("mobile", jsonObject.getString("mobile"));
                            mPreferenceManager.putString("email", jsonObject.getString("email"));
                            mPreferenceManager.putString("consumer_id", jsonObject.getString("id"));
                            mPreferenceManager.putString("referal_code", jsonObject.getString("referal_code"));


                            //pushFcmToken();

                            EventBus.getDefault().post(new LoginEvent(true, loginResponse.getString("status"), loginResponse));
                        } catch (JSONException e) {
                            e.printStackTrace();

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Error handling
                Log.d(TAG, error.toString());
                error.printStackTrace();
                JSONObject errorJson = null;
                // As of f605da3 the following should work
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        // Now you can use any deserializer to make sense of data
                        errorJson = new JSONObject(res);
                        Log.d(TAG, "onErrorResponse: " + errorJson);
                        // Toast.makeText(mContext, errorJson.getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (UnsupportedEncodingException | JSONException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    }
                }
                if (error.networkResponse != null) {
                    try {
                        if (errorJson != null){
                            EventBus.getDefault().post(new LoginEvent(false, error.networkResponse.statusCode, errorJson.getString("message")));
                        }
                        else{
                            EventBus.getDefault().post(new LoginEvent(false, error.networkResponse.statusCode, ""));
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    EventBus.getDefault().post(new LoginEvent(false, 504, ""));
                }
            }
        }) {
            @Override
            public Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                // params.put("Authorization", getAuthHeader());
                return params;
            }
        };
        jsonRequest.setShouldCache(false);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(10000, 2, 2));
        ExecutiveServicesApp.getInstance().addToRequestQueue(jsonRequest);
    }*/


    public void signIn(final String str_email, final String str_password) {}/*{
        RequestQueue requstQueue = Volley.newRequestQueue(mContext);

        StringRequest jsonobj = new StringRequest(Request.Method.POST, Constants.CUSTOM_SIGNIN_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        mPreferenceManager.putString("UserId", "1");

                        *//*Intent intent = new Intent( MainActivity.this,DashBoardActivity.class);
                        startActivity(intent);*//*
                        Log.d(TAG, response.toString());
                       *//* try {
                            JSONObject jsonObject = response.getJSONObject("status");
*//*
                           *//* mPreferenceManager.putString("firstname", jsonObject.getString("firstname"));
                            mPreferenceManager.putString("lastname", jsonObject.getString("lastname"));
                            mPreferenceManager.putString("mobile", jsonObject.getString("mobile"));
                            mPreferenceManager.putString("email", jsonObject.getString("email"));
                            mPreferenceManager.putString("consumer_id", jsonObject.getString("id"));
                            mPreferenceManager.putString("referal_code", jsonObject.getString("referal_code"));*//*


                            //pushFcmToken();

                            EventBus.getDefault().post(new LoginEvent(true, "status", response));
  *//*                      } catch (JSONException e) {
                            e.printStackTrace();

                        }
  *//*

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Toast.makeText(LoginActivity.this, "Please Check Email Id and Password", Toast.LENGTH_SHORT).show();
                      *//*  Snackbar.make(view, "IConstant.someErrorOccured", Snackbar.LENGTH_LONG)
                                .setAction("OK", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        //RequestQuotation(v);
                                    }
                                }).show();*//*
//                        Toast.makeText(LoginActivity.this, "Some Error Occured", Toast.LENGTH_SHORT).show();
                        //btn_login.setVisibility(View.VISIBLE);
                        //progressBar.setVisibility(View.INVISIBLE);
                    }
                }

        ){

            protected Map<String, String> getParams() {


                Map<String, String> params = new HashMap<String, String>();
                params.put("email", str_email);
                params.put("password",str_password);
                return params;

            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("email",str_email);
                params.put("password",str_password);
                return params;
            }

            //here I want to post data to sever
        };
        requstQueue.add(jsonobj);
        ExecutiveServicesApp.getInstance().addToRequestQueue(jsonobj);
    }*/


    public  void register(JSONObject registerJson){}/*{
        Log.d(TAG, "bookACab: API POST" + CUSTOMER_REGISTRATION + " " + registerJson);
        *//*JsonObjectRequest jsonRequest = new JsonObjectRequest(CUSTOMER_REGISTRATION, registerJson,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject loginResponse) {
                        Log.d(TAG, loginResponse.toString());
                        try {
                            if (loginResponse.getBoolean("status"))
                                EventBus.getDefault().post(new RideNowEvent(true, 200, loginResponse.getString("message"), loginResponse.getJSONObject("data").getString("request_id")));
                            else
                                EventBus.getDefault().post(new RideNowEvent(false, 200, loginResponse.getString("message")));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Error handling
                Log.d(TAG, error.toString());
                error.printStackTrace();
                JSONObject errorJson = null;
                // As of f605da3 the following should work
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        // Now you can use any deserializer to make sense of data
                        errorJson = new JSONObject(res);
                        Log.d(TAG, "onErrorResponse: " + errorJson);
                        // Toast.makeText(mContext, errorJson.getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (UnsupportedEncodingException | JSONException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    }
                }
                if (error.networkResponse != null) {
                    try {
                        if (errorJson != null)
                            EventBus.getDefault().post(new RideNowEvent(false, error.networkResponse.statusCode, errorJson.getString("message")));
                        else
                            EventBus.getDefault().post(new RideNowEvent(false, error.networkResponse.statusCode, ""));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    EventBus.getDefault().post(new LoginEvent(false, 504, ""));
                }
            }
        }) {
            @Override
            public Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                // params.put("Authorization", getAuthHeader());
                return params;
            }
        };
        jsonRequest.setShouldCache(false);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(10000, 2, 2));
        ExecutiveServicesApp.getInstance().addToRequestQueue(jsonRequest);*//*
    }*/

    public void getVersion() {}/*{
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Constants.VERSION_URL , null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject splashResponse) {
                        Log.d(TAG, splashResponse.toString());
                        try {
                            EventBus.getDefault().post(new VersionEvent(true, splashResponse.getString("status"), splashResponse));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Error handling
                Log.d(TAG, error.toString());
                error.printStackTrace();
                JSONObject errorJson = null;
                // As of f605da3 the following should work
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        // Now you can use any deserializer to make sense of data
                        errorJson = new JSONObject(res);
                        Log.d(TAG, "onErrorResponse: " + errorJson);
                        // Toast.makeText(mContext, errorJson.getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (UnsupportedEncodingException | JSONException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    }
                }

                if (error.networkResponse != null) {
                    try {
                        if (errorJson != null)
                            EventBus.getDefault().post(new VersionEvent(false, error.networkResponse.statusCode, errorJson.getString("message")));
                        else
                            EventBus.getDefault().post(new VersionEvent(false, error.networkResponse.statusCode, ""));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    EventBus.getDefault().post(new VersionEvent(false, 504, ""));
                }


            }
        }) {
            @Override
            public Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("Content-Type", "json");applicat getAuthHeaderion/
                // params.put("Authorization",());
                return params;
            }
        };
        jsonRequest.setShouldCache(false);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(10000, 2, 2));
        ExecutiveServicesApp.getInstance().addToRequestQueue(jsonRequest);
        //requstQueue.add(jsonRequest);
    }*/

    public void getUserProfile() {}/*{
        String user_id = mPreferenceManager.getString("UserId");
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Constants.CUSTOMER_PROFILE+user_id , null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject profileResponce) {
                        Log.d(TAG, profileResponce.toString());
                        try {
                            String status = profileResponce.getString("status");
                            //EventBus.getDefault().post(new SplashEvent(true, "error.networkResponse.statusCode",profileResponce));
                            EventBus.getDefault().post(new ProfileEvent(true, profileResponce.getString("message"), profileResponce));

                        } catch (JSONException e) {

                        }
                    }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Error handling
                Log.d(TAG, error.toString());
                error.printStackTrace();
                JSONObject errorJson = null;
                // As of f605da3 the following should work
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        // Now you can use any deserializer to make sense of data
                        errorJson = new JSONObject(res);
                        Log.d(TAG, "onErrorResponse: " + errorJson);
                        // Toast.makeText(mContext, errorJson.getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (UnsupportedEncodingException | JSONException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    }
                }
                if (error.networkResponse != null) {
                    try {
                        if (errorJson != null){
                            EventBus.getDefault().post(new ProfileEvent(false, error.networkResponse.statusCode, errorJson.getString("message")));
                        }
                        else{
                            EventBus.getDefault().post(new ProfileEvent(false, error.networkResponse.statusCode, ""));
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    EventBus.getDefault().post(new ProfileEvent(false, 504, ""));
                }
            }
        }) {
            @Override
            public Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                // params.put("Authorization", getAuthHeader());
                return params;
            }
        };
        jsonRequest.setShouldCache(false);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(10000, 2, 2));
        ExecutiveServicesApp.getInstance().addToRequestQueue(jsonRequest);
    }*/
}