package com.kwiky.executivemodule.Helpers;

import android.text.Editable;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validations {
    /**
     * Verify Blank Fields
     */
    public boolean isBlank(String string) {
        return string.trim().equals("");
    }
    public static boolean IsEmpty(Editable text) {
        if (text == null || text.toString() == null || text.toString().trim() == null || text.toString().trim().length() == 0)
            return true;
        return false;
    }
    /**
     * @param string
     * @return return true if this is null otherwise false
     */
    public boolean isBlank(EditText string) {
        return string.getText().toString().trim().equals("");
    }

    /**
     * Verify Full Name
     */
    public boolean isValidFullName(EditText fullName) {
        return Pattern.compile("^[\\p{L} .'-]+$", Pattern.CASE_INSENSITIVE).matcher(fullName.getText().toString().trim()).matches();
    }

    /**
     * Match Passwords
     */
    public boolean isMatching(EditText stringFirst, EditText stringSecond) {
        return stringFirst.getText().toString().trim().equals(stringSecond.getText().toString().trim());
    }

    /**
     * Email Validations
     */
    public boolean isValidEmail(String email) {
       /*String PATTERN_EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
               + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
       return Pattern.compile(PATTERN_EMAIL, Pattern.CASE_INSENSITIVE).matcher(email.getText().toString().trim()).matches();*/
        String validemail = "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +

                "\\@" +

                "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +

                "(" +

                "\\." +

                "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +

                ")+";
        Matcher matcher= Pattern.compile(validemail).matcher(email);
        return !matcher.matches();
    }
/*
* ^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]

 *
* */

    public boolean isValidUrl(String url) {
       /*String PATTERN_EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
               + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
       return Pattern.compile(PATTERN_EMAIL, Pattern.CASE_INSENSITIVE).matcher(email.getText().toString().trim()).matches();*/
        String validurl = "^((((https?|http?|ftps?|gopher|telnet|nntp)://)|(mailto:|news:))" +
                "(%[0-9A-Fa-f]{2}|[-()_.!~*';/?:@&=+$,A-Za-z0-9])+)" +
                "([).!';/?:,][[:blank:]])?$";
        Matcher matcher= Pattern.compile(validurl).matcher(url);
        return !matcher.matches();
    }


public boolean isValidFb(String fb) {
       /*String PATTERN_EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
               + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
       return Pattern.compile(PATTERN_EMAIL, Pattern.CASE_INSENSITIVE).matcher(email.getText().toString().trim()).matches();*/
    String validfb = "(?:http:\\/\\/)?(?:www.)?facebook.com\\/(?:(?:\\w)*#!\\/)?(?:pages\\/)?(?:[?\\w\\-]*\\/)?(?:profile.php\\?id=(\\d.*))?([\\w\\-]*)?";
    Matcher matcher= Pattern.compile(validfb).matcher(fb);
    return !matcher.matches();
}



    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    /**
     * Verify City, State, Country
     */
    public boolean isValidLocation(EditText location) {
        return Pattern.compile("([a-zA-Z]+|[a-zA-Z]+\\s[a-zA-Z]+)", Pattern.CASE_INSENSITIVE).matcher(location.getText().toString().trim()).matches();
    }

    /**
     * Check Postal Location
     */
    public boolean isValidPostalAddress(EditText postalAddress) {
        return Pattern.compile("^[a-zA-Z0-9_\\s\\.,]{1,}$", Pattern.CASE_INSENSITIVE).matcher(postalAddress.getText().toString().trim()).matches();
    }

    /**
     * Check Valid Date
     */
    public boolean isValidDate(EditText date) {
        return Pattern.compile("(0?[1-9]|[12][0-9]|3[01])/(0?[1-9]|1[012])/((19|20)\\d\\d)", Pattern.CASE_INSENSITIVE).matcher(date.getText().toString().trim()).matches();
    }

    public boolean isValidDate(TextView date) {
        if (date.getText().toString().equalsIgnoreCase("YYYY/MM/DD")) {
            return true;
        } else {
        }
        return false/*Pattern.compile("(0?[1-9]|[12][0-9]|3[01])/(0?[1-9]|1[012])/((19|20)\\d\\d)", Pattern.CASE_INSENSITIVE).matcher(date.getText().toString().trim()).matches()*/;
    }
//(0?[1-9]|[12][0-9]|3[01])/(0?[1-9]|1[012])/((19|20)\\d\\d)

    /**
     * Check Valid NumberHelper
     */
    public boolean isValidNumber(EditText editText) {
        return Pattern.compile("((\\d{1,9})(((\\.)(\\d{0,2})){0,1}))", Pattern.CASE_INSENSITIVE).matcher(editText.getText().toString().trim()).matches();
    }

    public boolean isValidPassword(EditText editText) {

        return Pattern.compile("^[\\p{L} .'-]+$").matcher(editText.getText().toString().trim()).matches();
//        return Pattern.compile("\\d[0-9]{5,15}").matcher(editText.getText().toString().trim()).matches();

    }
    /**
     * Check Valid Phone NumberHelper
     */
    public boolean isValidPhoneNumber(EditText editText) {
        return Pattern.compile("\"^[7-9][0-9]{9}$\"", Pattern.CASE_INSENSITIVE).matcher(editText.getText().toString().trim()).matches();
    }

    /**
     * checck valid mobile no
     *
     * @param editText
     * @return
     */
    public boolean isValidPhone(EditText editText) {
        return Pattern.compile("\\d{18}").matcher(editText.getText().toString().trim()).matches();
    }

    public boolean isValidSelection(Spinner spinner) {
        try {
            if (spinner.getSelectedItemPosition()==0){
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return true;
        }
        return false;

    }


    public static boolean isValidGST(String text) {
        ///^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$/
        //^([0][1-9]|[1-2][0-9]|[3][0-5])([a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}[1-9a-zA-Z]{1}[zZ]{1}[0-9a-zA-Z]{1})+$

        //[0-9]{2} represents the first two characters should be a number.
        //[A-Z]{5} represents the next five characters should be any upper case alphabets.
        //[0-9]{4} represents the next four characters should be any number.
        //[A-Z]{1} represents the next character should be any upper case alphabet.
        //[1-9A-Z]{1} represents the 13th character should be a number from 1-9 or an alphabet.
        //Z represents the 14th character should be Z.
        //[0-9A-Z]{1} represents the 15th character should be an alphabet or a number.

        // "^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$"--geeks for geeks

        return Pattern.compile("^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$", Pattern.CASE_INSENSITIVE).matcher(text).matches();
    }

    public static boolean isValidFSSAI(String text) {
        return text.length()==14;
    }
    public static boolean isValidLabourCertificationNumber(String text) {
        return Pattern.compile("^[A-Z]{2}[0-9]{16}$", Pattern.CASE_INSENSITIVE).matcher(text).matches();
    }

}
