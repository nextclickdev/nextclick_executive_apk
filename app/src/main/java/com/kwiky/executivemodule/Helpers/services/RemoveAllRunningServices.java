package com.kwiky.executivemodule.Helpers.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

/**
 * Created by Chanchal Kishore on 06/07/'19.
 */

public class RemoveAllRunningServices extends Service {

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("ClearFromRecentService", "Service Started");
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("ClearFromRecentService", "Service Destroyed");
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Log.e("ClearFromRecentService", "END");
        //Code here
        /*stopService(new Intent(this, TrackSPLocationService.class));
        stopService(new Intent(this, SPCheckCabIsAcceptService.class));
        new TrackSPLocationService().stopTimer();*/
        stopSelf();
    }
}