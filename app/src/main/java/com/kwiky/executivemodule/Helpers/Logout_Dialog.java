package com.kwiky.executivemodule.Helpers;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kwiky.executivemodule.Activities.DashboardActivity;
import com.kwiky.executivemodule.R;


public class Logout_Dialog {
    private Context context;
    private Dialog dialog;
    private RelativeLayout main;
    private TextView stay,logout,company_name;


    public Logout_Dialog(Context ctx) {
        this.context = ctx;

        dialog = new Dialog(context);
        //note_list=new ArrayList<>();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.logout_dailog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);

        logout=dialog.findViewById(R.id.logout);
        stay=dialog.findViewById(R.id.stay);
        company_name=dialog.findViewById(R.id.company_name);

       // company_name.setText("You might miss updates from "+companyname);
        stay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                ((DashboardActivity)context).setLogout();
            }
        });
    }
    public void showDialog() {
        dialog.show();
        dialog.setCancelable(false);
    }
    public void cancle(){
        dialog.dismiss();
    }

}
