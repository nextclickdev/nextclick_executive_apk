package com.kwiky.executivemodule.Helpers;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Address;
import android.os.Build;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;


import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.kwiky.executivemodule.R;

import java.util.List;


public class Utility {
    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;

    public static  String UserIntent ="user";
    public static String DPIntent ="delivery_partner";
    public static String VendorIntent ="vendor";
    public static String ExecutiveIntent ="executive";

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public static boolean checkPermission(final Context context)
    {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if(currentAPIVersion>= Build.VERSION_CODES.M)
        {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Permission necessary");
                    alertBuilder.setMessage("External storage permission is necessary");
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                        }
                    });
                    AlertDialog alert = alertBuilder.create();
                    alert.show();

                } else {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    public static String getShortcutAddress(List<Address> addresses) {
        String address="";
        try {
            address = addresses.get(0).getAddressLine(0) + "";
            String city = addresses.get(0).getLocality();
            String locality = addresses.get(0).getSubLocality();
            if(locality==null || locality.equals("null"))
                locality = addresses.get(0).getThoroughfare();
            if(locality==null || locality.equals("null"))
                locality = addresses.get(0).getSubAdminArea();
            if(locality==null || locality.equals("null") || city ==null || city.equals("null"))
                return address;
            return locality +", "+ city;

        } catch (Exception ex) {
        }
        return address;
    }


    public static void setupOtpInputs(Context mContext, EditText editTextone, EditText editTexttwo, EditText editTextthree,
                                      EditText editTextfour, EditText editTextFive, EditText editTextSix) {
        addTextChangedListener(editTextone,editTexttwo);
        addTextChangedListener(editTexttwo,editTextthree);
        addTextChangedListener(editTextthree,editTextfour);
        addTextChangedListener(editTextfour,editTextFive);
        addTextChangedListener(editTextFive,editTextSix);
        //    addTextChangedListener(editTextone,editTexttwo);

        editTextone.setOnKeyListener(new GenericKeyEvent(editTextone, null));
        editTexttwo.setOnKeyListener(new GenericKeyEvent(editTexttwo, editTextone));
        editTextthree.setOnKeyListener(new GenericKeyEvent(editTextthree, editTexttwo));
        editTextfour.setOnKeyListener(new GenericKeyEvent(editTextfour, editTextthree));
        editTextFive.setOnKeyListener(new GenericKeyEvent(editTextFive, editTextfour));
        editTextSix.setOnKeyListener(new GenericKeyEvent(editTextSix, editTextFive));
    }

    private static void addTextChangedListener(EditText editTextone, final EditText editTexttwo) {
        editTextone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().trim().isEmpty())
                    editTexttwo.requestFocus();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }
    public static class GenericKeyEvent implements  View.OnKeyListener {

        private final EditText currentView,previousView;

        public GenericKeyEvent(EditText currentView, EditText previousView) {
            this.currentView=currentView;
            this.previousView=previousView;
        }

        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            if(event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_DEL &&
                    currentView.getId() !=
                            R.id.editTextone && currentView.getText().toString().isEmpty()) {
                //If current is empty then previous EditText's number will also be deleted
                if(previousView!=null) {
                    //previousView.setText(null);
                    previousView.requestFocus();
                }
                return true;
            }
            return false;
        }
    }

}
