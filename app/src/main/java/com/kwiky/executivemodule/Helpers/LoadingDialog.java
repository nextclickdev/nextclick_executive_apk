package com.kwiky.executivemodule.Helpers;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;


import com.kwiky.executivemodule.R;

import static android.view.Gravity.CENTER;

public class LoadingDialog {

    public static Dialog dialog;
    
    public static void loadDialog(Context context){
        ImageView loader_gif;
        dialog = new Dialog(context);
        Window window = dialog.getWindow();
        //window.requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.gif_dialog);
        dialog.setCancelable(false);
        window.setGravity(CENTER);


        loader_gif = dialog.findViewById(R.id.loader_gif);
        loader_gif.setVisibility(View.GONE);


        /*Glide.with(context)
                .load(R.drawable.loader_gif)
                .into(loader_gif);
        window.setGravity(CENTER);
        dialog.show();*/
    }
}
