package com.kwiky.executivemodule.utilities;



public interface ValidationMessages {

    public String INVALID = "Invalid";
    public String INVALID_USERID = "Invalid user id";
    public String NOT_ALLOWED = "Not allowed";
    public String EMPTY = "Empty";
    public String EMPTY_NOT_ALLOWED = "Empty field not allowed";
    public String INVALID_MOBILE = "Invalid mobile";
    public String INVALID_WHATSAPP = "Invalid whatsapp number";
    public String INVALID_PASSWORD = "Minimum four characters required";
    public String NOT_MATCHED = "Password not matched";
    public String INVALID_MAIL = "Invalid mail";
    public String LOGIN_SUCCESS = "Login successful";
    public String AVAILABLE = "Will be available soon";
    public String OOPS = "Oops! Something went wrong";
    public String INVALID_LOGIN = "Login Failed";
    public String MAINTENANCE = "Oops! Sorry for inconvenience. Server is under maintenance";
    public String AVAILABLE_SOON = "Will be available soon";
    public String UPDATED_PROFILE="Profile has been updated succesfully";
    public String TIMEINAVLID ="Please select valid time";

}
