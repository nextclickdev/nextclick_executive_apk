package com.kwiky.executivemodule.utilities;

/**
 * Created by Mounika on 24-01-2021.
 */

public class AppConstants {
    public static final int LOCATION_REQUEST = 1000;
    public static final int GPS_REQUEST = 1001;
}
