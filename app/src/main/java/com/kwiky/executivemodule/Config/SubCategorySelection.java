package com.kwiky.executivemodule.Config;

import com.kwiky.executivemodule.models.CategoryObject;

import java.util.ArrayList;

public interface SubCategorySelection {
    void setSelectedCategoryIDs(ArrayList<String> selectedCategoryIDs);
    void setCategory(CategoryObject category);
}
