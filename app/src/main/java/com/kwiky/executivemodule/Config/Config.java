package com.kwiky.executivemodule.Config;

public interface Config {

    public int SPLASH_SCREEN_TIME_OUT = 500;

    public String APP_ID="APP_ID";
    public String APP_ID_VALUE="TXc9PQ==";
    public String VENDOR_APP_ID_VALUE="TWc9PQ==";
    public String USER_TOKEN="user_token";
    public String X_AUTH_TOKEN="X_AUTH_TOKEN";
    public String MAINTENANCE = "Oops! Sorry for inconvenience. Server is under maintenance";
    public String OOPS = "Oops! Something went wrong";

    //Main live

    // public String BASE_URL = "https://nextclick.in/app/";
     public String BASE_URL = "http://debug.nextclick.in/";

    // Test
  //  String BASE_URL = "http://test.foodynest.com/";// "http://test.nextclick.in/";

    // Dev
  //  public String BASE_URL = "http://dev.foodynest.com/";
  //  public String BASE_URL = "http://192.168.29.1/";
    //public String BASE_URL = "http://dev.nextclick.in/";

  //  public String BASE_URL = "http://grepthorhosting.com/nextclick_new/";

    //   String BASE_URL="http://13.233.215.21/";//aws

    //New test
 //   public String BASE_URL="http://newtest.nextclick.in/";

    public String Login = BASE_URL + "auth/api/auth/login";
    public String REGISTER_USER = BASE_URL + "auth/api/auth/register";
    public String REGISTER = BASE_URL + "/executive/api/executive/register/executive";

    public String Login_Verify_Token = BASE_URL + "auth/api/auth/verify";
    public String PROFILE = BASE_URL + "executive/api/executive/profile";
    public String USER_PROFILE = BASE_URL + "user/profile/me?intent=executive";
    public String PROFILEUPDATE = BASE_URL + "user/master/profile/u";
    public String MainAppVendorRegistration = BASE_URL + "user/profile/manage";//executive/vendors/c


    public String States = BASE_URL + "general/api/master/states/";

    public String Categories = BASE_URL + "general/api/master/categories/";

    public String VendorCreation = BASE_URL + "executive/vendors/c";
    public String VENDOR_LIST = BASE_URL + "executive/api/executive/vendor_list";

    public String WALLETHISTORY = BASE_URL + "payment/api/payment/wallet";
    public String ForgotPassword = BASE_URL + "auth/api/auth/forgot_password";
    public String CATEGORY_LIST = BASE_URL + "general/api/master/categories/";

    //terms and conditions
    public String GET_TERMSCONDITIONS = BASE_URL + "/general/api/terms_conditions/termsconditions";
    public String ACCEPT_TERMSCONDITIONS = BASE_URL + "/general/api/terms_conditions/accept_tc";
    public String VALIDATE_TERMSCONDITIONS = BASE_URL + "/general/api/terms_conditions/validate_user";

   // public String SEND_OTP = BASE_URL + "auth/api/auth/otp_gen";
    public String SEND_OTP = BASE_URL + "auth/api/auth/otp";//AUthor-sunil//"executive/api/executive/otp_gen";
    public String VERIFY_OTP = BASE_URL + "auth/api/auth/validate_otp";//AUthor-sunil//"auth/api/auth/verify_otp";


    public String ECITIVE_TYPES=BASE_URL+"/executive/api/executive/executive_type";

}
